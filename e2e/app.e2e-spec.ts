import { DaccsWebPage } from './app.po';

describe('daccs-web App', () => {
  let page: DaccsWebPage;

  beforeEach(() => {
    page = new DaccsWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('daccs works!');
  });
});
