import { browser, element, by } from 'protractor';

export class DaccsWebPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('daccs-root h1')).getText();
  }
}
