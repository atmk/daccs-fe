export const environment = {
  production: true,
  apiBaseUrl: 'https://api.daccs.co.za',
  socketBaseUrl: 'wss://api.daccs.co.za'
};
