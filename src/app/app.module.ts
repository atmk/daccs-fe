import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from 'clarity-angular';
import { LoginComponent } from './login/login.component';
import { DailyReconReconModule } from './views/daily-recon/daily-recon.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './shared/ngrx/reducers.ngrx';
import { SocketService } from './initialiaze/services/socket.service';
import { ConfirmDeactivateGuard } from './shared/guards/confirm.guard';
import { LayoutComponent } from './layout/layout.component';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './shared/guards/auth.guard';
import { InitialiazeComponent } from './initialiaze/initialiaze.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ToasterModule } from 'angular2-toaster';
import { EffectsModule } from '@ngrx/effects';
import { ShiftsSummaryEffects } from './views/daily-recon/shifts-recon/effects/shifts-summary.effect';
import { LoadEODEffects } from './views/daily-recon/eod-recon/effects/load-eod.effect';
import { LoadShiftsEffects } from './views/daily-recon/shifts-recon/effects/load-shifts.effect';
import { InitializerService } from './initialiaze/services/initializer.service';
import { NoContentComponent } from './layout/no-content/no-content.component';
import { SiteAdminModule } from './views/site-admin/site-admin.module';
import { SetupDoneGuard } from './shared/guards/setup-required.guard';
import { CheckSetupComponent } from './views/check-setup/check-setup.component';
import { SharedModule } from './shared/modules/shared.module';
import { ReportsModule } from './views/reports/reports.module';
import { SiteProfileService } from './initialiaze/services/site-profile.service';
import { DashboardModule } from './views/dashboard/dashboard.module';
import { BatchModule } from './views/batch/batch.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    InitialiazeComponent,
    NoContentComponent,
    CheckSetupComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AuthModule,
    MyDatePickerModule,
    SharedModule,
    DashboardModule,
    ReportsModule,
    BatchModule,

    // ngrx
    EffectsModule.forRoot([LoadShiftsEffects, LoadEODEffects, ShiftsSummaryEffects]),
    StoreModule.forRoot(reducers),

    // toaster
    ToasterModule,

    // Views
    DailyReconReconModule,
    SiteAdminModule,

    ClarityModule.forRoot(),
  ],
  providers: [
    SocketService,
    AuthService,
    AuthGuard,
    InitializerService,
    SiteProfileService,

    // guards
    ConfirmDeactivateGuard,
    SetupDoneGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
