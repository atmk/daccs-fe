import * as MainReconActions from '../actions/main-recon.actions';
import { IMainRecon } from '../models/main-recon';

export type Action = MainReconActions.All;

export function mainReconReducer(state: IMainRecon, action: Action): IMainRecon {
  switch (action.type) {
    case MainReconActions.LOAD_MAIN_RECON:
      return action.payload;
    case MainReconActions.COMPLETE_MAIN_RECON:
      return Object.assign({}, state, {complete: action.payload});
    default:
      return state;
  }
}
