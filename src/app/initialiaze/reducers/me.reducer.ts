import * as MeActions from '../actions/me.actions';
import { IUser } from '../../shared/models/user';

export type Action = MeActions.All;

export function meReducer(state: IUser, action: Action): IUser {
  switch (action.type) {
    case MeActions.LOAD_ME:
      return action.payload;
    case MeActions.UPDATE_ME:
      return action.payload;
    default:
      return state;
  }
}
