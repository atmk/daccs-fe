import * as SiteProfileActions from '../actions/site-profile.actions';
import { ISite } from '../../views/site-admin/company/sites/models/site';

export type Action = SiteProfileActions.All;

export function siteProfileReducer(state: ISite, action: Action): ISite {
  switch (action.type) {

    case SiteProfileActions.LOAD_SITE_PROFILE:
      return action.payload;

    case SiteProfileActions.LOAD_ACCESS_SITES:
      return Object.assign({}, state, {access_sites: action.payload});

    case SiteProfileActions.ADD_ACCESS_SITE:
      const newSites = [...state.access_sites, action.payload];
      return Object.assign({}, state, {access_sites: newSites});

    case SiteProfileActions.REMOVE_ACCESS_SITE:

      const remSites = state.access_sites.filter((site: ISite) => {
        return site._id_ !== action.payload;
      });

      return Object.assign({}, state, {access_sites: remSites});

    case SiteProfileActions.UPDATE_ACCESS_SITE:
      const updSites = state.access_sites.map((site: ISite) => {
        return site._id_ === action.payload._id_
          ? Object.assign({}, site, action.payload)
          : site;
      });
      return Object.assign({}, state, {access_sites: updSites});

    default:
      return state;
  }
}
