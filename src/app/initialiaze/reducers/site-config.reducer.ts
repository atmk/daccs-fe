import * as SiteConfigActions from '../actions/site-config.actions';
import { ConfigItems, IGenLedgerConfig, ISiteConfig } from '../models/site-config';

export type Action = SiteConfigActions.All;

class SiteConfigInitial implements ISiteConfig {
  cashiers = [];
  general_ledgers = [];
  suppliers = [];
  account_holders = [];
  vat_codes = [];
  received_on_account_descriptions = [];
  speedpoint_terminals = [];
}

export function siteConfigReducer(state: ISiteConfig = new SiteConfigInitial(), action: Action): ISiteConfig {
  switch (action.type) {

    case SiteConfigActions.LOAD_SITE_CONFIG:
      return action.payload;

    case SiteConfigActions.UPDATE_SITE_CONFIG:
      if (action.payload.key === 'general_ledgers') {
        const systemGLArr: Array<IGenLedgerConfig> = state.general_ledgers.filter(itm => {
          return itm.site === null;
        });
        const newGLArr = [...action.payload.data, ...systemGLArr];
        return Object.assign({}, state, {general_ledgers: newGLArr});
      } else {
        const configUpdObj: { data: ConfigItems, key: string } = <{ data: ConfigItems, key: string }>{};
        configUpdObj[action.payload.key] = action.payload.data;
        return Object.assign({}, state, configUpdObj);
      }

    default:
      return state;
  }
}
