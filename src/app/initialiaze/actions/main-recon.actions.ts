import { Action } from '@ngrx/store';
import { IMainRecon } from '../models/main-recon';

export const LOAD_MAIN_RECON = 'LOAD_MAIN_RECON';
export const COMPLETE_MAIN_RECON = 'COMPLETE_MAIN_RECON';

export class LoadMainRecon implements Action {
  readonly type = LOAD_MAIN_RECON;

  constructor(public payload: IMainRecon) {
  }
}

export class CompleteMainRecon implements Action {
  readonly type = COMPLETE_MAIN_RECON;

  constructor(public payload: boolean) {
  }
}

export type All = LoadMainRecon | CompleteMainRecon
