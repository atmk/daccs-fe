import { Action } from '@ngrx/store';
import { ISite } from '../../views/site-admin/company/sites/models/site';

export const LOAD_SITE_PROFILE = 'LOAD_SITE_PROFILE';
export const LOAD_ACCESS_SITES = 'LOAD_ACCESS_SITES';
export const ADD_ACCESS_SITE = 'ADD_ACCESS_SITE';
export const REMOVE_ACCESS_SITE = 'REMOVE_ACCESS_SITE';
export const UPDATE_ACCESS_SITE = 'UPDATE_ACCESS_SITE';

export class LoadSiteProfile implements Action {
  readonly type = LOAD_SITE_PROFILE;

  constructor(public payload: ISite) {
  }
}

export class LoadAccessSites implements Action {
  readonly type = LOAD_ACCESS_SITES;

  constructor(public payload: Array<ISite>) {
  }
}

export class AddAccessSites implements Action {
  readonly type = ADD_ACCESS_SITE;

  constructor(public payload: ISite) {
  }
}

export class RemoveAccessSites implements Action {
  readonly type = REMOVE_ACCESS_SITE;

  constructor(public payload: number) {
  }
}

export class UpdateAccessSites implements Action {
  readonly type = UPDATE_ACCESS_SITE;

  constructor(public payload: ISite) {
  }
}

export type All = LoadSiteProfile | LoadAccessSites | AddAccessSites | RemoveAccessSites | UpdateAccessSites
