import { Action } from '@ngrx/store';
import { ConfigItems, ISiteConfig } from '../models/site-config';

export const LOAD_SITE_CONFIG = 'LOAD_SITE_CONFIG';
export const UPDATE_SITE_CONFIG = 'UPDATE_SITE_CONFIG';

export class LoadSiteConfig implements Action {
  readonly type = LOAD_SITE_CONFIG;

  constructor(public payload: ISiteConfig) {
  }

}

export class UpdateSiteConfig implements Action {
  readonly type = UPDATE_SITE_CONFIG;

  constructor(public payload: { data: ConfigItems, key: string }) {
  }

}

export type All = LoadSiteConfig | UpdateSiteConfig
