import { Action } from '@ngrx/store';
import { IUser } from '../../shared/models/user';

export const LOAD_ME = 'LOAD_ME';
export const UPDATE_ME = 'UPDATE_ME';

export class LoadMe implements Action {
  readonly type = LOAD_ME;

  constructor(public payload: IUser) {
  }
}

export class UpdateMe implements Action {
  readonly type = UPDATE_ME;

  constructor(public payload: IUser) {
  }
}

export type All = LoadMe | UpdateMe
