import { Injectable } from '@angular/core';
import { QueueingSubject } from 'queueing-subject';
import 'rxjs/add/operator/share';
import websocketConnect, { Connection } from 'rxjs-websockets';

@Injectable()
export class SocketService {
  private inputStream: QueueingSubject<any>;
  public connection: Connection;

  public connect(socketUrl: string): void {
    this.connection = websocketConnect(
      socketUrl,
      this.inputStream = new QueueingSubject<any>()
    );
  }

  public send(message: any): void {
    this.inputStream.next(message);
  }

}
