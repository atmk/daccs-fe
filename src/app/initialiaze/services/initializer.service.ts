import { Injectable } from '@angular/core';
import { IAppStore } from '../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ISiteConfig } from '../models/site-config';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../environments/environment';
import { IMainRecon } from '../models/main-recon';
import * as siteProfileAct from '../actions/site-profile.actions';
import * as meAct from '../actions/me.actions';
import { ISite } from '../../views/site-admin/company/sites/models/site';
import { IUser } from '../../shared/models/user';

export interface ISetupReq {
  setup_required: boolean;
  required_items?: Array<string>;
}

export interface IBrokenCon {
  broken: boolean;
  timeout?: number;
}

@Injectable()
export class InitializerService {

  private _apiBaseUrl = environment.apiBaseUrl;

  public settings$: Observable<ISiteConfig> = this._store.select('siteConfigReducer');
  public mainRecon$: Observable<IMainRecon> = this._store.select('mainReconReducer');
  public newDate$: ReplaySubject<string> = new ReplaySubject(1);
  public currDate$: BehaviorSubject<string> = new BehaviorSubject('');
  public serverIsRunning$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public setupRequired$: BehaviorSubject<ISetupReq> = new BehaviorSubject({setup_required: false});
  public reloadSocket$: Subject<boolean> = new Subject();
  public conbroken$: Subject<IBrokenCon> = new Subject();

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
  }

  public getProfile(): void {
    const currSiteProfile = this._authHttp.get(`${this._apiBaseUrl}/companies/site/me/`).map(res => res.json());
    const sites = this._authHttp.get(`${this._apiBaseUrl}/companies/site/switch/list/`).map(res => res.json());

    Observable.forkJoin([currSiteProfile, sites])
      .subscribe((results: [ISite, Array<ISite>]) => {
        this._store.dispatch(new siteProfileAct.LoadSiteProfile(results[0]));
        this._store.dispatch(new siteProfileAct.LoadAccessSites(results[1]));
      }, err => {
        console.log(err);
      });
  }

  public getMe(): void {
    this._authHttp.get(`${this._apiBaseUrl}/users/user/me/`).map(res => res.json())
      .subscribe((val: IUser) => {
        this._store.dispatch(new meAct.LoadMe(val));
      }, err => console.log(err));
  }

  public closeRecon(body: { complete: boolean }, id: number): any {
    return this._authHttp.patch(`${this._apiBaseUrl}/recons/recon/${id}/`, JSON.stringify(body))
      .map(res => res.json());
  }

}
