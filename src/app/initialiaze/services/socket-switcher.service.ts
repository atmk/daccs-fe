import { Injectable } from '@angular/core';
import { IAppStore } from '../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { IncomingDataType } from '../models/incoming-data-type';
import { IMainRecon } from '../models/main-recon';
import { ShiftsReconService } from '../../views/daily-recon/shifts-recon/services/shifts-recon.service';
import { Toast, ToasterService } from 'angular2-toaster';
import { IDecodedUser, IPartialPayload, ReconData } from '../initialiaze.component';
import { InitializerService } from './initializer.service';
import * as cashierAct from '../../views/daily-recon/shifts-recon/actions/shift.actions';
import * as eodAct from '../../views/daily-recon/eod-recon/actions/eod.actions';
import * as settingsAct from '../actions/site-config.actions';
import * as mainReconAct from '../actions/main-recon.actions';

@Injectable()
export class SocketSwitcherService {

  constructor(private _store: Store<IAppStore>,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService,
              private _shiftsReconService: ShiftsReconService) {
  }

  socketSwitch(reconData: ReconData, user: IDecodedUser): void {

    switch (reconData.data_type) {

      //  **** ALL INITIAL DATA
      case IncomingDataType.ALL_SHIFTS_AND_EOD_RECONS:
        // load shift into reducer
        this._store.dispatch(new cashierAct.LoadShifts(reconData.shift_recon));
        // load EOD into reducer
        this._store.dispatch(new eodAct.LoadEODData(reconData.eod_recon));
        // load the settings insto state
        this._store.dispatch(new settingsAct.LoadSiteConfig(reconData.settings));
        // set recon complete or not
        const mainReconObj: IMainRecon = <IMainRecon>{};
        mainReconObj['complete'] = reconData.complete;
        mainReconObj['data_type'] = reconData.data_type;
        mainReconObj['date'] = reconData.date;
        mainReconObj['recon_id'] = reconData.recon_id;
        this._store.dispatch(new mainReconAct.LoadMainRecon(mainReconObj));
        break;

      //  **** ALL PARTIAL SHIFT DATA
      case IncomingDataType.SHIFT_PARTIAL_UPDATE:

        if (reconData.updated_by.id !== user.user_id) {

          const shiftName = this._shiftsReconService.getShiftNameById(reconData.id);

          const toastUpd: Toast = {
            type: 'info',
            title: 'Data Changed',
            body: `User ${reconData.updated_by.name} has made changes to ${reconData.key} \
               for cashier: ${shiftName}, shift id: ${reconData.id}`,
            timeout: 8000
          };

          this._toasterService.pop(toastUpd);
        }

        const payload: IPartialPayload = {
          data: reconData.data,
          key: reconData.key,
          id: reconData.id // mongo ID
        };

        this._store.dispatch(new cashierAct.UpdatePartialShift(payload));
        break;

      //  **** ADD A NEW SHIFT
      case IncomingDataType.SHIFT_CREATED:

        if (reconData.created_by.id !== user.user_id) {

          const toastAdd: Toast = {
            type: 'info',
            title: 'Data Changed',
            body: `User ${reconData.created_by.name} has added a new cashier shift. Cashier: ${reconData.data.name}, \
                 shift ID: ${reconData.data.id}`,
            timeout: 8000
          };

          this._toasterService.pop(toastAdd);
        }

        this._store.dispatch(new cashierAct.AddNewShift(reconData.data));
        break;

      //  **** DELETE A SHIFT
      case IncomingDataType.SHIFT_DELETED:

        if (reconData.deleted_by.id !== user.user_id) {

          const shiftName = this._shiftsReconService.getShiftNameById(reconData.id);

          const toastDel: Toast = {
            type: 'info',
            title: 'Data Changed',
            body: `User ${reconData.deleted_by.name} has deleted cashier: ${shiftName}, shift id: ${reconData.id}`,
            timeout: 8000
          };

          this._toasterService.pop(toastDel);
        }

        this._store.dispatch(new cashierAct.RemoveShift({id: reconData.id}));
        this._store.dispatch(new cashierAct.RemoveShiftSummary({id: reconData.id}));
        break;

      //  **** UPDATE EOD
      case IncomingDataType.EOD_PARTIAL_UPDATE:
        if (reconData.key === 'sales') {
          this._store.dispatch(new eodAct.UpdateEODSalesData(reconData.data));
        } else {
          this._store.dispatch(new eodAct.UpdateEODItemData({key: reconData.key, amount: reconData.data}));
        }
        break;

      // **** NO DATA SETUP YET
      case IncomingDataType.SETUP_REQUIRED:
        this._initializerService.setupRequired$.next({setup_required: true, required_items: reconData.data});
        break;

      // **** A Site config item is updated
      case IncomingDataType.CONFIG_UPDATED:
        this._store.dispatch(new settingsAct.UpdateSiteConfig({data: reconData.data, key: reconData.key}));
        break;

      default:
        console.log('Def');
        break;
    }

  }

}
