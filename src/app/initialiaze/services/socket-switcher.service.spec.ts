import { TestBed, inject } from '@angular/core/testing';

import { SocketSwitcherService } from './socket-switcher.service';

describe('SocketSwitcherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocketSwitcherService]
    });
  });

  it('should be created', inject([SocketSwitcherService], (service: SocketSwitcherService) => {
    expect(service).toBeTruthy();
  }));
});
