import { Injectable } from '@angular/core';
import { IAppStore } from '../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { ISite } from '../../views/site-admin/company/sites/models/site';

@Injectable()
export class SiteProfileService {

  public siteProfile$: Observable<ISite>;
  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.siteProfile$ = this._store.select('siteProfileReducer')
  }

  public switchSite(id): any {
    this._authHttp.get(`${this._apiBaseUrl}/companies/site/switch/${id}/`).map(res => res.json())
      .subscribe(() => {
        window.location.replace('/');
      }, err => console.log(err));
  }
}
