import { Component, OnDestroy, OnInit } from '@angular/core';
import { IShift } from '../views/daily-recon/shifts-recon/models/shift';
import { IEODData } from '../views/daily-recon/eod-recon/models/eod';
import { SocketService } from './services/socket.service';
import { AuthService } from '../auth/auth.service';
import { UtilityFunctions } from '../shared/utils/utility-functions';
import { environment } from '../../environments/environment';
import { Subscription } from 'rxjs/Subscription';
import { JwtHelper } from 'angular2-jwt';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { InitializerService } from './services/initializer.service';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/delayWhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/mapTo';
import { Observable } from 'rxjs/Observable';
import { SocketSwitcherService } from './services/socket-switcher.service';
import { SOCKET_TIMOUT } from '../shared/constants/socket-timeout';

interface IReconDataFull {
  complete: boolean;
  data_type: number;
  date: string;
  recon_id: number;
  eod_recon: IEODData;
  shift_recon: Array<IShift>;
  settings: any; // TODO
}

interface IReconDataPartial {
  id: string;
  data?: any;
  date: string;
  data_type: number;
  key?: string;
  recon?: string;
  name?: string;
  updated_by?: { id: number, name: string };
  deleted_by?: { id: number, name: string };
  created_by?: { id: number, name: string };
}

export interface IPartialPayload {
  data: any;
  key: string;
  id: string; // mongo ID
}

export interface IDecodedUser {
  email: string;
  exp: number;
  orig_iat: number;
  user_id: number;
  username: string;
}

export type ReconData = IReconDataFull & IReconDataPartial;

@Component({
  selector: 'daccs-initialiaze',
  templateUrl: './initialiaze.component.html',
  styleUrls: ['./initialiaze.component.scss'],
  providers: [SocketSwitcherService]
})
export class InitialiazeComponent implements OnInit, OnDestroy {

  jwtHelper: JwtHelper = new JwtHelper();

  public loading = false;
  public online$: Observable<boolean>;

  private _socketBaseUrl = environment.socketBaseUrl;
  private _socketSubscription: Subscription;
  private _clickDateSubscription: Subscription;
  private _reloadSubscription: Subscription;
  private _currDate: Date;

  constructor(private _socketService: SocketService,
              private _initializerService: InitializerService,
              private _socketSwitcherService: SocketSwitcherService,
              private _authService: AuthService) {

    this.online$ = Observable.merge(
      Observable.of(navigator.onLine),
      Observable.fromEvent(window, 'online').mapTo(true),
      Observable.fromEvent(window, 'offline').mapTo(false)
    );

    if (environment.production) {
      this._currDate = new Date();
      this._currDate.setDate(this._currDate.getDate() - 1);
    } else {
      this._currDate = new Date(2017, 11 - 1, 5);
    }

    this._clickDateSubscription = this._initializerService.newDate$
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe((date: string) => {
        this._socketSubscription.unsubscribe();
        this._conn(date, true);
      });

    this._reloadSubscription = this._initializerService.reloadSocket$.subscribe(val => {
      if (val) {
        this._socketSubscription.unsubscribe();
        const date = UtilityFunctions.getDateStringFromDate(this._currDate);
        this._conn(date, true);
      }
    });

    // watch token status
    this._authService.tokenIsExpired().subscribe(val => {
      if (val) {
        this._authService.logout();
      }
    });

  }

  public dateChangedEvent(dateStr) {
    this._initializerService.newDate$.next(dateStr);
    this._initializerService.currDate$.next(dateStr); // TODO relook at the date change and ReplaySubject to clean this up
  }

  public handleManualReconnect(ev: { newCon: boolean, date: Date }): void {
    if (ev.newCon) {
      this._socketSubscription.unsubscribe();
      const date = UtilityFunctions.getDateStringFromDate(ev.date);
      this._conn(date, false);
    }
  }

  private _conn(dateStr: string, fresh?: boolean): void {
    if (fresh) {
      this.loading = true;
    }
    const token = this._authService.getToken();
    const wsUrl = `${this._socketBaseUrl}/ws/${dateStr}/?token=${token}`;

    this._socketService.connect(wsUrl);

    this._socketSubscription = this._socketService.connection.messages
      .retryWhen(errors => errors
        .do(() => {
          console.log(errors);
          this._initializerService.conbroken$.next({broken: true, timeout: SOCKET_TIMOUT});
        })
        // restart in n seconds
        .delayWhen(() => Observable.timer(SOCKET_TIMOUT))
      )
      .subscribe((rawData: string) => {
        const reconData: ReconData = JSON.parse(rawData);
        this._initializerService.conbroken$.next({broken: false});

        if (!environment.production) {
          console.log('Recon Data from server: ', reconData);
        }

        this.loading = false;
        this._initializerService.serverIsRunning$.next(false);
        this._initializerService.setupRequired$.next({setup_required: false, required_items: []});
        const user: IDecodedUser = this.jwtHelper.decodeToken(token);

        // Handle all incoming sockets in this service via data_type from API
        this._socketSwitcherService.socketSwitch(reconData, user);

      }, err => {
        console.log('Socket Error', err);
        this._initializerService.serverIsRunning$.next(false);
      });

    // send message to server, if the socket is not connected it will be sent
    // as soon as the connection becomes available thanks to QueueingSubject
    this._socketService.send({type: 'ping server'});
  }

  ngOnInit(): void {
    const date = UtilityFunctions.getDateStringFromDate(this._currDate);
    this._conn(date, true);
    this._initializerService.currDate$.next(date);

    // load site profile data into state
    this._initializerService.getProfile();
    this._initializerService.getMe();
  }

  ngOnDestroy(): void {
    this._socketSubscription.unsubscribe();
    if (this._clickDateSubscription) {
      this._clickDateSubscription.unsubscribe();
    }
    if (this._reloadSubscription) {
      this._reloadSubscription.unsubscribe();
    }
  }

}
