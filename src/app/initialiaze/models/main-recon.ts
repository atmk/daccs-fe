export interface IMainRecon {
  complete: boolean;
  data_type: number;
  date: string;
  recon_id: number
}
