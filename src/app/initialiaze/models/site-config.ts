import { GLTypes } from '../../shared/constants/gl-types';

export interface ISiteConfig {
  cashiers: Array<ICashierConfig>;
  general_ledgers: Array<IGenLedgerConfig>;
  suppliers: Array<ISupplierConfig>;
  account_holders: Array<IAccHolderConfig>;
  vat_codes: Array<IVatConfig>;
  received_on_account_descriptions: Array<IRecAccConfig>;
  speedpoint_terminals: Array<ISpeedpointTermsConfig>;
}

export interface ICashierConfig {
  _id_: number;
  name: string;
  surname: string;
  employee_number: string;
  id_number: string;
  site: number,
}

export interface IGenLedgerConfig {
  _id_: number;
  title: string;
  code: string;
  ledger_type: number,
  site: number
}

export interface ISupplierConfig {
  _id_: number;
  title: string;
  code: string;
  site: number
}

export interface IAccHolderConfig {
  _id_: number;
  title: string;
  code: string;
  site: number
}

export interface IVatConfig {
  _id_: number;
  title: string;
  code: string;
  percentage: string;
  country: string;
  description: string;
}

export interface IRecAccConfig {
  _id_: number;
  title: string;
  code: string;
  ledger_type: GLTypes;
  description?: string
}

export interface ISpeedpointTermsConfig {
  _id_: number;
  terminal_number: string;
  description: string;
  site?: number;
}

export type ConfigItems =
  Array<ICashierConfig>
  | Array<IGenLedgerConfig>
  | Array<ISupplierConfig>
  | Array<IAccHolderConfig>
  | Array<IVatConfig>
  | Array<IRecAccConfig>
  | Array<ISpeedpointTermsConfig>
