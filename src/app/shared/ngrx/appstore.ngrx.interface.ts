import { IShift, IShiftTotal } from '../../views/daily-recon/shifts-recon/models/shift';
import { IEODCat, IEODData, IMainCat, ISubCat } from '../../views/daily-recon/eod-recon/models/eod';
import {
  IAccHolderConfig,
  ICashierConfig,
  IGenLedgerConfig,
  ISiteConfig,
  ISpeedpointTermsConfig,
  ISupplierConfig
} from '../../initialiaze/models/site-config';

import { ISite } from '../../views/site-admin/company/sites/models/site';
import { IMainRecon } from '../../initialiaze/models/main-recon';
import { IUser } from '../models/user';
import { ISiteNotifySetting } from '../../views/site-admin/company/notifications/models/site-notifications';
import { ICompanyNotifySetting } from '../../views/site-admin/company/notifications/models/company-notifications';
import { BatchState } from '../../views/batch/models/batch-status';

export interface IAppStore {
  shiftsReducer: Array<IShift>;
  shiftsSummaryReducer: Array<IShiftTotal>;
  eodReconReducer: IEODData;
  sitesReducer: Array<ISite>;
  siteConfigReducer: ISiteConfig;
  siteProfileReducer: ISite;
  mainCatReducer: Array<IMainCat>;
  eodCatReducer: Array<IEODCat>;
  subCatReducer: Array<ISubCat>;
  suppliersSetupReducer: Array<ISupplierConfig>;
  speedpointsSetupReducer: Array<ISpeedpointTermsConfig>;
  genLedgersSetupReducer: Array<IGenLedgerConfig>;
  cashiersSetupReducer: Array<ICashierConfig>;
  accHoldersSetupReducer: Array<IAccHolderConfig>;
  meReducer: IUser;
  mainReconReducer: IMainRecon;
  usersReducer: Array<IUser>;
  siteNotificationsReducer: Array<ISiteNotifySetting>;
  companyNotificationsReducer: Array<ICompanyNotifySetting>;
  batchStateReducer: BatchState
}
