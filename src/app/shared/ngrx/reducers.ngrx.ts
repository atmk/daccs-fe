import { ActionReducerMap } from '@ngrx/store';
import { IAppStore } from './appstore.ngrx.interface';
import { shiftsReducer } from '../../views/daily-recon/shifts-recon/reducers/shift.reducers';
import { shiftsSummaryReducer } from '../../views/daily-recon/shifts-recon/reducers/shift-summary.reducer';
import { eodReconReducer } from '../../views/daily-recon/eod-recon/reducers/eod-recon.reducer';
import { siteConfigReducer } from '../../initialiaze/reducers/site-config.reducer';
import { siteProfileReducer } from '../../initialiaze/reducers/site-profile.reducer';
import { sitesReducer } from '../../views/site-admin/company/sites/reducers/sites.reducer';
import { mainCatReducer } from '../../views/site-admin/site-setup/cat-setup/reducers/main-cat.reducers';
import { subCatReducer } from '../../views/site-admin/site-setup/cat-setup/reducers/sub-cat.reducers';
import { eodCatReducer } from '../../views/site-admin/site-setup/cat-setup/reducers/eod-cat.reducers';
import { suppliersSetupReducer } from '../../views/site-admin/site-setup/suppliers-setup/reducers/suppliers-setup.reducer';
import { speedpointsSetupReducer } from '../../views/site-admin/site-setup/speedpoint-setup/reducers/speedpoints-setup.reducer';
import { genLedgersSetupReducer } from '../../views/site-admin/site-setup/gen-ledger-setup/reducers/gen-ledger-setup.reducer';
import { cashiersSetupReducer } from '../../views/site-admin/site-setup/cashiers-setup/reducers/cashiers-setup.reducer';
import { accHoldersSetupReducer } from '../../views/site-admin/site-setup/acc-holders-setup/reducers/acc-holders-setup.reducer';
import { meReducer } from '../../initialiaze/reducers/me.reducer';
import { mainReconReducer } from '../../initialiaze/reducers/main-recon.reducer';
import { usersReducer } from '../../views/site-admin/company/users/reducers/users.reducer';
import { companyNotificationsReducer } from '../../views/site-admin/company/notifications/reducers/company-notifications.reducer';
import { siteNotificationsReducer } from '../../views/site-admin/company/notifications/reducers/site-notifications.reducer';
import { batchStateReducer } from '../../views/batch/reducers/batch-status.reducer';

export const reducers: ActionReducerMap<IAppStore> = {
  shiftsReducer: shiftsReducer,
  shiftsSummaryReducer: shiftsSummaryReducer,
  eodReconReducer: eodReconReducer,
  sitesReducer: sitesReducer,
  siteConfigReducer: siteConfigReducer,
  siteProfileReducer: siteProfileReducer,
  mainCatReducer: mainCatReducer,
  eodCatReducer: eodCatReducer,
  subCatReducer: subCatReducer,
  suppliersSetupReducer: suppliersSetupReducer,
  speedpointsSetupReducer: speedpointsSetupReducer,
  genLedgersSetupReducer: genLedgersSetupReducer,
  cashiersSetupReducer: cashiersSetupReducer,
  accHoldersSetupReducer: accHoldersSetupReducer,
  meReducer: meReducer,
  mainReconReducer: mainReconReducer,
  usersReducer: usersReducer,
  siteNotificationsReducer: siteNotificationsReducer,
  companyNotificationsReducer: companyNotificationsReducer,
  batchStateReducer: batchStateReducer
};
