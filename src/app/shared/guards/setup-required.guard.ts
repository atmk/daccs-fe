import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ISetupReq, InitializerService } from '../../initialiaze/services/initializer.service';

@Injectable()
export class SetupDoneGuard implements CanActivate {

  private _setupDone: boolean;

  constructor(private _initializerService: InitializerService, private _router: Router) {
    this._initializerService.setupRequired$.subscribe((val: ISetupReq) => {
      this._setupDone = !val.setup_required;
    })
  }

  canActivate() {
    // if (this._setupDone) {
    //   return true;
    // }
    // this._router.navigate(['/dash']);
    // return false;
    return true
  }

}
