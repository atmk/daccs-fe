import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot, CanActivateChild
} from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private _authService: AuthService, private _router: Router) {
  }

  canActivate(// Not using but worth knowing about
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    if (this._authService.isLoggedIn()) {
      return true;
    }
    this._router.navigate(['/login']);
    return false;
  }

  canActivateChild(// Not using but worth knowing about
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    if (this._authService.isLoggedIn()) {
      return true;
    }
    this._router.navigate(['/login']);
    return false;
  }

}
