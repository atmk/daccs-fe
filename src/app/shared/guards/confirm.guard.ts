import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ShiftControllerComponent } from '../../views/daily-recon/shifts-recon/controller/shift-controller.component';
import { EodControllerComponent } from '../../views/daily-recon/eod-recon/controller/eod-controller.component';
import {
  FormSalesEodComponent
} from '../../views/daily-recon/eod-recon/components/sales-eod-container/form-sales-eod/form-sales-eod.component';

type CMP = ShiftControllerComponent | EodControllerComponent | FormSalesEodComponent

@Injectable()
export class ConfirmDeactivateGuard implements CanDeactivate<CMP> {

  canDeactivate(target: CMP) {
    if (target.checkDirty()) {
      return window.confirm('You have unsaved changes! Are you sure you want to navigate away?');
    }
    return true;
  }

}
