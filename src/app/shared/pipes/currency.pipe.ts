import { Pipe, PipeTransform } from '@angular/core';

const PADDING = '000000';

@Pipe({name: 'daccsCurrency'})
export class DaccsCurrencyPipe implements PipeTransform {

  private _prefix: string;
  private _decimalSeperator: string;
  private _thousandSeperator: string;
  private _suffix: string;
  private _invalid = '';

  constructor() {
    // TODO comes from configuration settings
    this._prefix = 'R ';
    this._decimalSeperator = '.';
    this._thousandSeperator = ' ';
    this._suffix = '';
  }

  transform(value: any, fractionSize: number = 2): string {

    if (typeof value === 'string' && (value.startsWith('.') || value.startsWith('-.'))) {
      value = '0' + value;
    }

    let [integer, fraction = ''] = (value || '').toString()
      .split('.');

    if (value === 0) {
      integer = '0';
    }

    fraction = fractionSize > 0
      ? this._decimalSeperator + (fraction + PADDING).substring(0, fractionSize)
      : '';

    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this._thousandSeperator);

    if (!integer || value === this._invalid) {
      return this._invalid;
    }

    return this._prefix + integer + fraction + this._suffix;
  }

  parse(value: string, fractionSize: number = 2): string {
    let [integer, fraction = ''] = (value || '').replace(this._prefix, '')
      .replace(this._suffix, '')
      .split(this._decimalSeperator);

    integer = integer.replace(new RegExp(this._thousandSeperator, 'g'), '');

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this._decimalSeperator + (fraction + PADDING).substring(0, fractionSize)
      : '';

    return integer + fraction;
  }

}
