import { Pipe, PipeTransform } from '@angular/core';

// adapted from http://www.fueltravel.com/blog/migrating-from-angular-1-to-2-part-1-pipes/

@Pipe({
  name: 'multiOrder'
})
export class MultiOrderPipe implements PipeTransform {

  private _orderByComparator(a: any, b: any): number {

    if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
      // Isn't a number so lowercase the string to properly compare
      if (typeof a === 'string') a.toLowerCase();
      if (typeof b === 'string') b.toLowerCase();
      if (a < b) return -1;
      if (a > b) return 1;
    } else {
      // Parse strings as numbers to compare properly
      if (parseFloat(a) < parseFloat(b)) return -1;
      if (parseFloat(a) > parseFloat(b)) return 1;
    }

    return 0; // equal each other
  }

  transform(value: any, config = '+'): any {
    if (!Array.isArray(value)) return value;

    if (!Array.isArray(config) || (Array.isArray(config) && config.length === 1)) {
      // Single property to sort on
      const propertyToCheck: string = !Array.isArray(config) ? config : config[0];
      const desc = propertyToCheck.substr(0, 1) === '-';

      if (!propertyToCheck || propertyToCheck === '-' || propertyToCheck === '+') {
        // is a basic array that is sorting on the array's object itself
        return !desc ? value.sort() : value.sort().reverse();
      } else {
        const property: string = propertyToCheck.substr(0, 1) === '+' || propertyToCheck.substr(0, 1) === '-'
          ? propertyToCheck.substr(1)
          : propertyToCheck;

        return value.sort((a: any, b: any) => {
          return !desc
            ? this._orderByComparator(a[property], b[property])
            : -this._orderByComparator(a[property], b[property]);
        });
        // is a complex array that is sorting on a single property
      }
    } else {
      // Loop over property of the array in order and sort
      return value.sort((a: any, b: any) => {

        for (let i = 0; i < config.length; i++) {
          const desc = config[i].substr(0, 1) === '-';
          const property = config[i].substr(0, 1) === '+' || config[i].substr(0, 1) === '-'
            ? config[i].substr(1)
            : config[i];

          //
          const comparison = !desc
            ? this._orderByComparator(a[property], b[property])
            : -this._orderByComparator(a[property], b[property]);

          // Don't return 0 yet in case of needing to sort by next property
          if (comparison !== 0) return comparison;
        }

        return 0; // equal each other
      });
    }
  }

}
