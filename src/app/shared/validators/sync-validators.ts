import { AbstractControl } from '@angular/forms';
import { emailRegex } from '../utils/email-regex';
import { passwordRegex } from '../utils/password-regex';

export class CustomSyncValidators {

  static integerValidator = (control: AbstractControl) => {
    if (control.value) {
      if (isNaN(control.value)) {
        return {'invalidIntegerValue': true};
      }
    }
    return null;
  };

  static emailValidator(control: AbstractControl) {
    if (control.value) {
      if (!control.value.match(emailRegex)) {

        return {'invalidEmailAddress': true};
      }
    }
    return null;
  }
  static passwordValidator(control: AbstractControl) {
    if (control.value) {
      if (!control.value.match(passwordRegex)) {

        return {'invalidEmailAddress': true};
      }
    }
    return null;
  }

}

