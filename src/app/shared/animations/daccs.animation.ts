import { trigger, state, animate, style, transition } from '@angular/animations';

export function routerTransition() {
  return opacityInOut();
}

export function flashTransition() {
  return flash();
}

export function flash() {
  return trigger('flashTransition', [
    transition('*=>*', [
      style({background: '#FFFADC', color: '#565656'}),
      animate('0.5s ease-in-out', style({background: '#9460b8', color: '#FFFFFF'})),
      animate('1s 5s ease-in-out', style({background: '#FFFADC', color: '#565656'}))
    ])
  ]);
}

export function slideToRight() {
  return trigger('routerTransition', [
    state('void', style({position: 'fixed', width: '100%'})),
    state('*', style({position: 'fixed', width: '100%'})),
    transition(':enter', [
      style({transform: 'translateX(-100%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
    ]),
    transition(':leave', [
      style({transform: 'translateX(0%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateX(100%)'}))
    ])
  ]);
}

export function slideToLeft() {
  return trigger('routerTransition', [
    transition(':enter', [
      style({transform: 'translateX(100%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateX(0%)'}))
    ]),
    transition(':leave', [
      style({transform: 'translateX(0%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateX(-100%)'}))
    ])
  ]);
}

export function opacityInOut() {
  return trigger('routerTransition', [
    transition('*=>*', [
      style({opacity: 0}),
      animate('0.2s ease-in-out', style({opacity: 1}))
    ]),
    transition(':leave', [
      style({opacity: 1}),
      animate('0.2s ease-in-out', style({opacity: 0}))
    ])
  ]);
}

export function slideToBottom() {
  return trigger('routerTransition', [
    state('void', style({position: 'fixed', width: '100%', height: '100%'})),
    state('*', style({position: 'fixed', width: '100%', height: '100%'})),
    transition(':enter', [
      style({transform: 'translateY(-100%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateY(0%)'}))
    ]),
    transition(':leave', [
      style({transform: 'translateY(0%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateY(100%)'}))
    ])
  ]);
}

export function slideToTop() {
  return trigger('routerTransition', [
    state('void', style({position: 'fixed', width: '100%', height: '100%'})),
    state('*', style({position: 'fixed', width: '100%', height: '100%'})),
    transition(':enter', [
      style({transform: 'translateY(100%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateY(0%)'}))
    ]),
    transition(':leave', [
      style({transform: 'translateY(0%)'}),
      animate('0.5s ease-in-out', style({transform: 'translateY(-100%)'}))
    ])
  ]);
}
