export interface IUser {
  first_name: string;
  last_name: string;
  full_name: string;
  email: string;
  mobile_number: number;
  holding_company?: number;
  site: number;
  site_name?: string;
  is_active: boolean;
  is_staff: boolean;
  suspended: boolean;
  designation: number;
  id?: number;
}
