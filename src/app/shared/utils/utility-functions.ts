import { IShift, IShiftTotal } from '../../views/daily-recon/shifts-recon/models/shift';
import { IDateModel } from '../../layout/layout.component';
import { DEPOSIT_TYPES } from '../constants/deposits-types';

export class UtilityFunctions {

  static addUpAmounts(listOfFields: Array<any>): number {
    return listOfFields.reduce(function (acc: number, val: any) {
      return acc + +val.amount;
    }, 0);
  }

  static underscoreify(str: string): string {
    return str.replace(/-/g, '_');
  }

  // Ben's formula Sales - Cash Payout + Cash Payin + Rec On Acc - Acc Sales - Deposits (excl EFT deposit)
  static getTotalShortsArray(data: IShift) {

    // clsing - opening
    const movement = data.float_control.closing_float - data.float_control.opening_float;

    const totalDeps: number = DEPOSIT_TYPES.map((itm: string) => {
      if (data[itm]) {
        return this.addUpAmounts(data[itm]);
      } else {
        return 0;
      }
    }).reduce((acc: number, val: number) => {
      return acc + val;
    });

    let total = UtilityFunctions.addUpAmounts(data.sales)
      - this.addUpAmounts(data.cash_payouts)
      + this.addUpAmounts(data.cash_payins)
      + this.addUpAmounts(data.received_on_account)
      - this.addUpAmounts(data.account_sales)
      - totalDeps;

    total -= movement;
    return (Math.round((total ) * 100) / 100);
  }

  static getTotalShortsSimple(data: IShiftTotal) {

    // clsing - opening
    const movement = data.float_control;

    const totalDeps: number = DEPOSIT_TYPES.map((itm: string) => {
      if (data[itm]) {
        return data[itm];
      } else {
        return 0;
      }
    }).reduce((acc: number, val: number) => {
      return acc + val;
    });

    let total = UtilityFunctions.addUpAmounts(data.sales)
      - data.cash_payouts
      + data.cash_payins
      + data.received_on_account
      - data.account_sales
      - totalDeps;

    total -= movement;
    return (Math.round((total ) * 100) / 100);
  }

  static getDateModel(date: Date): IDateModel {
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();
    return {date: {year: year, month: month, day: day}};
  }

  static getDateStringFromDate(date: Date): string {
    return `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
  }

  static enumSelector(enumDef) {
    return Object.keys(enumDef)
      .map(key => ({value: enumDef[key], title: key}))
      .filter(itm => {
        return typeof itm.value === 'number';
      });
  }

  static rounder(n: number): number {
    return (Math.round((n ) * 100) / 100);
  }

  static parseErrs(errBody: string, errStatus: number): string {
    console.log(errBody);
    let htmlStrng = '';
    let errObj;
    if (errStatus === 404) {
      htmlStrng = 'The requested resource could not be found';
    } else {
      try {
        errObj = JSON.parse(errBody);
      } catch (e) {
        console.log(e);
      }
    }

    if (errObj) {
      Object.keys(errObj).forEach(errField => {
        // todo, there may be more than one error per field
        if (Array.isArray(errField)) {
          errField = errField[0];
        }
        htmlStrng += `<span>${errField}:  ${errObj[errField]}</span><br>`;
      });
    }

    return htmlStrng;
  }

}
