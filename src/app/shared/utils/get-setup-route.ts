export function getSetupRoute(arrOfReqs: Array<string>) {
  const first = arrOfReqs[0];
  if (first.indexOf('SUPPLIER') !== -1) {
    return 'suppliers'
  } else if (first.indexOf('ACCOUNT') !== -1) {
    return 'debtors'
  } else if (first.indexOf('CASHIER') !== -1) {
    return 'cashiers'
  } else {
    return 'categories'
  }
}
