import { IEODSubCategory } from '../../views/daily-recon/eod-recon/models/eod';

export class CalcFunctions {

  static addVATtoAmnt(subcat: IEODSubCategory, vat: string): number {
    // returns non rounded
    if (+subcat.incl) {
      return +subcat.incl;
    } else {
      const amount = +subcat.amount;
      const vatDecimal = +vat / 100;
      return vatDecimal > 0 ? amount * (vatDecimal + 1) : amount;
    }
  }

}
