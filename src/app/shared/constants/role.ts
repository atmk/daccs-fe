export const ROLES: Array<{ title: string, id: number }> = [
  {title: 'Administrator', id: 0},
  {title: 'Accountant', id: 1},
  {title: 'Site manager',  id: 2},
  {title: 'Assistant manager', id: 3},
  {title: 'Report Viewer',  id: 4},
  {title: 'Cashier',id: 5},
];