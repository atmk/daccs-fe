export const MONTHS = [
  {val: 1, name: 'Jan', longName: 'January'},
  {val: 2, name: 'Feb', longName: 'February'},
  {val: 3, name: 'Mar', longName: 'March'},
  {val: 4, name: 'Apr', longName: 'April'},
  {val: 5, name: 'May', longName: 'May'},
  {val: 6, name: 'Jun', longName: 'June'},
  {val: 7, name: 'Jul', longName: 'July'},
  {val: 8, name: 'Aug', longName: 'August'},
  {val: 9, name: 'Sep', longName: 'September'},
  {val: 10, name: 'Oct', longName: 'October'},
  {val: 11, name: 'Nov', longName: 'November'},
  {val: 12, name: 'Dec', longName: 'December'}
];
