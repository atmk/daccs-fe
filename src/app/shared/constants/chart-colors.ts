// colours are from the Clarity pallete

export const CHART_COLORS = [
  ['#60B515', '#AADB1E'],
  ['#FF8400', '#FFB565'],
  ['#49AFD9', '#A6D8E7'],
  ['#F1428A', '#F897BF'],
  ['#919FA8', '#C1CDD4'],
  ['#F76F6C', '#F8B7B6'],
  ['#00B7D6', '#6DDBEB'],
  ['#AD73C8', '#D0ACE4'],
  ['#FDD006', '#FFE860'],
  ['#00D4B8', '#6FEAD9'],
];

export const ACTIVE_BLUE_CHART = ['#0094d2', '#49afd9'];
