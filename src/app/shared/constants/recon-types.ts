export const RECON_TYPES: Array<string> = [
  'cash_payouts',
  'cash_payins',
  'received_on_account',
  'account_sales',
  'float_control'
];
