export const DEPOSIT_TYPES: Array<string> = [
  'cash_deposits',
  'cash_on_hand_deposits',
  'speedpoint_deposits',
  'check_deposits',
  'snapscan_deposits',
  'zapper_deposits',
  'voucher_deposits',
  'wigroup_deposits',
  'fuel_card_deposits',
  'eft_deposits',
];
