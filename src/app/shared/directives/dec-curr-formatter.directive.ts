import { AfterViewInit, Directive, ElementRef, HostListener, Input } from '@angular/core';
import { DaccsCurrencyPipe } from '../pipes/currency.pipe';

@Directive({
  selector: '[daccsDecCurrFormatter]'
})
export class DecimalCurrencyFormatterDirective implements AfterViewInit {

  // adapted from : https://stackoverflow.com/questions/41806383/limit-input-type-number-to-2-place-of-decimal-in-angular-2

  constructor(private _el: ElementRef, private _currencyPipe: DaccsCurrencyPipe) {
  }

  @Input() onlyNumber: boolean;
  @Input() decimalPlaces: string;
  @Input() minValue: string;
  @Input() maxValue: string;
  @Input() multiField: boolean;

  private _selectedField: string;

  @HostListener('keydown', ['$event'])
  onKeyDown(event) {
    const e = <KeyboardEvent> event;
    if (this.onlyNumber) {
      // 109, 189, are keycodes for minus/negative
      if ([46, 8, 9, 27, 13, 109, 189, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode === 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode === 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode === 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
  }

  @HostListener('keypress', ['$event'])
  onKeyPress(event) {
    const e = <any> event;

    const valInFloat: number = parseFloat(e.target.value);

    if (this.minValue.length) {
      // (isNaN(valInFloat) && e.key === "0") - When user enters value for first time valInFloat will be NaN, e.key condition is
      // because I didn't want user to enter anything below 1.
      // NOTE: You might want to remove it if you want to accept 0
      if (valInFloat < parseFloat(this.minValue)) {
        e.preventDefault();
      }
    }

    if (this.maxValue.length) {
      if (valInFloat > parseFloat(this.maxValue)) {
        e.preventDefault();
      }
    }

    if (this.decimalPlaces) {
      let currentCursorPos = -1;
      if (typeof this._el.nativeElement.selectionStart === 'number') {
        currentCursorPos = this._el.nativeElement.selectionStart;
      } else {
        // Probably an old IE browser
        console.log('This browser doesn\'t support selectionStart');
      }

      if (currentCursorPos > 0 && e.key === '-') {
        e.preventDefault();
      }

      const lastChar = e.target.value[e.target.value.length - 1];
      const secondLastChar = e.target.value[e.target.value.length - 2];
      if ((e.key === '.' || e.key === '-') && (e.key === lastChar || e.key === secondLastChar)) {
        e.preventDefault();
      }

      const dotLength: number = e.target.value.replace(/[^\.]/g, '').length;
      // If user has not entered a dot(.) e.target.value.split(".")[1] will be undefined
      const decimalLength = e.target.value.split('.')[1] ? e.target.value.split('.')[1].length : 0;

      // (this.DecimalPlaces - 1) because we don't get decimalLength including currently pressed character
      // currentCursorPos > e.target.value.indexOf(".") because we must allow user's to enter value before dot(.)
      // Checking Backspace etc.. keys because firefox doesn't pressing them while chrome does by default
      if (dotLength > 1 || (decimalLength > (parseInt(this.decimalPlaces) - 1) &&
          currentCursorPos > e.target.value.indexOf('.')) && ['Backspace', 'ArrowLeft', 'ArrowRight'].indexOf(e.key) === -1) {
        e.preventDefault();
      }
    }
  }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(value) {
    if (this.multiField) {
      if (!this._selectedField) {
        this._el.nativeElement.value = this._currencyPipe.transform(value);
      }
    }
  }

  @HostListener('focusin', ['$event.target.name'])
  onTouch() {
    if (this.multiField) {
      this._selectedField = this._el.nativeElement.id;
    }
  }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value) {
    if (this.onlyNumber) {
      this._el.nativeElement.value = this._currencyPipe.parse(value);
    }
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    if (this.onlyNumber) {
      this._el.nativeElement.value = this._currencyPipe.transform(value);
    }
    if (this.multiField) {
      this._el.nativeElement.value = this._currencyPipe.transform(value);
      this._selectedField = undefined;
    }
  }

  ngAfterViewInit(): void {
    if (this.onlyNumber && !this.multiField) {
      this._el.nativeElement.value = this._currencyPipe.transform(this._el.nativeElement.value);
    }
  }
}
