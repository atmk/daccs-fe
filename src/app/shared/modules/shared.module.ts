import { ModuleWithProviders, NgModule } from '@angular/core';
import { DaccsCurrencyPipe } from '../pipes/currency.pipe';
import { TitleCasePipe } from '../pipes/title-case.pipe';
import { OrderByPipe } from '../pipes/order-by.pipe';
import { SelectModule } from '../components/select-filter/ng-select';
import { MultiOrderPipe } from '../pipes/multi-order.pipe';
import { PrettyDatePipe } from '../pipes/pretty-date.pipe';
import { UnslugPipe } from '../pipes/unslug.pipe';
import { DecimalCurrencyFormatterDirective } from '../directives/dec-curr-formatter.directive';

@NgModule({
  imports: [SelectModule],
  declarations: [
    DaccsCurrencyPipe,
    TitleCasePipe,
    OrderByPipe,
    MultiOrderPipe,
    PrettyDatePipe,
    UnslugPipe,
    DecimalCurrencyFormatterDirective
  ],
  exports: [
    DaccsCurrencyPipe,
    TitleCasePipe,
    OrderByPipe,
    SelectModule,
    MultiOrderPipe,
    PrettyDatePipe,
    UnslugPipe,
    DecimalCurrencyFormatterDirective
  ],
  providers: [DaccsCurrencyPipe]
})
export class SharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }

}
