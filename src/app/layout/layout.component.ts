import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { IAppStore } from '../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { AuthService } from '../auth/auth.service';
import { IMyDateModel, IMyDpOptions, IMyOptions } from 'mydatepicker';
import { UtilityFunctions } from '../shared/utils/utility-functions';
import { Observable } from 'rxjs/Observable';
import { IBrokenCon, InitializerService, ISetupReq } from '../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';
import { IOption } from '../shared/components/select-filter/option.interface';
import { SiteProfileService } from '../initialiaze/services/site-profile.service';
import { ISite } from '../views/site-admin/company/sites/models/site';
import { IUser } from '../shared/models/user';
import 'rxjs/add/operator/takeWhile';

export interface IDateModel {
  date: { year: number, month: number, day: number };
}

class CustOptions implements IOption {
  value: any;
  label: string;
  logo?: string;
}

@Component({
  selector: 'daccs-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() public loading: boolean;
  @Output() dateChangedEvent: EventEmitter<string> = new EventEmitter();
  @Output() manualSocketReconnect: EventEmitter<{ newCon: boolean, date: Date }> = new EventEmitter();

  public currDate: Date;
  public now: Date = new Date();

  public prod = environment.production;

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'yyyy-mm-dd',
    showIncreaseDateBtn: true,
    showDecreaseDateBtn: true,
    showClearDateBtn: false,
    editableDateField: false,
    showTodayBtn: false,
    height: '24px',
    disableSince: {
      year: this.now.getFullYear(),
      month: this.now.getMonth() + 1,
      day: this.now.getDate()
    }
  };

  public dateModel: IDateModel;
  public prodEnv: boolean;
  public loaderSVG = 'assets/images/ui/loader.svg';
  public siteProfile: ISite;
  public siteAccessSites: Array<CustOptions>;
  public setupRequired$: Observable<ISetupReq>;
  public me$: Observable<IUser>;
  public selectedSiteId: number;
  public brokenConn: IBrokenCon;
  public countDown: Observable<number>;

  private _profileSub: Subscription;
  private _conBrokenSub: Subscription;
  public logoisLoaded = false;
  public defaultLogo = 'assets/images/eagle.png';
  public brokenImage = false;

  constructor(private _store: Store<IAppStore>,
              private _siteProfileService: SiteProfileService,
              private _initializerService: InitializerService,
              private _authService: AuthService) {

    if (environment.production) {
      this.currDate = new Date();
      this.currDate.setDate(this.currDate.getDate() - 1);
    } else {
      this.currDate = new Date(2017, 11 - 1, 5);
    }

    this.prodEnv = environment.production;
    this._profileSub = this._siteProfileService.siteProfile$.subscribe((data: ISite) => {
      this.siteProfile = data;
      if (data && data.access_sites) {
        this.selectedSiteId = data._id_;

        this.siteAccessSites = data.access_sites.map((site: ISite) => {
          return {
            label: site.holding_company_title + ' | ' + site.title,
            value: site._id_,
            logo: site.franchise_logo_url
          };
        });
      }
    });
    this.setupRequired$ = this._initializerService.setupRequired$;

    this._conBrokenSub = this._initializerService.conbroken$.subscribe((val: IBrokenCon) => {
      this.brokenConn = val;
      if (val.broken) {
        this._countdown(val.timeout);
      }
    });

    this.me$ = this._store.select('meReducer');
  }

  public reconnect() {
    console.log(this.currDate);
    this.manualSocketReconnect.emit({newCon: true, date: this.currDate});
  }

  private _countdown(secs: number): void {
    const count = secs / 1000;
    this.countDown = Observable.timer(0, 1000)
      .map(value => count - value)
      .takeWhile(value => value > 0);
  }

  public onDateChange(ev: IMyDateModel): void {
    if (ev.jsdate) {
      if (ev.jsdate.getTime() !== this.currDate.getTime()) {
        this.currDate = ev.jsdate;
        this.dateChangedEvent.emit(ev.formatted);
      }
      this._checkDate(ev.jsdate);
    }
  }

  public siteSwitch(ev: IOption): void {
    console.log(ev);

    this._siteProfileService.switchSite(ev.value);
  }

  public logout() {
    this._authService.logout();
  }

  public logState(): void {
    console.log(environment);
    console.log(this._getState(this._store));
  }

  public logoLoaded() {
    this.logoisLoaded = true;
  }

  private _getState(store: Store<IAppStore>): IAppStore {
    let state;
    store.subscribe(s => state = s);
    return state;
  }

  private _checkDate(curr): void {
    const yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
    const currDate = Date.UTC(curr.getFullYear(), curr.getMonth(), curr.getDate());
    const todaydate = Date.UTC(yesterday.getFullYear(), yesterday.getMonth(), yesterday.getDate());
    const ms = currDate - todaydate;
    const diffInDays = Math.floor(ms / 1000 / 60 / 60 / 24);
    const newOptions = this._getCopyOfOptions();
    newOptions.showIncreaseDateBtn = diffInDays <= -1; // minus one so we disable today forward.  todo: clean this up
    this.myDatePickerOptions = newOptions;
  }

  private _getCopyOfOptions(): IMyOptions {
    return JSON.parse(JSON.stringify(this.myDatePickerOptions));
  }

  private _timeOutLogoLoad() {
    setTimeout(() => {
      if (!this.logoisLoaded) {
        this.brokenImage = true;
      }
    }, 5000);
  }

  ngOnInit(): void {
    this._checkDate(this.currDate);
    this.dateModel = UtilityFunctions.getDateModel(this.currDate);
  }

  ngOnDestroy(): void {
    this._conBrokenSub.unsubscribe();
    this._profileSub.unsubscribe();
  }

  ngAfterViewInit() {
    this._timeOutLogoLoad();
  }
}
