import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EODReconComponent } from './views/daily-recon/eod-recon/eod-recon.component';
import { ShiftsReconComponent } from './views/daily-recon/shifts-recon/shifts-recon.component';
import { DailyReconComponent } from './views/daily-recon/daily-recon.component';
import { ShiftControllerComponent } from './views/daily-recon/shifts-recon/controller/shift-controller.component';
import { ShiftRouteRedirectComponent } from './views/daily-recon/shifts-recon/controller/shift-route-redirect.component';
import { SalesEodContainerComponent } from './views/daily-recon/eod-recon/components/sales-eod-container/sales-eod-container.component';
import { DepositsShiftsContainerComponent } from './views/daily-recon/shifts-recon/components/deposits-shifts-container/deposits-shifts-container.component';
import { EodSalesRouteRedirectComponent } from './views/daily-recon/eod-recon/controller/eod-sales-route-redirect.component';
import { DepositsEodContainerComponent } from './views/daily-recon/eod-recon/components/deposits-eod-container/deposits-eod-container.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { InitialiazeComponent } from './initialiaze/initialiaze.component';
import { EodControllerComponent } from './views/daily-recon/eod-recon/controller/eod-controller.component';
import { FormSalesEodComponent } from './views/daily-recon/eod-recon/components/sales-eod-container/form-sales-eod/form-sales-eod.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { RouteInfoComponent } from './views/daily-recon/shifts-recon/components/route-info/route-info.component';
import { NoContentComponent } from './layout/no-content/no-content.component';
import { ConfirmDeactivateGuard } from './shared/guards/confirm.guard';
import { MyProfileComponent } from './views/site-admin/preferences/my-profile/my-profile.component';
import { SitesComponent } from './views/site-admin/company/sites/sites.component';
import { CompanySettingsComponent } from './views/site-admin/company/company-settings/company-settings.component';
import { CatSetupComponent } from './views/site-admin/site-setup/cat-setup/cat-setup.component';
import { GenLedgerSetupComponent } from './views/site-admin/site-setup/gen-ledger-setup/gen-ledger-setup.component';
import { CashiersSetupComponent } from './views/site-admin/site-setup/cashiers-setup/cashiers-setup.component';
import { AccHoldersSetupComponent } from './views/site-admin/site-setup/acc-holders-setup/acc-holders-setup.component';
import { SuppliersSetupComponent } from './views/site-admin/site-setup/suppliers-setup/suppliers-setup.component';
import { SetupDoneGuard } from './shared/guards/setup-required.guard';
import { SiteNewComponent } from './views/site-admin/company/sites/site-new/site-new.component';
import { SiteUpdateComponent } from './views/site-admin/company/sites/site-update/site-update.component';
import { ReconHistoryComponent } from './views/daily-recon/recon-history/recon-history.component';
import { CheckSetupComponent } from './views/check-setup/check-setup.component';
import { SpeedpointSetupComponent } from './views/site-admin/site-setup/speedpoint-setup/speedpoint-setup.component';
import { MonthlyCumulativeComponent } from './views/reports/monthly-cumulative/monthly-cumulative.component';
import { UsersComponent } from './views/site-admin/company/users/users.component';
import { UserUpdateComponent } from './views/site-admin/company/users/user-update/user-update.component';
import { SummaryComponent } from './views/reports/summary/summary.component';
import { UserNewComponent } from './views/site-admin/company/users/user-new/user-new.component';
import { NotificationsComponent } from './views/site-admin/company/notifications/notifications.component';
import { GenerateBatchComponent } from './views/batch/generate-batch/generate-batch.component';
import { FilesComponent } from './views/daily-recon/files/files.component';

const routes: Routes = [
  {
    canActivateChild: [AuthGuard],
    path: '', component: InitialiazeComponent,
    children: [
      {path: '', redirectTo: 'dash', pathMatch: 'full'},
      {
        path: 'dash',
        component: DashboardComponent
      },
      {
        path: 'check-setup',
        component: CheckSetupComponent
      },
      {
        path: 'recon-history',
        component: ReconHistoryComponent,
      },
      {
        path: 'files',
        component: FilesComponent,
      },
      {
        path: 'daily-recon',
        component: DailyReconComponent,
        canActivate: [SetupDoneGuard],
        children: [
          {
            path: 'shifts',
            component: ShiftsReconComponent,
            children: [
              {path: '', component: ShiftRouteRedirectComponent, data: {isSubRoot: true}},
              {path: 'sales/:shift_id', component: ShiftControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'sales', component: RouteInfoComponent},
              {
                path: 'cash-payouts/:shift_id',
                component: ShiftControllerComponent,
                canDeactivate: [ConfirmDeactivateGuard]
              },
              {path: 'cash-payouts', component: RouteInfoComponent},
              {
                path: 'cash-payins/:shift_id',
                component: ShiftControllerComponent,
                canDeactivate: [ConfirmDeactivateGuard]
              },
              {path: 'cash-payins', component: RouteInfoComponent},
              {
                path: 'received-on-account/:shift_id',
                component: ShiftControllerComponent,
                canDeactivate: [ConfirmDeactivateGuard]
              },
              {path: 'received-on-account', component: RouteInfoComponent},
              {
                path: 'account-sales/:shift_id',
                component: ShiftControllerComponent,
                canDeactivate: [ConfirmDeactivateGuard]
              },
              {path: 'account-sales', component: RouteInfoComponent},
              {path: 'shorts/:shift_id', component: ShiftControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'shorts', component: RouteInfoComponent},
              {
                path: 'float-control/:shift_id',
                component: ShiftControllerComponent,
                canDeactivate: [ConfirmDeactivateGuard]
              },
              {path: 'float-control', component: RouteInfoComponent},
              {
                path: 'deposits',
                component: DepositsShiftsContainerComponent,
                children: [
                  {path: '', component: ShiftRouteRedirectComponent, data: {isDep: true}},
                  {
                    path: 'cash-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'cash-on-hand-deposits', component: RouteInfoComponent},
                  {
                    path: 'cash-on-hand-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'cash-deposits', component: RouteInfoComponent},
                  {
                    path: 'speedpoint-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'speedpoint-deposits', component: RouteInfoComponent},
                  {
                    path: 'check-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'check-deposits', component: RouteInfoComponent},
                  {
                    path: 'snapscan-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'snapscan-deposits', component: RouteInfoComponent},
                  {
                    path: 'zapper-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'zapper-deposits', component: RouteInfoComponent},
                  {
                    path: 'voucher-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {
                    path: 'wigroup-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {
                    path: 'fuel-card-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'voucher-deposits', component: RouteInfoComponent},
                  {
                    path: 'eft-deposits/:shift_id',
                    component: ShiftControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'eft-deposits', component: RouteInfoComponent},
                ]
              }
            ]
          },
          {
            path: 'eod',
            component: EODReconComponent,
            children: [
              {path: '', redirectTo: 'sales', pathMatch: 'full'},
              {
                path: 'sales',
                component: SalesEodContainerComponent,
                children: [
                  {path: '', component: EodSalesRouteRedirectComponent},
                  {
                    path: 'itm/:sales_itm_id',
                    component: FormSalesEodComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                ]
              },
              {path: 'cash-payouts', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'cash-payins', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'received-on-account', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'account-sales', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {
                path: 'deposits',
                component: DepositsEodContainerComponent,
                children: [
                  {path: '', redirectTo: 'cash-deposits', pathMatch: 'full'},
                  {path: 'cash-deposits', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
                  {
                    path: 'cash-on-hand-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {
                    path: 'speedpoint-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'check-deposits', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
                  {
                    path: 'snapscan-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'zapper-deposits', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
                  {
                    path: 'voucher-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {
                    path: 'wigroup-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                  {path: 'eft-deposits', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
                  {
                    path: 'fuel-card-deposits',
                    component: EodControllerComponent,
                    canDeactivate: [ConfirmDeactivateGuard]
                  },
                ]
              },
              {path: 'float-control', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
              {path: 'shorts', component: EodControllerComponent, canDeactivate: [ConfirmDeactivateGuard]},
            ]
          }
        ]
      },
      {
        path: 'preferences/my-profile',
        component: MyProfileComponent
      },
      {
        path: 'company/company-settings',
        component: CompanySettingsComponent
      },
      {path: 'company/sites/new', component: SiteNewComponent},
      {path: 'company/sites/update/:site_id', component: SiteUpdateComponent},
      {path: 'company/sites', component: SitesComponent},
      {path: 'company/users/new', component: UserNewComponent},
      {path: 'company/users/update/:user_id', component: UserUpdateComponent},
      {path: 'company/users', component: UsersComponent},
      {path: 'company/notifications', component: NotificationsComponent},
      {path: 'batch/generate-batch', component: GenerateBatchComponent},
      {
        path: 'setup/categories',
        component: CatSetupComponent
      },
      {
        path: 'setup/ledgers',
        component: GenLedgerSetupComponent
      },
      {
        path: 'setup/cashiers',
        component: CashiersSetupComponent
      },
      {
        path: 'setup/debtors',
        component: AccHoldersSetupComponent
      },
      {
        path: 'setup/suppliers',
        component: SuppliersSetupComponent
      },
      {
        path: 'setup/speedpoints',
        component: SpeedpointSetupComponent
      },
      {
        path: 'reports/monthly-cumulative',
        component: MonthlyCumulativeComponent
      },
      {
        path: 'reports/summary',
        component: SummaryComponent
      },
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {path: '**', component: NoContentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
