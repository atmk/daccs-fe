import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/interval';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  public token: string;
  private _apiBaseUrl = environment.apiBaseUrl;
  private _jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: Http, private _router: Router) {
    // set token if saved in local storage
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public isLoggedIn() {
    return tokenNotExpired();
  }

  public login(email: string, password: string): Observable<boolean> {

    const contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');

    return this.http.post(`${this._apiBaseUrl}/api-token-auth/`, JSON.stringify({
      email: email,
      password: password
    }), {headers: contentHeaders})
      .map((response: Response) => {
        const token = response.json() && response.json().token;
        if (token) {
          this.token = token;

          localStorage.setItem('currentUser', JSON.stringify({email: email}));
          localStorage.setItem('token', token);
          return true;
        } else {
          return false;
        }
      });
  }

  public passwordReset(email: string): Observable<boolean> {

    const contentHeaders = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');

    return this.http.post(`${this._apiBaseUrl}/users/user/password/reset/request/`, JSON.stringify({
      email: email
    }), {headers: contentHeaders})
      .map((response: Response) => {
        console.log(response);
        const res = response.json();
        return res.success;
      });
  }

  public logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this._router.navigateByUrl('/login');
  }

  public tokenIsExpired(): Observable<any> {
    return Observable.interval(1000).map(() => {
      if (this.getToken()) {
        return this._jwtHelper.isTokenExpired(this.getToken());
      }
      return true;
    });
  }
}
