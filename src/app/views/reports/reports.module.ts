import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonthlyCumulativeComponent } from './monthly-cumulative/monthly-cumulative.component';
import { ReportsService } from './services/reports.service';
import { FormsModule } from '@angular/forms';
import { SummaryComponent } from './summary/summary.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { CellFormatPipe } from './pipes/cell-format.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule
  ],
  declarations: [MonthlyCumulativeComponent, SummaryComponent, CellFormatPipe],
  providers: [ReportsService]
})
export class ReportsModule {
}
