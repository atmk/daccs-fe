import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cellFormat'
})
export class CellFormatPipe implements PipeTransform {

  transform(value: string, formatType: any): string {
    if (isNaN(+value)) {
      return value;
    } else if (formatType === 'money') {
      return `R ${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`
    } else if (formatType === 'liters') {
      return `${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}`
    } else {
      return value;
    }
  }

}
