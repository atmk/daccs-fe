import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { IEODData, IEODSubCategory, ISalesEODItem } from '../../daily-recon/eod-recon/models/eod';
import { IShift } from '../../daily-recon/shifts-recon/models/shift';
import { Observable } from 'rxjs/Observable';
import { ShiftsReconService } from '../../daily-recon/shifts-recon/services/shifts-recon.service';
import { EODReconService } from '../../daily-recon/eod-recon/services/eod-recon.service';
import { IHeaderDataItem, IShort } from '../../daily-recon/live-header/models/live-sales-data';
import { UtilityFunctions } from '../../../shared/utils/utility-functions';
import { ITallyData, LiveTallyService } from '../../daily-recon/live-header/services/live-tally.service';
import 'rxjs/add/observable/combineLatest';

type CombinedData = [IShift[], IEODData];

interface IGenTally {
  cashierAmount: number;
  eodAmount: number;
  diffAmount: number;
  discAmount?: number;
}

interface IWetTally {
  liters: number;
  amount: number;
}

interface IMoveTally {
  [key: string]: IHeaderDataItem;
}

@Component({
  selector: 'daccs-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  providers: [LiveTallyService]
})
export class SummaryComponent implements OnInit, OnDestroy {

  public EODData$: Observable<IEODData>;
  public shiftsData$: Observable<Array<IShift>>;
  public reportData$: Observable<[IShift[], IEODData]>;
  public wetStock: Array<IEODSubCategory> = [];
  public salesTotals: IGenTally;
  public depTotals: IGenTally;
  public movementTotals: IGenTally;
  public wetStockTotals: IWetTally;
  public shortsData: IShort;
  public balance: IGenTally;
  public tallied: ITallyData;
  public totalDiscrep: number;
  public grandCheck: number;

  private _data: CombinedData;
  private _dataSub: Subscription;

  constructor(private _EODReconService: EODReconService,
              private _shiftsReconService: ShiftsReconService,
              private _liveTallyService: LiveTallyService) {
    this.shiftsData$ = this._shiftsReconService.shifts$;
    this.EODData$ = this._EODReconService.EODData$;

    this.reportData$ = Observable.combineLatest(this.shiftsData$, this.EODData$);

    this._dataSub = this.reportData$.subscribe((val: [IShift[], IEODData]) => this._data = val);

  }

  private _getWetSTockTotals(arr: Array<IEODSubCategory>): IWetTally {
    return arr.reduce((mem, itm) => {
      mem['liters'] = UtilityFunctions.rounder(mem['liters'] + +itm['liters']);
      mem['amount'] = UtilityFunctions.rounder(mem['amount'] + +itm['amount']);
      return mem;
    }, {liters: 0, amount: 0});
  }

  private _getGenTotals(arr: Array<IHeaderDataItem>): IGenTally {
    return arr.reduce((mem, itm) => {
      mem['cashierAmount'] = UtilityFunctions.rounder(mem['cashierAmount'] + +itm['cashierAmount']);
      mem['eodAmount'] = UtilityFunctions.rounder(mem['eodAmount'] + +itm['eodAmount']);
      mem['diffAmount'] = UtilityFunctions.rounder(mem['diffAmount'] + +itm['diffAmount']);
      if (!itm.discAmount || isNaN(itm.discAmount)) {
        itm.discAmount = 0;
      }
      mem['discAmount'] = UtilityFunctions.rounder(mem['discAmount'] + +itm['discAmount']);
      return mem;
    }, {cashierAmount: 0, eodAmount: 0, diffAmount: 0, discAmount: 0});
  }

  private _getBalance(salesTotal: IGenTally, depTotal: IGenTally): IGenTally {
    return {
      cashierAmount: UtilityFunctions.rounder(salesTotal.cashierAmount - depTotal.cashierAmount),
      eodAmount: UtilityFunctions.rounder(salesTotal.eodAmount - depTotal.eodAmount),
      diffAmount: UtilityFunctions.rounder(salesTotal.diffAmount - depTotal.diffAmount)
    };
  }

  private _getTotalDiscrep(salesTotal: IGenTally, depTotal: IGenTally, moveTotal: IGenTally): any {
    return UtilityFunctions.rounder(salesTotal.discAmount + depTotal.discAmount + moveTotal.discAmount);
  }

  private _getMovementTotals(arr: Array<IHeaderDataItem>): any {
    const totalObj: IMoveTally = <IMoveTally>{};
    const totals = ['cashierAmount', 'eodAmount', 'diffAmount', 'discAmount'];
    const talliedObject: IGenTally = {cashierAmount: 0, eodAmount: 0, diffAmount: 0, discAmount: 0};

    arr.forEach((itm: IHeaderDataItem) => {
      totalObj[itm.title] = itm;
    });

    totals.forEach((itm) => {
      talliedObject[itm] = UtilityFunctions.rounder(
        totalObj.cash_payouts[itm]
        - totalObj.cash_payins[itm]
        - totalObj.received_on_account[itm]
        + totalObj.account_sales[itm]
        - totalObj.float_control[itm]
      );
    });

    return talliedObject;
  }

  ngOnInit(): void {
    this.tallied = this._liveTallyService.getTallyObject(this._data[1], this._data[0]);

    const cats = this._data[1].sales.find((itm: ISalesEODItem) => {
      return itm.category_type === 0;
    });

    if (cats && cats.hasOwnProperty('categories')) {
      cats.categories.forEach(itm => {
        itm.sub_categories.forEach(subItm => {
          this.wetStock.push(subItm);
        });
      });
    }

    if (this.wetStock) {
      this.wetStockTotals = this._getWetSTockTotals(this.wetStock);
    }

    this.salesTotals = this._getGenTotals(this.tallied.salesDataArr);
    this.depTotals = this._getGenTotals(this.tallied.depositData);
    this.movementTotals = this._getMovementTotals(this.tallied.reconData);
    this.balance = this._getBalance(this.salesTotals, this.depTotals);
    this.shortsData = this.tallied.shortDataShifts;
    this.shortsData['variance'] = UtilityFunctions.rounder(this.tallied.shortDataShifts.system
      - this.tallied.shortDataShifts.explained);
    this.totalDiscrep = this._getTotalDiscrep(this.salesTotals, this.depTotals, this.movementTotals);
    this.grandCheck = UtilityFunctions.rounder(this.tallied.shortDataShifts.system
      - this.tallied.shortDataEOD);
  }

  ngOnDestroy(): void {
    this._dataSub.unsubscribe();
  }

}
