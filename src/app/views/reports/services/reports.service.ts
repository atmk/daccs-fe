import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../environments/environment';

export interface IDailyCumulativeData {
  // headers: Array<string>;
  // data: Array<Array<string>>
  date_created: string;
  meta_data: string;
  month: number;
  report: string;
  report_name: string;
  site: number;
  year: number
}

export interface IDailyRowData {
  headers: Array<string>;
  rows: Array<Array<string>>
}

@Injectable()
export class ReportsService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public loadMonthlyCumulativeReport(year: number, month: number): Observable<IDailyCumulativeData> {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/cumulative-report/${year.toString()}/${month}/`)
      .map(res => res.json())
  }
}
