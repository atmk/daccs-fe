import { Component, OnDestroy, OnInit } from '@angular/core';
import { IDailyCumulativeData, IDailyRowData, ReportsService } from '../services/reports.service';
import { INITIAL_YEAR } from '../../../shared/constants/reporting.constant';
import { InitializerService } from '../../../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';
import { UtilityFunctions } from '../../../shared/utils/utility-functions';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { MONTHS } from '../../../shared/constants/months';

@Component({
  selector: 'daccs-daily-cumulative',
  templateUrl: './monthly-cumulative.component.html',
  styleUrls: ['./monthly-cumulative.component.scss']
})
export class MonthlyCumulativeComponent implements OnInit, OnDestroy {

  public reportData: IDailyRowData;

  public months: Array<{ val: number, name: string, longName?: string }>;
  public mm: number;
  public years: Array<number> = [];
  public yy: number;
  public reportMonth: { month: number, year: number };
  public totalArr: Array<number> = [];
  public busy = false;
  public noReport = false;
  public indexOfLitersCol: number;

  private _selectedDate: Date;
  private _dateSub: Subscription;

  constructor(private _reportsService: ReportsService,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService) {
    this.months = MONTHS;
    this._dateSub = this._initializerService.currDate$.subscribe(val => {
      this._selectedDate = new Date(val);
    })
  }

  public onSubmit(body: { month: number, year: number }, valid) {
    this.reportData = null;
    this.totalArr = [];
    this._getReportData(body.year, body.month)
  }

  private _getMonth(): void {
    this.mm = +(this._selectedDate.getMonth() + 1).toString().slice(-2);
  }

  private _getYear(): void {
    this.yy = this._selectedDate.getFullYear();
    for (let i = INITIAL_YEAR; i <= this.yy; i++) {
      this.years.push(i);
    }
  }

  private _getTotals(data: Array<Array<string>>, headerLen: number): void {
    for (let i = 1; i < headerLen; i++) {
      const total = data.reduce((acc: number, itm: any) => {
        return acc + +itm[i];
      }, 0);
      this.totalArr.push(UtilityFunctions.rounder(total));
    }
  }

  private _getReportData(year: number, month: number): void {
    this.busy = true;
    this.noReport = false;
    this._reportsService.loadMonthlyCumulativeReport(year, month)
      .subscribe((val: IDailyCumulativeData) => {
        this.busy = false;
        this.reportData = JSON.parse(val.meta_data);
        this._getTotals(this.reportData.rows, this.reportData.headers.length);
        this.indexOfLitersCol = this.reportData.headers.indexOf('Liters');
      }, err => {

        console.log(err);
        if (err.status === 404) {
          this.noReport = true
        }

        const toast: Toast = {
          type: 'error',
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
        this.busy = false;
      });
  }

  ngOnInit(): void {
    this._getMonth();
    this._getYear();

    this.reportMonth = {
      year: this.yy,
      month: this.mm
    };

    this._getReportData(this.reportMonth.year, this.reportMonth.month);

  }

  ngOnDestroy() {
    this._dateSub.unsubscribe()
  }

}
