import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyCumulativeComponent } from './monthly-cumulative.component';

describe('MonthlyCumulativeComponent', () => {
  let component: MonthlyCumulativeComponent;
  let fixture: ComponentFixture<MonthlyCumulativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyCumulativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyCumulativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
