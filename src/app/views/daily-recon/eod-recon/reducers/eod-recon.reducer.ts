import { IEODData, ISalesEODItem } from '../models/eod';
import * as EODActions from '../actions/eod.actions';

export type Action = EODActions.All;

export function eodReconReducer(state: IEODData, action: Action): IEODData {
  switch (action.type) {
    case EODActions.LOAD_EOD_DATA:
      return action.payload;

    case EODActions.UPDATE_EOD_SALES_DATA:
      const idx = state.sales.findIndex((elm: ISalesEODItem) => {
        return elm._id_ === action.payload._id_
      });

      return Object.assign({}, state, {
        sales:
          state.sales.map((itm: ISalesEODItem, index: number) => {
            if (index !== idx) {
              return itm;
            }
            return {
              ...itm,
              ...action.payload
            };
          })

      });

    case EODActions.UPDATE_EOD_ITEM_DATA:
      const payObj = {};
      payObj[action.payload.key] = action.payload.amount;
      return Object.assign({}, state, payObj);

    default:
      return state;
  }
}
