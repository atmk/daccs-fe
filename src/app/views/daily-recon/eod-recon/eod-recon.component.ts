import { Component, OnInit } from '@angular/core';
import { EODReconService } from './services/eod-recon.service';
import { IEODData } from './models/eod';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'daccs-eod-recon',
  templateUrl: './eod-recon.component.html',
  styleUrls: ['./eod-recon.component.scss']
})
export class EODReconComponent implements OnInit {

  public EODData$: Observable<IEODData>;
  public initialItemId: number;

  private _initialSalesitemSub: Subscription;

  constructor(private _eodReconService: EODReconService) {
    this.EODData$ = this._eodReconService.EODData$;

    this._initialSalesitemSub = this._eodReconService.firstSalesItmID$.subscribe(val => {
      this.initialItemId = val;
    });
  }

  ngOnInit(): void {
    // this._eodReconService.getEODByDate();
  }

}
