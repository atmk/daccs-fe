import { IVatConfig } from '../../../../initialiaze/models/site-config';

export interface IEODData {
  id: string;
  date: string;
  site: number;
  sales: Array<ISalesEODItem>;
  cash_payouts: IEODItem;
  cash_payins: IEODItem;
  received_on_account: IEODItem;
  account_sales: IEODItem;
  cash_deposits: IEODItem;
  speedpoint_deposits: Array<ISpeedPointDepEOD>;
  check_deposits: IEODItem;
  eft_deposits: IEODItem;
  float_control: number;
  shorts: string;
}

export interface IEODItem {
  amount: number;
  discrepancy: number;
}

enum SalesItemCat {
  WET_STOCK,
  DRY_STOCK,
  OTHER
}

export interface ICat {
  _id_: number;
  id: string;
  title: string;
}

export interface IMainCat extends ICat {
  category_type: SalesItemCat;
  discrepancy: string;
  active?: boolean;
}

export interface IEODCat extends ICat {
  main_category: number;
  franchise: number;
  active?: boolean;
}

export interface ISubCat extends ICat {
  category: number;
  vat_code: IVatConfig;
  active?: boolean;
}

export interface ISalesEODItem extends IMainCat {
  categories: Array<IEODCategory>;
}

export interface IEODCategory extends IEODCat {
  sub_categories: Array<IEODSubCategory>;
}

export interface IEODSubCategory extends ISubCat {
  liters?: number;
  amount: number;
  incl: number;
  vat: number;
}

export interface ISpeedPointDepEOD {
  terminal_description: string;
  terminal_number: string;
  batch_number: string;
  amount_cashiers: string;
  amount: string;
  discrepancy: string;
}
