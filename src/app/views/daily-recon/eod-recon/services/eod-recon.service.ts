import { Injectable } from '@angular/core';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { IEODData } from '../models/eod';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class EODReconService {

  public EODData$: Observable<IEODData>;
  public firstSalesItmID$ = new BehaviorSubject(0);

  constructor(private _store: Store<IAppStore>) {
    this.EODData$ = _store.select('eodReconReducer');
  }

}
