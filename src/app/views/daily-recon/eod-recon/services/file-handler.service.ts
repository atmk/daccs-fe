import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Headers, Http, RequestOptions } from '@angular/http';
import { AuthService } from '../../../../auth/auth.service';
import { AuthHttp } from 'angular2-jwt';

export interface IFile {
  id: number;
  file: string;
  file_name: string;
  file_type: string;
  file_extension: string;
  size: string;
}

@Injectable()
export class FileHandlerService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _http: Http,
              private _authHttp: AuthHttp,
              private _authService: AuthService) {

  }

  public getAllFilesByDate(date: string): Observable<Array<IFile>> {
    return this._authHttp.get(`${this._apiBaseUrl}/files/document/${date}/`)
      .map(res => res.json());
  }

  public uploadFile(date: string, formData: FormData): Observable<IFile> {
    const headers = new Headers();
    headers.append('Authorization', 'JWT ' + this._authService.getToken());
    headers.append('Accept', 'application/json');
    const options = new RequestOptions({headers: headers});
    return this._http.post(`${this._apiBaseUrl}/files/document/${date}/`, formData, options)
      .map(res => res.json());
  }

  public deleteFile(id: number) {
    return this._authHttp.delete(`${this._apiBaseUrl}/files/document/retrieve/destroy/${id}/`)
      .map(res => res.json());
  }
}
