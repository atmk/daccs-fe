import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { IEODData, IEODItem, ISalesEODItem, ISpeedPointDepEOD } from '../models/eod';

interface ISubmitEODObj {
  [key: string]: IEODItem | ISalesEODItem | Array<ISpeedPointDepEOD>
}

@Injectable()
export class EODUpdateItemsService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public updateEODReconItem(body: ISubmitEODObj, id: string): Observable<IEODData> {

    return this._authHttp.patch(`${this._apiBaseUrl}/recons/eod/update/${id}/`, JSON.stringify(body))
      .map(res => res.json())
  }

}
