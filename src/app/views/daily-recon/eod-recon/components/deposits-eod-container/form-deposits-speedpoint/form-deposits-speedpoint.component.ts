import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ISpeedPointDepEOD } from '../../../models/eod';
import { Subscription } from 'rxjs/Subscription';
import { IBrokenCon, InitializerService } from '../../../../../../initialiaze/services/initializer.service';
import { EODUpdateItemsService } from '../../../services/eod-update-items.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../../initialiaze/models/main-recon';

interface ISpeedpointSubmission {
  speedpoint_deposits: Array<ISpeedPointDepEOD>;
}

@Component({
  selector: 'daccs-form-deposits-speedpoint',
  templateUrl: './form-deposits-speedpoint.component.html',
  styleUrls: ['./form-deposits-speedpoint.component.scss']
})
export class FormDepositsSpeedpointComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public speedpointDataArr: Array<ISpeedPointDepEOD>;
  @Input() public mongoId: string;

  @Output() dirtyForm = new EventEmitter();

  public depSpeedpointForm: FormGroup;
  public totals: { amount: number, amount_cashiers: number, discrepancy: number, adjust: number };
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;

  private _formChangeSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _toasterService: ToasterService,
              private _actRoute: ActivatedRoute,
              private _EODUpdateItemsService: EODUpdateItemsService,
              private _initializerService: InitializerService,
              private _settingsService: InitializerService) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  public onSubmit(body: { string: Array<ISpeedPointDepEOD> }): void {
    console.log(body);
    this._settingsService.serverIsRunning$.next(true);
    this.dirtyForm.emit(false);
    this._EODUpdateItemsService.updateEODReconItem(body, this.mongoId)
      .subscribe(
        () => {
          this.dirtyForm.emit(false);
          this._settingsService.serverIsRunning$.next(false);
          const nextUrl = '../check-deposits';
          this._router.navigate([nextUrl], {relativeTo: this._actRoute});
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err)
        }
      );
  }

  private _getTotalAmounts(rowItemData: ISpeedpointSubmission) {

    this.totals = rowItemData.speedpoint_deposits.reduce((mem: any, curr: any) => {
      mem['amount_cashiers'] += +curr['amount_cashiers'];
      mem['amount'] += +curr['amount'];
      mem['discrepancy'] += +curr['discrepancy'];
      return mem
    }, {amount_cashiers: 0, amount: 0, discrepancy: 0, adjust: 0});

  }

  public delInputRow(index: number): void {
    this.depSpeedpointForm.markAsDirty();
    const arrayControl: FormArray = this.depSpeedpointForm.get('speedpoint_deposits') as FormArray;
    arrayControl.removeAt(index);
  }

  ngOnInit(): void {
    this._formChangeSub = this.depSpeedpointForm.valueChanges
      .subscribe((data: ISpeedpointSubmission) => {
        this.dirtyForm.emit(this.depSpeedpointForm.dirty);
        this._getTotalAmounts(data);
      });
  }

  ngOnChanges(): void {

    this.depSpeedpointForm = this._fb.group({
      'speedpoint_deposits': this._fb.array([]),
    });

    if (this.speedpointDataArr.length) {
      const pkgArrayControl: FormArray = this.depSpeedpointForm.get('speedpoint_deposits') as FormArray;
      this.speedpointDataArr.forEach((speedPointTransaction: ISpeedPointDepEOD) => {
        const newGroup = this._fb.group({
          'terminal_description': speedPointTransaction.terminal_description,
          'terminal_number': speedPointTransaction.terminal_number,
          'batch_number': speedPointTransaction.batch_number,
          'amount_cashiers': speedPointTransaction.amount_cashiers,
          'amount': [speedPointTransaction.amount, Validators.required],
          'discrepancy': [speedPointTransaction.discrepancy],
        });
        pkgArrayControl.push(newGroup);
      });

      this._getTotalAmounts(this.depSpeedpointForm.value);
    }

  }

  ngOnDestroy(): void {
    if (this._formChangeSub) {
      this._formChangeSub.unsubscribe();
    }
  }

}
