import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ISpeedPointDepEOD } from '../../../../models/eod';

@Component({
  selector: 'daccs-form-deposits-speedpoint-row',
  templateUrl: './form-deposits-speedpoint-row.component.html',
  styleUrls: ['./form-deposits-speedpoint-row.component.scss']
})
export class FormDepositsSpeedpointRowComponent implements OnInit, OnDestroy {

  @Input() public arrForm: FormGroup;

  @Output() rowDeleted = new EventEmitter();

  public difference: number;
  public adjust: number;

  private _valueChangeSub: Subscription;

  public onDelete() {
    this.rowDeleted.emit();
  }

  public processEnter(e: Event, el: any): void {
    e.preventDefault();
    el.focus();
  }

  private _getDiff(amnt: number, amntCsh: number, discp: number): number {
    return (Math.round((amnt - amntCsh + discp ) * 100) / 100);
  }

  private _getAdjust(amnt: number, discp: number): number {
    return (Math.round((amnt + discp ) * 100) / 100);
  }

  ngOnInit(): void {

    const amnt = this.arrForm.get('amount').value;
    const amntCsh = this.arrForm.get('amount_cashiers').value;
    const discp = this.arrForm.get('discrepancy').value;

    this.difference = this._getDiff(+amnt, +amntCsh, +discp);
    this.adjust = this._getAdjust(+amnt, +discp);

    this._valueChangeSub = this.arrForm.valueChanges.subscribe((val: ISpeedPointDepEOD) => {
      this.difference = this._getDiff(+val.amount, +val.amount_cashiers, +val.discrepancy)
      this.adjust = this._getAdjust(+val.amount, +val.discrepancy)
    });
  }

  ngOnDestroy(): void {
    this._valueChangeSub.unsubscribe();
  }
}
