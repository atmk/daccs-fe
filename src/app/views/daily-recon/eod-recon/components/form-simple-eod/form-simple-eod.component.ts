import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EODUpdateItemsService } from '../../services/eod-update-items.service';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FORM_ORDER } from '../../../../../shared/constants/form-order';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

interface ISimpleItem {
  [key: string]: string;
}

@Component({
  selector: 'daccs-form-simple-eod',
  templateUrl: './form-simple-eod.component.html',
  styleUrls: ['./form-simple-eod.component.scss']
})
export class FormSimpleEodComponent implements OnInit, OnChanges {

  @Input() public simpleReconItemData: ISimpleItem;
  @Input() public simpleReconItemName: string;
  @Input() public mongoId: string;

  public simpleReconItemForm: FormGroup;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;

  constructor(private _EODUpdateItemsService: EODUpdateItemsService,
              private _settingsService: InitializerService,
              private _initializerService: InitializerService,
              private _router: Router,
              private _toasterService: ToasterService,
              private _actRoute: ActivatedRoute,
              private _fb: FormBuilder) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public onSubmit(body: any): void {
    this._settingsService.serverIsRunning$.next(true);
    this._EODUpdateItemsService.updateEODReconItem(body, this.mongoId)
      .subscribe(
        () => {
          const adjOrder = FORM_ORDER.filter(itm => {
            return itm.name !== 'shorts' // Pop shorts off the end, we may add it back later
          });
          this._settingsService.serverIsRunning$.next(false);
          const currUrl = this._actRoute.snapshot.url[0].path;
          const pos = adjOrder.findIndex(ordItm => ordItm.name === currUrl);
          if (pos < adjOrder.length - 1) { // if its the last item, then dont route
            const nextUrl = adjOrder[pos + 1].route;
            this._router.navigate([nextUrl], {relativeTo: this._actRoute});
          }
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  ngOnInit(): void {

    const createdGrp = {};
    createdGrp[this.simpleReconItemName] = [this.simpleReconItemData, Validators.required];
    this.simpleReconItemForm = this._fb.group(createdGrp);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.simpleReconItemForm && this.simpleReconItemData) {

      const createdGrp = {};
      createdGrp[this.simpleReconItemName] = this.simpleReconItemData;

      this.simpleReconItemForm.setValue(createdGrp);
    }
  }

}
