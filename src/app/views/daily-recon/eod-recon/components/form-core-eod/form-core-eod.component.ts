import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { EODUpdateItemsService } from '../../services/eod-update-items.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { IEODItem } from '../../models/eod';
import { FORM_ORDER } from '../../../../../shared/constants/form-order';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

@Component({
  selector: 'daccs-form-core-eod',
  templateUrl: './form-core-eod.component.html',
  styleUrls: ['./form-core-eod.component.scss'],
})
export class FormCoreEODComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public reconItemData: IEODItem;
  @Input() public reconItemName: string;
  @Input() public mongoId: string;

  @Output() dirtyForm = new EventEmitter();

  public reconItemForm: FormGroup;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;
  public adjustedTotal: number;

  private _valueChangeSub: Subscription;

  constructor(private _EODUpdateItemsService: EODUpdateItemsService,
              private _settingsService: InitializerService,
              private _router: Router,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService,
              private _actRoute: ActivatedRoute,
              private _fb: FormBuilder) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;

    this.reconItemForm = this._fb.group({
      amount: ['', Validators.required],
      discrepancy: ['']
    });
  }

  public onSubmit(body: IEODItem): void {

    this.dirtyForm.emit(false);

    const submitEOD: { string: IEODItem } = <{ string: IEODItem }>{};
    submitEOD[this.reconItemName] = body;

    this._settingsService.serverIsRunning$.next(true);
    this._EODUpdateItemsService.updateEODReconItem(submitEOD, this.mongoId)
      .subscribe(
        () => {
          this.dirtyForm.emit(false);
          this._settingsService.serverIsRunning$.next(false);
          const currUrl = this._actRoute.snapshot.url[0].path;
          const pos = FORM_ORDER.findIndex(ordItm => ordItm.name === currUrl);
          const nextUrl = FORM_ORDER[pos + 1].route;
          this._router.navigate([nextUrl], {relativeTo: this._actRoute});
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  public processEnter(e: Event, el: any): void {
    e.preventDefault();
    el.focus();
  }

  ngOnInit(): void {

    this._valueChangeSub = this.reconItemForm.valueChanges
      .subscribe((val: IEODItem) => {
        this.adjustedTotal = +val.amount + +val.discrepancy;
        this.dirtyForm.emit(this.reconItemForm.dirty);
      });
  }

  ngOnChanges(): void {

    if (this.reconItemData) {
      this.adjustedTotal = +this.reconItemData.amount + +this.reconItemData.discrepancy;
    }

    if (this.reconItemData && this.reconItemForm) {

      this.reconItemForm.setValue({
        amount: this.reconItemData.amount,
        discrepancy: this.reconItemData.discrepancy
      })
    }

  }

  ngOnDestroy(): void {
    if (this._valueChangeSub) {
      this._valueChangeSub.unsubscribe();
    }
  }

}
