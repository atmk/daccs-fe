import { ChangeDetectionStrategy, Component, OnChanges } from '@angular/core';
import { IEODData } from '../../models/eod';
import { EODReconService } from '../../services/eod-recon.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'daccs-sales-eod-container',
  templateUrl: './sales-eod-container.component.html',
  styleUrls: ['./sales-eod-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SalesEodContainerComponent implements OnChanges {

  public EODData$: Observable<IEODData>;

  constructor(private _EODReconService: EODReconService) {
    this.EODData$ = this._EODReconService.EODData$;
  }

  ngOnChanges(): void {
  }

}
