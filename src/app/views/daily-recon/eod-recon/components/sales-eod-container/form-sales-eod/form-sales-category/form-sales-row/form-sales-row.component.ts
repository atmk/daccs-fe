import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IAccHolderConfig, IGenLedgerConfig, ISupplierConfig, IVatConfig } from '../../../../../../../../initialiaze/models/site-config';
import { DaccsCurrencyPipe } from '../../../../../../../../shared/pipes/currency.pipe';
import { UtilityFunctions } from '../../../../../../../../shared/utils/utility-functions';

type Setting = ISupplierConfig | IGenLedgerConfig | IAccHolderConfig | IVatConfig;

@Component({
  selector: 'daccs-form-sales-row',
  templateUrl: './form-sales-row.component.html',
  styleUrls: ['./form-sales-row.component.scss']
})
export class FormSalesRowComponent implements OnInit {

  @Input() rowForm: FormGroup;
  @Input() mainCatType: number;
  @Input() vatCodes: Array<IVatConfig>;

  public exclAmnt: string | number;
  public inclAmnt: string | number;
  public vatAmnt: string | number;
  public vatObj: IVatConfig;

  public vatPercentage: number;

  constructor(private _currencyPipe: DaccsCurrencyPipe) {
  }

  public getTotalsFromExcl(amount: string | number): void {
    if (!amount) {
      this.exclAmnt = 0;
      this.vatAmnt = 0;
      this.inclAmnt = 0;
    } else if (isNaN(+amount)) {  // need to allow a '.' or a '-'
      this.exclAmnt = amount;
      this.vatAmnt = 0;
      this.inclAmnt = 0;
    } else {
      this.exclAmnt = +amount;
      const vatCalc = UtilityFunctions.rounder((+amount / 100) * this.vatPercentage);
      const inclCalc = UtilityFunctions.rounder(this.exclAmnt + vatCalc);
      this.vatAmnt = vatCalc;
      this.inclAmnt = inclCalc;
    }
  }

  public getTotalsFromVAT(amount: string | number): void {
    if (!amount) {
      this.exclAmnt = 0;
      this.vatAmnt = 0;
      this.inclAmnt = 0;
    } else if (isNaN(+amount)) {  // need to allow a '.' or a '-'
      this.exclAmnt = 0;
      this.vatAmnt = amount;
      this.inclAmnt = 0;
    } else {
      this.vatAmnt = +amount;
      const taxRatio = UtilityFunctions.rounder(this.vatPercentage / 100);
      if (taxRatio > 0) {
        const exclCalc = UtilityFunctions.rounder(+amount / taxRatio);
        const inclCalc = UtilityFunctions.rounder(exclCalc + this.vatAmnt);
        this.exclAmnt = exclCalc;
        this.inclAmnt = inclCalc;
      }
    }
  }

  public getTotalsFromIncl(amount: string | number, fromInput: boolean): void {
    if (!amount) {
      this.exclAmnt = 0;
      this.vatAmnt = 0;
      this.inclAmnt = 0;
    } else if (isNaN(+amount)) {  // need to allow a '.' or a '-'
      this.exclAmnt = 0;
      this.vatAmnt = 0;
      this.inclAmnt = amount;
    } else {
      this.inclAmnt = +amount;
      const taxRatio = UtilityFunctions.rounder(this.vatPercentage / 100 + 1);
      const exclCalc = UtilityFunctions.rounder(this.inclAmnt / taxRatio);
      const vatCalc = UtilityFunctions.rounder(this.inclAmnt - exclCalc);
      this.exclAmnt = exclCalc;
      this.vatAmnt = vatCalc;
    }
  }

  public compareFn(s1: Setting, s2: Setting): boolean {
    return s1._id_ && s2._id_ ? s1._id_ === s2._id_ : s1._id_ === s2._id_;
  }

  public processEnter(e: Event, el: any): void {
    e.preventDefault();
    el.focus();
  }

  ngOnInit(): void {

    this.vatObj = this.rowForm.controls['vat_code'].value;
    this.vatPercentage = +this.vatObj.percentage;

    this.getTotalsFromIncl(+this.rowForm.controls['incl'].value, false);

  }

}
