import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IEODCategory, IEODSubCategory } from '../../../../models/eod';
import { Subscription } from 'rxjs/Subscription';
import { IVatConfig } from '../../../../../../../initialiaze/models/site-config';

export interface ISubTotal {
  _id_?: number;
  incl: number
  excl: number
  vat: number
}

@Component({
  selector: 'daccs-form-sales-category',
  templateUrl: './form-sales-category.component.html',
  styleUrls: ['./form-sales-category.component.scss']
})
export class FormSalesCatergoyComponent implements OnInit, OnChanges, OnDestroy {

  @Input() arrForm: FormGroup;
  @Input() subCatData: Array<IEODSubCategory>;
  @Input() vatCodes: Array<IVatConfig>;
  @Input() title: string;
  @Input() mainCatType: number;
  @Input() catId: number;

  @Output() dirtyForm: EventEmitter<boolean> = new EventEmitter();
  @Output() subTotal: EventEmitter<ISubTotal> = new EventEmitter();
  @Output() salesType: EventEmitter<string> = new EventEmitter();

  private _formChangeSub: Subscription;

  public exclAmountTotal: number;
  public inclAmountTotal: number;
  public vatAmountTotal: number;
  public litresAmountTotal: number;

  // necessary for AOT compliation to avoid Abstract control err
  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  constructor(private _fb: FormBuilder) {
  }

  private _getTotalAmounts(rowItemData: IEODCategory): void {

    this.exclAmountTotal = 0;
    this.inclAmountTotal = 0;
    this.vatAmountTotal = 0;
    this.litresAmountTotal = 0;

    rowItemData.sub_categories.forEach((rowItem: IEODSubCategory) => {
      this.exclAmountTotal += +rowItem.amount;
      this.inclAmountTotal += +rowItem.incl;
      this.vatAmountTotal += +rowItem.vat;
      this.litresAmountTotal += +rowItem.liters;
    });

    this.subTotal.emit({
      _id_: this.catId,
      incl: this.inclAmountTotal,
      excl: this.exclAmountTotal,
      vat: this.vatAmountTotal
    });
  }

  ngOnInit(): void {

    // running this once here on init because the forms that include VAT are calculated
    // and then trigger the form value changes. Wetstock is not calcualted so there
    // is no initial foprm change detected
    this._getTotalAmounts(this.arrForm.value);

    this._formChangeSub = this.arrForm.valueChanges.subscribe((data: IEODCategory) => {
      this.dirtyForm.emit(this.arrForm.dirty);
      this._getTotalAmounts(data);
    });
  }

  ngOnChanges() {

    const pkgArrayControl: FormArray = this.arrForm.get('sub_categories') as FormArray;

    this.subCatData.forEach((cat: IEODSubCategory) => {
      const newGroup = this._fb.group({
        '_id_': [cat._id_, Validators.required],
        'title': [cat.title, Validators.required],
        'vat_code': [cat.vat_code, Validators.required],
        'amount': [cat.amount, Validators.required],
        'liters': [cat.liters, Validators.required],
        'vat': [0, Validators.required],
        'incl': [cat.incl, Validators.required]
      });
      pkgArrayControl.push(newGroup);
    });
  }

  ngOnDestroy(): void {

    this.subTotal.emit({
      _id_: this.catId,
      incl: 0,
      excl: 0,
      vat: 0
    });

    if (this._formChangeSub) {
      this._formChangeSub.unsubscribe();
    }
  }
}
