import { Component, OnDestroy, OnInit } from '@angular/core';
import { IEODCategory, ISalesEODItem } from '../../../models/eod';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EODReconService } from '../../../services/eod-recon.service';
import { Subscription } from 'rxjs/Subscription';
import { EODUpdateItemsService } from '../../../services/eod-update-items.service';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { ISiteConfig, IVatConfig } from '../../../../../../initialiaze/models/site-config';
import { IBrokenCon, InitializerService } from '../../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { ISubTotal } from './form-sales-category/form-sales-category.component';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../../initialiaze/models/main-recon';

@Component({
  selector: 'daccs-form-sales-eod',
  templateUrl: './form-sales-eod.component.html',
  styleUrls: ['./form-sales-eod.component.scss']
})
export class FormSalesEodComponent implements OnInit, OnDestroy {

  public EODItemData: ISalesEODItem;
  public EODItemForm: FormGroup;
  public salesItmId: number;
  public vatCodes: Array<IVatConfig> = [];
  public dirty = false;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;
  public subTotals: Array<ISubTotal> = [];
  public grandTotal: ISubTotal;

  private _salesIDArray: Array<number> = []; // for the next on submit routing
  private _mainEodId: string;
  private _paramsSub: Subscription;
  private _settingsSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _EODReconService: EODReconService,
              private _store: Store<IAppStore>,
              private _settingsService: InitializerService,
              private _initializerService: InitializerService,
              private _EODUpdateItemsService: EODUpdateItemsService,
              private _route: ActivatedRoute) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public setFormDirty(ev: boolean): void {
    this.dirty = ev;
  }

  public checkDirty(): boolean {
    return this.dirty;
  }

  public getGrandTotal(ev: ISubTotal): void {
    const idx = this.subTotals.findIndex((itm: ISubTotal) => {
      return itm._id_ === ev._id_;
    });
    if (idx !== -1) {
      this.subTotals.splice(idx, 1, ev);
    } else {
      this.subTotals.push(ev);
    }

    this.grandTotal = this.subTotals.reduce((mem: any, curr: any) => {
      mem['incl'] += curr['incl'];
      mem['excl'] += curr['excl'];
      mem['vat'] += curr['vat'];
      return mem;
    }, {incl: 0, excl: 0, vat: 0});
  }

  public getAdjusted(val: string, total: number): number {
    return +val + total;
  }

  public onSubmit(body: ISalesEODItem): void {

    const submitEOD: { string: ISalesEODItem } = <{ string: ISalesEODItem }>{};
    submitEOD['sales'] = [body];

    this.dirty = false;
    this._settingsService.serverIsRunning$.next(true);
    this._EODUpdateItemsService.updateEODReconItem(submitEOD, this._mainEodId)
      .subscribe(
        () => {
          this._settingsService.serverIsRunning$.next(false);
          if (this._salesIDArray.length) {
            const currPos = this._salesIDArray.indexOf(this.salesItmId);
            const nextItm = this._salesIDArray[currPos + 1];

            if (nextItm) {
              this._settingsService.serverIsRunning$.next(false);
              this._router.navigate(['../../itm', nextItm], {relativeTo: this._actRoute});
            } else {
              this._router.navigate(['../../../cash-payouts'], {relativeTo: this._actRoute});
            }
          }
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
        },
        () => console.log('Request Complete')
      );
  }

  // necessary for AOT compliation to avoid Abstract control err
  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  private _createForm(): void {
    if (this.EODItemData) {
      this.EODItemForm = this._fb.group({
        '_id_': this.EODItemData._id_,
        'title': this.EODItemData.title,
        'category_type': this.EODItemData.category_type,
        'discrepancy': this.EODItemData.discrepancy,
        'categories': this._fb.array([]),
      });

      const pkgArrayControl: FormArray = this.EODItemForm.get(`categories`) as FormArray;
      this.EODItemData.categories.forEach((cat: IEODCategory) => {
        const newGroup = this._fb.group({
          '_id_': cat._id_,
          'title': cat.title,
          'sub_categories': this._fb.array([]),
        });
        pkgArrayControl.push(newGroup);
      });
    }

  }

  ngOnInit() {

    this._settingsSub = this._store.select('siteConfigReducer')
      .subscribe((settings: ISiteConfig) => {
        this.vatCodes = settings.vat_codes;
      });

    this._paramsSub = this._route.params
      .switchMap((params: any) => {
        this.salesItmId = +params['sales_itm_id'];
        return this._EODReconService.EODData$;
      })
      .subscribe(data => {
        if (data) {
          if (data.sales) {
            this._salesIDArray = data.sales.map(itm => {
              return itm._id_;
            });
          }
          this._mainEodId = data.id;
          this.EODItemData = data.sales.find(itm => {
            return itm._id_ === this.salesItmId;
          });
        }
        this._createForm();
      });

  }

  ngOnDestroy(): void {
    if (this._paramsSub) {
      this._paramsSub.unsubscribe();
    }
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }
  }

}
