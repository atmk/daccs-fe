import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { EODReconService } from '../services/eod-recon.service';
import * as EODActions from '../actions/eod.actions';

@Injectable()
export class LoadEODEffects {

  @Effect({dispatch: false}) eod$ = this.actions$
    .ofType(EODActions.LOAD_EOD_DATA)
    .do((action: EODActions.All) => {
      if (action.payload.sales.length) {
        const salesitmId = action.payload.sales[0]._id_ || 0;
        this._EODReconService.firstSalesItmID$.next(salesitmId);

      }
    });

  constructor(private _EODReconService: EODReconService,
              private actions$: Actions) {
  }
}
