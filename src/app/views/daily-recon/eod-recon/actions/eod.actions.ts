import { Action } from '@ngrx/store';
import { IEODData, IEODItem, ISalesEODItem } from '../models/eod';

export const LOAD_EOD_DATA = 'LOAD_EOD_DATA';
export const UPDATE_EOD_SALES_DATA = 'UPDATE_EOD_SALES_DATA';
export const UPDATE_EOD_ITEM_DATA = 'UPDATE_EOD_ITEM_DATA';

interface IEODUpdate {
  key: string;
  amount: IEODItem;
}

export class LoadEODData implements Action {
  readonly type = LOAD_EOD_DATA;

  constructor(public payload: IEODData) {
  }
}

export class UpdateEODSalesData implements Action {
  readonly type = UPDATE_EOD_SALES_DATA;

  constructor(public payload: ISalesEODItem) {
  }
}

export class UpdateEODItemData implements Action {
  readonly type = UPDATE_EOD_ITEM_DATA;

  constructor(public payload: IEODUpdate) {
  }
}

export type All = LoadEODData & UpdateEODSalesData & UpdateEODItemData
