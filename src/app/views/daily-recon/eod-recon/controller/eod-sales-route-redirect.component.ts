import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { EODReconService } from '../services/eod-recon.service';

@Component({
  selector: 'daccs-eod-sales-route-redirect',
  templateUrl: './eod-sales-route-redirect.component.html',
  styleUrls: ['./eod-sales-route-redirect.component.scss']
})
export class EodSalesRouteRedirectComponent implements OnInit, OnDestroy {

  private _idSub: Subscription;

  constructor(private _eodReconService: EODReconService,
              private _router: Router,
              private _actRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._idSub = this._eodReconService.firstSalesItmID$.subscribe(val => {
      if (val >= 0) {
        this._router.navigate(['itm', val], {relativeTo: this._actRoute});
      }
    });
  }

  ngOnDestroy(): void {
    if (this._idSub) {
      this._idSub.unsubscribe();
    }
  }

}
