import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EODReconService } from '../services/eod-recon.service';
import { IEODData } from '../models/eod';
import { Observable } from 'rxjs/Observable';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-eod-controller',
  templateUrl: './eod-controller.component.html',
  styleUrls: ['./eod-controller.component.scss']
})
export class EodControllerComponent implements OnInit {

  public sectionURL: string;
  public EODData$: Observable<IEODData>;
  public dirty = false;

  constructor(private _route: ActivatedRoute, private _EODReconService: EODReconService) {
    this.EODData$ = this._EODReconService.EODData$;
  }

  public setFormDirty(ev: boolean): void {
    this.dirty = ev;
  }

  public checkDirty(): boolean {
    return this.dirty;
  }

  ngOnInit(): void {
    this.sectionURL = UtilityFunctions.underscoreify(this._route.snapshot.url[0].path);
  }

}
