import { Component, OnInit } from '@angular/core';
import { FileHandlerService, IFile } from '../eod-recon/services/file-handler.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { UtilityFunctions } from '../../../shared/utils/utility-functions';
import { Subscription } from 'rxjs/Subscription';
import { InitializerService } from '../../../initialiaze/services/initializer.service';

@Component({
  selector: 'daccs-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss'],
  providers: [FileHandlerService]
})
export class FilesComponent implements OnInit {

  public busyAdding = false;
  public loadingFiles = false;
  public remModalIsOpen = false;
  public currDateStr: string;
  public fileArray: Array<IFile> = [];

  private _selectedFileId: number;
  private _selectedDate: Date;
  private _dateString: string;
  private _dateSub: Subscription;

  constructor(private _fileHandlerService: FileHandlerService,
              private _initializerService: InitializerService,
              private _toasterService: ToasterService) {
    this._dateSub = this._initializerService.currDate$.subscribe(val => {
      this._selectedDate = new Date(val);
      this.currDateStr = this._selectedDate.toISOString().substring(0, 10);
      this._dateString = this._selectedDate.toISOString().substring(0, 10).replace(/-0+/g, '-')
    });
  }

  public onFileChange(event) {
    this.busyAdding = true;
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('file', file, file.name);
      this._fileHandlerService.uploadFile(this._dateString, formData)
        .subscribe(
          (res: IFile) => {
            this.busyAdding = false;
            this.fileArray.push(res);
            this._toasterService.pop('success', 'Success',
              'File successfully uploaded');
          },
          err => {
            this.busyAdding = false;
            const toast: Toast = {
              type: 'error',
              title: `Error Code: ${err.status}, Message: ${err.statusText}`,
              body: UtilityFunctions.parseErrs(err._body, err.status),
              bodyOutputType: BodyOutputType.TrustedHtml
            };
            this._toasterService.pop(toast);
          }
        );
    }
  }

  public openRemModal(id: number): void {
    this._selectedFileId = id;
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public onDeleteFile(): void {
    console.log(this._selectedFileId);
    this.remModalIsOpen = false;
    this._fileHandlerService.deleteFile(this._selectedFileId)
      .subscribe(
        (res: IFile) => {
          this.fileArray = this.fileArray.filter((file: IFile) => {
            return file.id !== this._selectedFileId;
          });
          console.log(res);
          this._toasterService.pop('success', 'Success',
            'File successfully removed');
        },
        err => {
          this.busyAdding = false;
          const toast: Toast = {
            type: 'error',
            title: `Error Code: ${err.status}, Message: ${err.statusText}`,
            body: UtilityFunctions.parseErrs(err._body, err.status),
            bodyOutputType: BodyOutputType.TrustedHtml
          };
          this._toasterService.pop(toast);
        }
      );
  }

  ngOnInit(): void {
    this._selectedFileId = null;
    this.loadingFiles = true;
    this._fileHandlerService.getAllFilesByDate(this._dateString)
      .subscribe(
        (res: Array<IFile>) => {
          this.fileArray = res;
          this.loadingFiles = false;
        },
        err => {
          this.loadingFiles = false;
          const toast: Toast = {
            type: 'error',
            title: `Error Code: ${err.status}, Message: ${err.statusText}`,
            body: UtilityFunctions.parseErrs(err._body, err.status),
            bodyOutputType: BodyOutputType.TrustedHtml
          };
          this._toasterService.pop(toast);
        }
      );
  }

}
