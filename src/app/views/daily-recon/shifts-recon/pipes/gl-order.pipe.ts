import { Pipe, PipeTransform } from '@angular/core';
import { GLTypes } from '../../../../shared/constants/gl-types';

@Pipe({
  name: 'glOrder'
})
export class GlOrderPipe implements PipeTransform {

  transform(array: any[], field: string): any[] {

    if (field === 'ledger_type') {
      const metaArr = [];
      const noGls = Object.keys(GLTypes).length / 2;
      const xArr = Array.from(Array(noGls), (_, i) => i);
      xArr.forEach(() => metaArr.push([]));

      array.forEach(itm => {
        metaArr[itm.ledger_type].push(itm)
      });

      const sortedByTitle = metaArr.map(arr => {
        return arr.sort((a: any, b: any) => {
          if (a['title'].toLowerCase() < b['title'].toLowerCase()) {
            return -1;
          } else if (a['title'].toLowerCase() > b['title'].toLowerCase()) {
            return 1;
          } else {
            return 0;
          }
        });
      });

      return [
        ...sortedByTitle[1],
        ...sortedByTitle[0],
        ...sortedByTitle[2],
        ...sortedByTitle[3],
        ...sortedByTitle[4]
      ];
    } else {
      return array
    }

  }

}
