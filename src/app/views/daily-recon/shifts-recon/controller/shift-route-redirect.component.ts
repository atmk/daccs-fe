import { Component, OnDestroy, OnInit } from '@angular/core';
import { ShiftsReconService } from '../services/shifts-recon.service';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'daccs-shift-route-redirect',
  template: '<h1>Loading....</h1>',
})
export class ShiftRouteRedirectComponent implements OnInit, OnDestroy {

  private _idSub: Subscription;
  private _initSub: Subscription;

  constructor(private _shiftsService: ShiftsReconService,
              private _router: Router,
              private _actRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    const isDepRoute = this._actRoute.snapshot.data['isDep'] || false;
    const isSubRoot = this._actRoute.snapshot.data['isSubRoot'] || false;

    if (isSubRoot) {
      this._initSub = this._shiftsService.firstShiftID$.subscribe(val => {
        this._router.navigate(['sales', val], {relativeTo: this._actRoute});
        if (val <= 0) {
          this._shiftsService.currShiftId$.next(0);
        }
      });
    } else {
      this._idSub = this._shiftsService.currShiftId$.subscribe(val => {
        if (val !== 0 && !isDepRoute) {
          this._router.navigate(['cash-payouts', val], {relativeTo: this._actRoute});
        } else if (val !== 0 && isDepRoute) {
          this._router.navigate(['cash-deposits', val], {relativeTo: this._actRoute});
        } else {
          this._router.navigate(['/daily-recon/shifts/sales', 0], {relativeTo: this._actRoute});
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this._idSub) {
      this._idSub.unsubscribe();
    }
    if (this._initSub) {
      this._initSub.unsubscribe();
    }
  }

}
