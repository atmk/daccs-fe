import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShiftsReconService } from '../services/shifts-recon.service';
import { IShift } from '../models/shift';
import { ILineInput } from '../components/form-core-shift/form-configs/input-line.interface';
import { FormConfig } from '../components/form-core-shift/form-configs/main-form.config';
import 'rxjs/add/operator/switchMap';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';
import { routerTransition } from '../../../../shared/animations/daccs.animation';
import { ISiteConfig } from '../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { InitializerService } from '../../../../initialiaze/services/initializer.service';

export interface IReconFields {
  [key: string]: ILineInput;
}

@Component({
  selector: 'daccs-shift-controller',
  templateUrl: './shift-controller.component.html',
  styleUrls: ['./shift-controller.component.scss'],
  animations: [routerTransition()],
})
export class ShiftControllerComponent implements OnInit, OnDestroy {
  @HostBinding('style.display')
  get display() {
    return 'block';
  }

  public shiftId: string;
  public mongoId: string;
  public shiftArr: Array<IShift>;
  public shiftObject: IShift;
  public reconItemFields: IReconFields;
  public reconItemData: any;
  public settings$: Observable<ISiteConfig>;
  public sectionURL: string;
  public systemShortsOvers: number;
  public fresh = false;
  public dirty = false;

  private _paramsSub: any;
  private _settingsSub: any;

  constructor(private _actRoute: ActivatedRoute,
              private _router: Router,
              private _settingsService: InitializerService,
              private _shiftsReconService: ShiftsReconService) {
  }

  public setFormDirty(ev: boolean): void {
    this.dirty = ev;
  }

  public checkDirty(): boolean {
    return this.dirty;
  }

  public newShiftButton(): void {
    this._shiftsReconService.newShiftModalOpen$.next(true);
  }

  private _setShiftById(id: string): void {
    if (this.shiftArr) {
      this.shiftObject = this.shiftArr.find(shift => shift.id === id);
      const formPart = UtilityFunctions.underscoreify(this.sectionURL);

      this.systemShortsOvers = UtilityFunctions.getTotalShortsArray(this.shiftObject);

      this._shiftsReconService.currShiftId$.next(this.shiftId);

      this.reconItemFields = FormConfig[formPart];
      this.reconItemData = this.shiftObject[formPart];
      this.mongoId = this.shiftObject.id;
    }
  }

  ngOnInit(): void {

    this.settings$ = this._settingsService.settings$;

    this._paramsSub = this._actRoute.params
      .switchMap((params: any) => {
        this.shiftId = params['shift_id'] || 0;
        this.sectionURL = this._actRoute.snapshot.url[0].path;
        this._shiftsReconService.dynRoute$.next(this.sectionURL);
        return this._shiftsReconService.shifts$;
      })
      .subscribe((data: Array<IShift>) => {
        if (!data) {
          console.log('NO CONNECTION');
        } else if (data && data.length) {
          const c: number = data.findIndex((csh: IShift) => csh.id === this.shiftId);
          if (c === -1) {
            const newId = data[0].id;
            this._router.navigate(['../../sales', newId], {relativeTo: this._actRoute});
          } else {
            this.fresh = false;
            this.shiftArr = data;
            this._setShiftById(this.shiftId);
          }
        } else {
          this.fresh = true;
          this._router.navigate(['../../sales', 0], {relativeTo: this._actRoute});
        }
      });

  }

  ngOnDestroy(): void {
    if (this._paramsSub) {
      this._paramsSub.unsubscribe();
    }
    if (this._settingsSub) {
      this._settingsSub.unsubscribe();
    }
  }

}
