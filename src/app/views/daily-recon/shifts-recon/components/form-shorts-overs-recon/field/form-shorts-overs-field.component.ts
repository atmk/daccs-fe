import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'daccs-form-shorts-overs-field',
  templateUrl: './form-shorts-overs-field.component.html',
  styleUrls: ['./form-shorts-overs-field.component.scss']
})
export class FormShortsOversFieldComponent {

  @Input() arrForm: FormGroup;
  @Output() rowDeleted = new EventEmitter();

  public onDelete(): void {
    this.rowDeleted.emit();
  }

}
