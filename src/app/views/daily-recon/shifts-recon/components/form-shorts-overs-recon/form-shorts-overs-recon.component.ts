import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { IShortsOversItem } from '../../models/shift';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ShiftsUpdateItemsService } from '../../services/shifts-update-items.service';
import { Subscription } from 'rxjs/Subscription';
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';
import { ISubmittedData } from '../form-core-shift/form-core-shift.component';
import { animate, style, transition, trigger } from '@angular/animations';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

@Component({
  selector: 'daccs-form-shorts-overs-recon',
  templateUrl: './form-shorts-overs-recon.component.html',
  styleUrls: ['./form-shorts-overs-recon.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({opacity: 0}))
      ])
    ])
  ]
})
export class FormShortsOversReconComponent implements OnChanges, OnDestroy {

  @Input() public shortsOversData: Array<IShortsOversItem>;
  @Input() public systemShortsOvers: number;
  @Input() public mongoId: string;
  @Output() dirtyShortsOversForm = new EventEmitter();

  public rollingAmmount = 0;
  public shortsOversReconForm: FormGroup;
  public shortsDiscrep: number;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;

  private _formSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _settingsService: InitializerService,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService,
              private _reconItemsService: ShiftsUpdateItemsService) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public onSubmit(body: ISubmittedData) {
    this._settingsService.serverIsRunning$.next(true);
    this.dirtyShortsOversForm.emit(false);
    this._reconItemsService.updateReconItemByShift(body, this.mongoId)
      .subscribe(
        () => {
          this._settingsService.serverIsRunning$.next(false);
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  // necessary for AOT compliation to avoid Abstract control err
  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  public addInputRow(): void {
    const arrayControl: FormArray = this.shortsOversReconForm.get(`shorts`) as FormArray;
    const newGroup = this._fb.group({
      description: [null, Validators.required],
      ref_number: [null, Validators.required],
      amount: [null, [Validators.required, CustomSyncValidators.integerValidator]],
    });
    arrayControl.push(newGroup);
  }

  public delPackageInput(index: number): void {
    this.shortsOversReconForm.controls['shorts'].markAsDirty();
    const arrayControl: FormArray = this.shortsOversReconForm.get(`shorts`) as FormArray;
    arrayControl.removeAt(index);
  }

  public getIsNan(val: any) {
    return isNaN(val) ? '----' : (Math.round((val ) * 100) / 100);
  }

  ngOnChanges(): void {

    this.rollingAmmount = 0;

    if (this.shortsOversData) {

      this.shortsOversReconForm = this._fb.group({
        shorts: this._fb.array([]),
      });

      if (this._formSub) {
        this._formSub.unsubscribe();
      }
      this._formSub = this.shortsOversReconForm.valueChanges.subscribe((formData: any) => {
        this.rollingAmmount = UtilityFunctions.addUpAmounts(formData.shorts);
        this.dirtyShortsOversForm.emit(this.shortsOversReconForm.dirty);
        this.shortsDiscrep = UtilityFunctions.rounder(this.systemShortsOvers - this.rollingAmmount);
      });

      const shortsOversArrayControl: FormArray = this.shortsOversReconForm.get(`shorts`) as FormArray;
      this.shortsOversData.forEach((item: IShortsOversItem) => {
        const newGroup = this._fb.group({
          description: [item.description, Validators.required],
          ref_number: [item.ref_number, Validators.required],
          amount: [item.amount, [Validators.required, CustomSyncValidators.integerValidator]],
        });
        shortsOversArrayControl.push(newGroup);
      });
    }
  }

  ngOnDestroy(): void {
    if (this._formSub) {
      this._formSub.unsubscribe();
    }
  }
}
