import { Component, OnDestroy, OnInit } from '@angular/core';
import { ShiftsReconService } from '../../services/shifts-recon.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'daccs-deposits-shifts-container',
  templateUrl: './deposits-shifts-container.component.html',
  styleUrls: ['./deposits-shifts-container.component.scss']
})
export class DepositsShiftsContainerComponent implements OnInit, OnDestroy {

  public currShiftIndex: number;

  private _shiftSub: Subscription;

  constructor(private _cashierShiftsReconService: ShiftsReconService,
              private _router: Router) {
  }

  public getActive(urlStr: string): boolean {
    return this._router.url.indexOf(urlStr) > -1;
  }

  public setFormDirty(ev, formType: string): boolean {
    return true;
  }

  ngOnInit() {
    this._shiftSub = this._cashierShiftsReconService.currShiftId$
      .subscribe((val: number) => {
        setTimeout(() => {
          this.currShiftIndex = val
        });
      });
  }

  ngOnDestroy(): void {
    if (this._shiftSub) {
      this._shiftSub.unsubscribe();
    }
  }
}
