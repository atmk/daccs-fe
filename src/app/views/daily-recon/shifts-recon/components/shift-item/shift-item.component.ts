import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Router } from '@angular/router';
import { IShiftTotal } from '../../models/shift';
import { RECON_TYPES } from '../../../../../shared/constants/recon-types';
import { DEPOSIT_TYPES } from '../../../../../shared/constants/deposits-types';
import { animate, style, transition, trigger } from '@angular/animations';
import { ICashierConfig, ISiteConfig } from '../../../../../initialiaze/models/site-config';
import { InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';
import { ShiftsReconService } from '../../services/shifts-recon.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-shift-item',
  templateUrl: './shift-item.component.html',
  styleUrls: ['./shift-item.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateY(100%)', opacity: 0, height: 0}),
          animate('180ms', style({transform: 'translateY(0)', opacity: 1, height: '76px'}))
        ]),
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1, height: '76px'}),
          animate('180ms', style({transform: 'translateY(100%)', opacity: 0, height: 0}))
        ])
      ]
    )
  ],
})
export class ShiftItemComponent implements OnChanges {

  @Input() public dynRoute: string;
  @Input() public summary: IShiftTotal;
  @Input() public shiftId: string;
  @Input() public shift_Id_: string;
  @Input() public shiftGivenID: string | number;
  @Input() public name: string;

  @Output() public confirmDel$ = new EventEmitter();

  public cashiers: Array<ICashierConfig>;
  public tallyKeys: Array<string>;
  public editDrawerOpen = false;
  public busy = false;

  private _cashierSub: Subscription;

  constructor(private _router: Router,
              private _toasterService: ToasterService,
              private _initService: InitializerService,
              private _shiftsService: ShiftsReconService) {
    this._cashierSub = this._initService.settings$.subscribe((settings: ISiteConfig) => {
      this.cashiers = settings.cashiers;
    })
  }

  public getActiveShift(urlStr: string): boolean {
    const n = this._router.url.lastIndexOf('/');
    const urlID = this._router.url.substring(n + 1);
    return urlStr === urlID;
  }

  public updateShift(body) {
    this.busy = true;
    const newCashier = this.cashiers.find(itm => {
      return itm._id_ === body._id_
    });
    body['name'] = `${newCashier.name} ${newCashier.surname}`;
    console.log('submitted to API : ', body);
    this._shiftsService.updateShift(body, this.shiftId).subscribe((res) => {
      console.log('RETURNED FROM XHR REQ: ', JSON.parse(res._body));
      this.busy = false;
    }, err => {
      console.log(err);
      const toast: Toast = {
        type: 'error',
        title: `Error Code: ${err.status}, Message: ${err.statusText}`,
        body: UtilityFunctions.parseErrs(err._body, err.status),
        bodyOutputType: BodyOutputType.TrustedHtml
      };
      this._toasterService.pop(toast);
      this.busy = false;
    })
  }

  public openRemModal() {
    this.confirmDel$.emit({del: true, shiftId: this.shiftId});
  }

  public toggleEditDrawer(ev: Event) {
    ev.preventDefault();
    this.editDrawerOpen = !this.editDrawerOpen;
  }

  ngOnChanges(): void {
    this.tallyKeys = (DEPOSIT_TYPES.concat(RECON_TYPES));
  }
}
