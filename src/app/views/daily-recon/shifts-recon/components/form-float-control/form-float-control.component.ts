import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IFloatControlItem } from '../../models/shift';
import { ShiftsUpdateItemsService } from '../../services/shifts-update-items.service';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

export interface IFloatSubmit {
  [key: string]: IFloatControlItem;
}

@Component({
  selector: 'daccs-form-float-control',
  templateUrl: './form-float-control.component.html',
  styleUrls: ['./form-float-control.component.scss']
})
export class FormFloatControlComponent implements OnChanges, OnDestroy {

  @Input() public floatControlData: IFloatControlItem;
  @Input() public mongoId: string;
  @Output() dirtyShortsOversForm = new EventEmitter();

  public movement = 0;
  public busy$: Observable<boolean>;
  public loaderImg = 'assets/images/ui/loader.svg';
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;
  public floatControlReconForm: FormGroup;

  private _formSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService,
              private _shiftsUpdateService: ShiftsUpdateItemsService) {
    this.busy$ = this._initializerService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;

    this.floatControlReconForm = this._fb.group({
      opening_float: ['', Validators.required],
      closing_float: ['', Validators.required]
    });

    this._formSub = this.floatControlReconForm.valueChanges.subscribe((val) => {
      this._getMovement(val.closing_float, val.opening_float);
    });
  }

  public onSubmit(body: IFloatControlItem) {
    const submitData: IFloatSubmit = {'float_control': body};
    this._initializerService.serverIsRunning$.next(true);
    this.dirtyShortsOversForm.emit(false);
    this._shiftsUpdateService.updateReconItemByShift(submitData, this.mongoId)
      .subscribe(
        () => {
          this._initializerService.serverIsRunning$.next(false);
          this._router.navigate(['../../shorts', this.mongoId], {relativeTo: this._actRoute});
        },
        err => {
          this._initializerService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  private _getMovement(closing: number, opening: number): void {
    this.movement = (closing - opening) || 0;
  }

  ngOnChanges(): void {
    if (this.floatControlData) {
      this._getMovement(this.floatControlData.closing_float, this.floatControlData.opening_float);
      this.floatControlReconForm.setValue({
        opening_float: this.floatControlData.opening_float,
        closing_float: this.floatControlData.closing_float
      });
    }
  }

  ngOnDestroy(): void {
    if (this._formSub) {
      this._formSub.unsubscribe();
    }
  }

}
