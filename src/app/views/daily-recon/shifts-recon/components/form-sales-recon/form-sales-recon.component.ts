import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { ISalesItem } from '../../models/shift';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ShiftsUpdateItemsService } from '../../services/shifts-update-items.service';
import { Subscription } from 'rxjs/Subscription';
import { ISubmittedData } from '../form-core-shift/form-core-shift.component';
import { ActivatedRoute, Router } from '@angular/router';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

interface ISalesSubmission {
  sales: Array<ISalesItem>;
}

@Component({
  selector: 'daccs-form-sales-recon',
  templateUrl: './form-sales-recon.component.html',
  styleUrls: ['./form-sales-recon.component.scss']
})
export class FormSalesReconComponent implements OnChanges, OnDestroy {

  @Input() public salesData: Array<ISalesItem>;
  @Input() public mongoId: string;
  @Input() public shiftId: number;
  @Output() dirtySalesForm = new EventEmitter();

  public salesReconForm: FormGroup;
  public amountTotal: number;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;

  private _valueChangeSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _settingsService: InitializerService,
              private _initializerService: InitializerService,
              private _reconItemsService: ShiftsUpdateItemsService) {
    this._createForm();
    this._valueChangeSub = this.salesReconForm.valueChanges.subscribe((data: ISalesSubmission) => {
      this.dirtySalesForm.emit(this.salesReconForm.dirty);
      this._getTotalAmounts(data);
    });
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public onSubmit(body: ISubmittedData) {
    console.log(body);
    this.dirtySalesForm.emit(false);
    this._settingsService.serverIsRunning$.next(true);
    this._reconItemsService.updateReconItemByShift(body, this.mongoId)
      .subscribe(
        () => {
          this._settingsService.serverIsRunning$.next(false);
          this._router.navigate(['../../cash-payouts', this.shiftId], {relativeTo: this._actRoute});
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  private _createForm() {
    this.salesReconForm = this._fb.group({
      'sales': this._fb.array([]),
    });
  }

  private _getTotalAmounts(rowItemData: ISalesSubmission) {
    this.amountTotal = 0;
    rowItemData.sales.forEach((rowItem: ISalesItem) => {
      this.amountTotal += +rowItem.amount;
    });
  }

  ngOnChanges(): void {

    if (this.salesReconForm) {
      this.salesReconForm.markAsPristine();
      this.dirtySalesForm.emit(this.salesReconForm.dirty);
    }

    if (this.salesData) {

      const pkgArrayControl: FormArray = this.salesReconForm.get(`sales`) as FormArray;
      pkgArrayControl.controls = []; // reset so fresh cashier is not added to sales form array
      this.salesData.forEach((saleItem: ISalesItem) => {
        const newGroup = this._fb.group({
          '_id_': [saleItem._id_, Validators.required],
          'title': [saleItem.title, Validators.required],
          'category_type': [saleItem.category_type, Validators.required],
          'amount': [saleItem.amount, Validators.required],
        });
        pkgArrayControl.push(newGroup);
      });
    }
  }

  ngOnDestroy(): void {
    if (this._valueChangeSub) {
      this._valueChangeSub.unsubscribe();
    }
  }

}
