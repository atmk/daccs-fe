import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'daccs-form-sales-recon-row',
  templateUrl: './form-sales-recon-row.component.html',
  styleUrls: ['./form-sales-recon-row.component.scss']
})
export class FormSalesReconRowComponent implements OnInit {

  @Input() public arrForm: FormGroup;
  @Input() odd: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
