import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ShiftsUpdateItemsService } from '../../services/shifts-update-items.service';
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import { ILineInput } from './form-configs/input-line.interface';
import {
  IAccSalesItem,
  ICashPayIn,
  ICashPayOut,
  IDepositItem,
  IRecOnAccItem,
  IShortsOversItem
} from '../../models/shift';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';
import {
  IAccHolderConfig,
  IGenLedgerConfig,
  IRecAccConfig,
  ISpeedpointTermsConfig,
  ISupplierConfig,
  IVatConfig
} from '../../../../../initialiaze/models/site-config';
import { ActivatedRoute, Router } from '@angular/router';
import { IBrokenCon, InitializerService } from '../../../../../initialiaze/services/initializer.service';
import { Observable } from 'rxjs/Observable';
import { FORM_ORDER } from '../../../../../shared/constants/form-order';
import { ToasterService } from 'angular2-toaster';
import { IMainRecon } from '../../../../../initialiaze/models/main-recon';

export type DataItem = IShortsOversItem
  | IAccSalesItem
  | ICashPayOut
  | ICashPayIn
  | IRecOnAccItem
  | IDepositItem

export interface ISubmittedData {
  [key: string]: Array<DataItem>;
}

@Component({
  selector: 'daccs-form-core',
  templateUrl: './form-core-shift.component.html',
  styleUrls: ['./form-core-shift.component.scss']
})
export class FormCoreShiftComponent implements OnChanges, OnDestroy {

  @Input() public mongoId: string;
  @Input() public reconItemData: Array<DataItem>;
  @Input() public reconItemFields: Array<ILineInput>;

  @Input() public genLedgers: Array<IGenLedgerConfig>;
  @Input() public suppliers: Array<ISupplierConfig>;
  @Input() public accHolders: Array<IAccHolderConfig>;
  @Input() public vatCodes: Array<IVatConfig>;
  @Input() public recAccDesc: Array<IRecAccConfig>;
  @Input() public speedPointTerms: Array<ISpeedpointTermsConfig>;

  @Input() private shiftId: string;

  @Input('reconItemName')
  set reconItemName(value: string) {
    this.reconKeyName = UtilityFunctions.underscoreify(value);
  }

  @Output() dirtyForm = new EventEmitter();

  public reconKeyName: string;
  public reconItemForm: FormGroup;
  public amountTotal: number;
  public columns: Array<number>;
  public cols: number;
  public colPercent: number;
  public loaderImg = 'assets/images/ui/loader.svg';
  public busy$: Observable<boolean>;
  public conbroken$: Observable<IBrokenCon>;
  public mainRecon$: Observable<IMainRecon>;

  private _numberCols: number;
  private _valueChangeSub: Subscription;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _settingsService: InitializerService,
              private _initializerService: InitializerService,
              private _reconItemsService: ShiftsUpdateItemsService) {
    this.busy$ = this._settingsService.serverIsRunning$;
    this.conbroken$ = this._initializerService.conbroken$;
    this.mainRecon$ = this._initializerService.mainRecon$;
  }

  public onSubmit(body: ISubmittedData): void {
    this.dirtyForm.emit(false);
    this._settingsService.serverIsRunning$.next(true);
    this._reconItemsService.updateReconItemByShift(body, this.mongoId)
      .subscribe(
        () => {
          this.dirtyForm.emit(false);
          this._settingsService.serverIsRunning$.next(false);
          const currUrl = this._actRoute.snapshot.url[0].path;
          const pos = FORM_ORDER.findIndex(ordItm => ordItm.name === currUrl);
          const nextUrl = '../' + FORM_ORDER[pos + 1].route; // the '../' is so that this is reuasble in EOD / easier to add then substract
          this._router.navigate([`${nextUrl}`, this.shiftId], {relativeTo: this._actRoute});
        },
        err => {
          this._settingsService.serverIsRunning$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          console.log(err);
        }
      );
  }

  public getFormArrayControls(form: FormGroup, cntrl: string): FormControl[] {
    return form.get(cntrl)['controls'];
  }

  public addInputRow(): void {
    const newInputObj = {};
    this.reconItemFields.forEach((field: ILineInput) => {
      const validArr = [];
      if (field.int) {
        validArr.push(CustomSyncValidators.integerValidator)
      }
      if (field.required) {
        validArr.push(Validators.required)
      }
      newInputObj[field.name] = [null, Validators.compose(validArr)]
    });
    const arrayControl: FormArray = this.reconItemForm.get(this.reconKeyName) as FormArray;
    const newGroup = this._fb.group(newInputObj);
    arrayControl.push(newGroup);
  }

  public delInputRow(index: number): void {
    this.reconItemForm.controls[this.reconKeyName].markAsDirty();
    const arrayControl: FormArray = this.reconItemForm.get(this.reconKeyName) as FormArray;
    arrayControl.removeAt(index);
  }

  private _getTotalAmounts(rowItemData: Array<ISubmittedData>) {

    const firstKey = Object.keys(rowItemData)[0];
    const fieldArr = rowItemData[firstKey];

    this.amountTotal = 0;

    fieldArr.forEach((rowItem: DataItem) => {
      this.amountTotal += +rowItem.amount;
    });
  }

  private _calculateColWidth(): void {
    this._numberCols = 0;
    this.reconItemFields.forEach((field: ILineInput) => {
      this._numberCols += field.width;
    });
    this.columns = Array(this._numberCols).fill(1).map((x, i) => i);
    this.columns.splice(-2);
  }

  ngOnChanges(): void {

    this.amountTotal = 0;

    if (this.reconItemData && this.reconItemFields) {

      this.cols = this.reconItemFields.reduce((acc, itm: ILineInput) => {
        return acc + itm.width;
      }, 0) + 1; // plus one for del col

      this.colPercent = 100 / this.cols; // 12 because of tbs grid

      // just work out the columns so i can place the totals div nicel
      this._calculateColWidth();

      const formGrpObject = {};
      formGrpObject[this.reconKeyName] = this._fb.array([]);

      this.reconItemForm = this._fb.group(formGrpObject);

      if (this._valueChangeSub) {
        this._valueChangeSub.unsubscribe();
      }
      this._valueChangeSub = this.reconItemForm.valueChanges
        .subscribe((formData: Array<ISubmittedData>) => {
          this.dirtyForm.emit(this.reconItemForm.dirty);
          this._getTotalAmounts(formData);
        });

      const cashPayoutsArrayControl: FormArray = this.reconItemForm.get(this.reconKeyName) as FormArray;
      this.reconItemData.forEach((item: any) => {
        const dynFormGroupObj = {};
        this.reconItemFields.forEach(field => {
          const validArr = [];
          if (field.int) {
            validArr.push(CustomSyncValidators.integerValidator)
          }
          if (field.required) {
            validArr.push(Validators.required)
          }
          dynFormGroupObj[field.name] = [item[field.name], Validators.compose(validArr)];
        });
        const newGroup = this._fb.group(dynFormGroupObj);
        cashPayoutsArrayControl.push(newGroup);
      });

    }
  }

  ngOnDestroy(): void {
    if (this._valueChangeSub) {
      this._valueChangeSub.unsubscribe();
    }
  }

}
