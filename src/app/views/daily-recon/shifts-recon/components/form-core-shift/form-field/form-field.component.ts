import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import {
  IAccHolderConfig,
  IGenLedgerConfig,
  IRecAccConfig,
  ISpeedpointTermsConfig,
  ISupplierConfig,
  IVatConfig
} from '../../../../../../initialiaze/models/site-config';
import { ILineInput } from '../form-configs/input-line.interface';
import { GLTypes } from '../../../../../../shared/constants/gl-types';
import { IOption } from 'ng-select';

type Setting =
  ISupplierConfig
  | IGenLedgerConfig
  | IAccHolderConfig
  | IVatConfig
  | IRecAccConfig
  | ISpeedpointTermsConfig

@Component({
  selector: 'daccs-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnChanges {

  @Input() public arrForm: FormGroup;
  @Input() public reconItemFields: Array<ILineInput>;

  @Input() public suppliers: Array<ISupplierConfig>;
  @Input() public genLedgers: Array<IGenLedgerConfig>;
  @Input() public accHolders: Array<IAccHolderConfig>;
  @Input() public vatCodes: Array<IVatConfig>;
  @Input() public recAccDesc: Array<IRecAccConfig>;
  @Input() public speedPointTerms: Array<ISpeedpointTermsConfig>;

  @Output() rowDeleted = new EventEmitter();

  public selectValues: Array<IOption>;
  public cols: number;
  public colPercent: number;

  constructor() {
  }

  public compareFn(s1: Setting, s2: Setting): boolean {
    return s1 && s2 ? s1._id_ === s2._id_ : true;
  }

  public onDelete(): void {
    this.rowDeleted.emit();
  }

  private _getSelectValues(selectType: string): Array<any> {

    switch (selectType) {
      case 'supplier':
        return this.suppliers.map((itm: ISupplierConfig) => {
          return {label: itm.title, value: itm}
        });
      case 'debtor':
        return this.accHolders.map((itm: IAccHolderConfig) => {
          return {label: itm.title, value: itm}
        });
      case 'general_ledger':
        return this.genLedgers.map((itm: IGenLedgerConfig) => {
          return {label: itm.code + ' - ' + itm.title + ' - ' + GLTypes[itm.ledger_type], value: itm}
        });
      case 'vat_code':
        return this.vatCodes.map((itm: IVatConfig) => {
          return {label: itm.description, value: itm}
        });
      case 'description':
        return this.recAccDesc.map((itm: IRecAccConfig) => {
          return {label: itm.description, value: itm}
        });
      case 'speedpoint_terminal':
        return this.speedPointTerms.map((itm: ISpeedpointTermsConfig) => {
          return Object.assign({}, itm, {
            label: itm.description + ' - ' + itm.terminal_number,
            value: itm
          })
        });
      default:
        return [];
    }
  }

  ngOnChanges(): void {

    this.cols = this.reconItemFields.reduce((acc, itm: ILineInput) => {
      return acc + itm.width;
    }, 0) + 1; // plus one for del col

    this.colPercent = 100 / this.cols; // 12 because of tbs grid

    this._getSelectValues('');
    this.selectValues = this.reconItemFields.map((itm: any) => {  // TODO TODO TODOTODOTODO TODO TODOTODO TODO
      if (itm.inputType === 'select') {
        return Object.assign({}, itm, {choices: this._getSelectValues(itm.name)})
      } else {
        return itm
      }
    });
  }

}
