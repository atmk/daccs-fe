import { ILineInput } from './input-line.interface';

export const depositsSpeedpointConfig: Array<ILineInput> = [
  {
    name: 'speedpoint_terminal',
    title: 'Terminal',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'batch_number',
    title: 'Batch Number',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
