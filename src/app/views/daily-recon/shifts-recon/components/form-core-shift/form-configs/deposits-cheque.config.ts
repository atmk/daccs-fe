import { ILineInput } from './input-line.interface';

export const depositsChequeConfig: Array<ILineInput> = [
  {
    name: 'ref_number',
    title: 'Cheque No',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    maxLen: 8,
    width: 1
  },
  {
    name: 'description',
    title: 'Payee',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    width: 2
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
