import { cashPayoutsConfig } from './cash-payouts.config';
import { cashPayinsConfig } from './cash-payins.config';
import { depositsCashConfig } from './deposits-cash.config';
import { depositsSpeedpointConfig } from './deposits-speedpoint.config';
import { depositsChequeConfig } from './deposits-cheque.config';
import { depositsEFTConfig } from './deposits-eft.config';
import { accSalesConfig } from './acc-sales.config';
import { recOnAccountConfig } from './rec-on-account.config';
import { depositsVoucherConfig } from './deposits-voucher.config';
import { depositsZapperConfig } from './deposits-zapper.config';
import { depositsSnapScanConfig } from './deposits-snapscan.config';
import { depositsWigroupConfig } from './deposits-wigroup.config';
import { depositsFuelCardConfig } from './deposits-fuel-card';
import { depositsCashOnHandConfig } from './deposits-cash-on-hand';

export const FormConfig = {
  cash_payouts: cashPayoutsConfig,
  cash_payins: cashPayinsConfig,
  received_on_account: recOnAccountConfig,
  account_sales: accSalesConfig,
  cash_deposits: depositsCashConfig,
  cash_on_hand_deposits: depositsCashOnHandConfig,
  speedpoint_deposits: depositsSpeedpointConfig,
  check_deposits: depositsChequeConfig,
  snapscan_deposits: depositsSnapScanConfig,
  zapper_deposits: depositsZapperConfig,
  wigroup_deposits: depositsWigroupConfig,
  voucher_deposits: depositsVoucherConfig,
  eft_deposits: depositsEFTConfig,
  fuel_card_deposits: depositsFuelCardConfig
};
