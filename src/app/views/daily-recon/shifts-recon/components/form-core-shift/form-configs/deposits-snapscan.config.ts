import { ILineInput } from './input-line.interface';

export const depositsSnapScanConfig: Array<ILineInput> = [
  {
    name: 'ref_number',
    title: 'Reference',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    maxLen: 8,
    width: 1
  },
  {
    name: 'description',
    title: 'Description',
    inputType: 'input',
    fieldType: 'text',
    required: false,
    int: false,
    width: 2
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
