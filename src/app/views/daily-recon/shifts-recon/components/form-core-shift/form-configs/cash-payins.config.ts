import { ILineInput } from './input-line.interface';

export const cashPayinsConfig: Array<ILineInput> = [
  {
    name: 'description',
    title: 'Description',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    width: 2
  },
  {
    name: 'ref_number',
    title: 'Reference',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    maxLen: 8,
    width: 1
  },
  {
    name: 'general_ledger',
    title: 'General Ledger',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
