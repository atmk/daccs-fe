export interface ILineInput {
  name: string;
  title: string;
  inputType: string;
  fieldType: string;
  required: boolean;
  int: boolean;
  width: number;
  currency?: boolean;
  maxLen?: number;
}
