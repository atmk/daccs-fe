import { ILineInput } from './input-line.interface';

export const accSalesConfig: Array<ILineInput> = [
  {
    name: 'debtor',
    title: 'Debtor',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'description',
    title: 'Description',
    inputType: 'input',
    fieldType: 'text',
    required: false,
    int: false,
    width: 2
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
