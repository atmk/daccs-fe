import { ILineInput } from './input-line.interface';

export const recOnAccountConfig: Array<ILineInput> = [
  {
    name: 'description',
    title: 'Description',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'debtor',
    title: 'Debtor',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
