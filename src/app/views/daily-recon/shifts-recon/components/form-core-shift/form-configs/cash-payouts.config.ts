import { ILineInput } from './input-line.interface';

export const cashPayoutsConfig: Array<ILineInput> = [
  {
    name: 'supplier',
    title: 'Supplier',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'description',
    title: 'Description',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'invoice_number',
    title: 'Invoice Number',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    int: false,
    maxLen: 8,
    width: 1
  },
  {
    name: 'general_ledger',
    title: 'General Ledger',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'vat_code',
    title: 'Vat Code',
    inputType: 'select',
    fieldType: '',
    required: true,
    int: false,
    width: 1
  },
  {
    name: 'amount',
    title: 'Amount',
    inputType: 'input',
    fieldType: 'text',
    required: true,
    currency: true,
    int: true,
    width: 1
  }
];
