import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { INewShiftBody, ShiftsReconService } from './services/shifts-recon.service';
import { IShiftTotal } from './models/shift';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ISiteConfig } from '../../../initialiaze/models/site-config';
import { InitializerService } from '../../../initialiaze/services/initializer.service';

interface IRemEvent {
  del: boolean;
  shiftId: string;
}

// TODO, this must come from somewhere else, unified constant
const DEP_ROUTES = [
  'cash-deposits',
  'cash-on-hand-deposits',
  'speedpoint-deposits',
  'check-deposits',
  'snapscan-deposits',
  'zapper-deposits',
  'voucher-deposits',
  'wigroup-deposits',
  'eft-deposits',
  'fuel-card-deposits'
];

@Component({
  selector: 'daccs-shifts-recon',
  templateUrl: './shifts-recon.component.html',
  styleUrls: ['./shifts-recon.component.scss']
})
export class ShiftsReconComponent implements OnDestroy {

  public shiftSummary$: Observable<Array<IShiftTotal>>;
  public currShiftIndex: string;
  public dynRoute = 'sales';
  public modalIsOpen = false;
  public remModalIsOpen = false;
  public settings$: Observable<ISiteConfig>;
  public newShiftModalOpen$: Observable<boolean>;
  public remEvent: IRemEvent;
  public loading = false;
  public error = false;
  public errorObj: any;

  private _idSub: Subscription;
  private _dynRouteSub: Subscription;
  private _modalOpenSub: Subscription;

  constructor(private _shiftsReconService: ShiftsReconService,
              private _cdr: ChangeDetectorRef,
              private _initService: InitializerService,
              private _router: Router) {
    this.shiftSummary$ = this._shiftsReconService.shiftSummary$;
    this.newShiftModalOpen$ = this._shiftsReconService.newShiftModalOpen$;

    // TODO clarity inputs dont allow async pipes :( for model open
    this._modalOpenSub = this._shiftsReconService.newShiftModalOpen$.subscribe(val => {
      this.modalIsOpen = val
    });

    this._dynRouteSub = this._shiftsReconService.dynRoute$.subscribe((val: string) => {
      this.dynRoute = val;
      this._cdr.detectChanges();
    });

    this._idSub = this._shiftsReconService.currShiftId$.subscribe((val: any) => {
      setTimeout(() => {
        this.currShiftIndex = val;
      })
    });

    this.settings$ = this._initService.settings$;

  }

  public openModal(): void {
    this.error = false;
    this._shiftsReconService.newShiftModalOpen$.next(true);
  }

  public openRemModal(ev): void {
    this.error = false;
    this.remEvent = ev;
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public onSubmitNewShift(body: INewShiftBody) {
    this.error = false;
    this.loading = true;
    if (body.cashier) {
      this._shiftsReconService.addNewShift(body)
        .subscribe(
          (res) => {
            const data = JSON.parse(res._body);
            this.loading = false;
            this._shiftsReconService.newShiftModalOpen$.next(false);
            setTimeout(() => { // timeout as the controller will route to the first shift in arr if this is not there
              this._router.navigate(['daily-recon', 'shifts', 'sales', data.id])
            })
          },
          (err) => {
            this.error = true;
            this.errorObj = err;
            this.loading = false;
          }
        )
    }
  }

  public onDeleteShift(): void {
    this._shiftsReconService.deleteShift(this.remEvent.shiftId);
    this.remModalIsOpen = false;
  }

  public getDynRoute(route: string): string {
    return DEP_ROUTES.includes(route) ? 'deposits/' + route : route;
  }

  public getActive(urlStr: string): boolean {
    return this._router.url.indexOf(urlStr) > -1;
  }

  ngOnDestroy(): void {
    this._idSub.unsubscribe();
    this._dynRouteSub.unsubscribe();
    this._modalOpenSub.unsubscribe();
  }
}
