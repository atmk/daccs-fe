import { IShift } from '../models/shift';
import * as ShiftActions from '../actions/shift.actions';

export type Action = ShiftActions.All;

export function shiftsReducer(state: Array<IShift>, action: Action): Array<IShift> {
  switch (action.type) {

    case ShiftActions.LOAD_SHIFTS:
      return action.payload;

    case ShiftActions.ADD_SHIFT:
      return [...state, action.payload];

    case ShiftActions.REMOVE_SHIFT:
      return state.filter(shift => {
        return shift.id !== action.payload.id;
      });

    case ShiftActions.UPDATE_PARTIAL_SHIFT:
      return state.map((shift: IShift) => {
        const tempobj = {};
        tempobj[action.payload.key] = action.payload.data[action.payload.key];
        return shift.id === action.payload.id
          ? Object.assign({}, shift, tempobj)
          : shift;
      });

    default:
      return state;
  }
}
