import { IShift, IShiftTotal } from '../models/shift';
import * as ShiftActions from '../actions/shift.actions';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';

export type Action = ShiftActions.All;

export function shiftsSummaryReducer(state: Array<IShiftTotal>, action: Action): Array<IShiftTotal> {
  switch (action.type) {
    case ShiftActions.LOAD_SHIFT_TOTALS:
      return action.payload;
    case ShiftActions.UPDATE_SHIFT_TOTAL:
      const section: string = action.payload.key;

      let updated;
      if (section === 'sales') {
        updated = state.map(shift => {
          return shift.id === action.payload.id ? Object.assign({}, shift, action.payload.data, {shorts: newShorts}) : shift;
        });
      } else if (section === 'float_control') {
        const movement = action.payload.data.float_control.closing_float - action.payload.data.float_control.opening_float;
        updated = state.map(shift => {
          return shift.id === action.payload.id
            ? Object.assign({}, shift, {float_control: movement}, {shorts: newShorts})
            : shift;
        });
      } else {
        const sectionTally = action.payload.data[section].reduce((acc: number, obj: any) => {
          return (Math.round((acc + +obj.amount ) * 100) / 100)
        }, 0);
        updated = state.map(shift => {
          const obj = {};
          obj[section] = sectionTally;
          return shift.id === action.payload.id ? Object.assign({}, shift, obj, {shorts: newShorts}) : shift;
        });
      }

      const shifty = updated.find(shiftItm => shiftItm.id === action.payload.id);
      const newShorts = UtilityFunctions.getTotalShortsSimple(shifty);

      return updated.map(shift => {
        return shift.id === action.payload.id
          ? Object.assign({}, shift, {shorts: newShorts})
          : shift;
      });

    case ShiftActions.UPDATE_SHIFT_META:
      return state.map((shift: IShiftTotal) => {
        const tempobj = {};
        tempobj[action.payload.key] = action.payload.data[action.payload.key];
        return shift.id === action.payload.id
          ? Object.assign({}, shift, tempobj)
          : shift;
      });

    case ShiftActions.ADD_SHIFT_TOTAL:
      return [...state, action.payload[0]];
    case ShiftActions.REMOVE_SHIFT_TOTAL:
      return state.filter(shift => {
        return shift.id !== action.payload.id;
      });
    default:
      return state;
  }
}
