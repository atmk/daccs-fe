import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { ShiftsReconService } from '../services/shifts-recon.service';
import * as ShiftActions from '../actions/shift.actions';
import 'rxjs/add/operator/do'

@Injectable()
export class LoadShiftsEffects {

  @Effect({dispatch: false}) shifts$ = this.actions$
    .ofType(ShiftActions.LOAD_SHIFTS)
    .do((action: ShiftActions.All) => {
      if (action.payload.length) {
        this._cashiersService.firstShiftID$.next(action.payload[0].id);
      } else {
        this._cashiersService.firstShiftID$.next(0);
      }
    });

  constructor(private _cashiersService: ShiftsReconService,
              private actions$: Actions) {
  }
}
