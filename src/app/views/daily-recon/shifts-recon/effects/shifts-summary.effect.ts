import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/operator/switchMap';
import 'rxjs/add/operator/catch'
import 'rxjs/add/observable/of';
import * as ShiftActions from '../actions/shift.actions';
import { ShiftsReconService } from '../services/shifts-recon.service';
import { IShiftTotal } from '../models/shift';
import { SHIFT_META_KEYS } from '../../../../shared/constants/shift-non-update-keys';

@Injectable()
export class ShiftsSummaryEffects {

  @Effect() loadShifts$ = this.actions$
    .ofType(ShiftActions.LOAD_SHIFTS)
    .switchMap((action: ShiftActions.All) => {
      const summArr: Array<IShiftTotal> = this._shiftsReconService.summarizeShifts(action.payload);
      return Observable.of(new ShiftActions.LoadAllShiftSummary(summArr))
    })
    .catch((e) => {
      return Observable.throw(e);
    });

  @Effect() updateShift$ = this.actions$
    .ofType(ShiftActions.UPDATE_PARTIAL_SHIFT)
    .switchMap((action: ShiftActions.All): any => {
      if (SHIFT_META_KEYS.indexOf(action.payload.key) === -1) {
        return Observable.of(new ShiftActions.UpdateShiftSummary(action.payload))
      } else {
        return Observable.of(new ShiftActions.UpdateShiftSummaryMeta(action.payload))
      }
    })
    .catch((e) => {
      return Observable.throw(e);
    });

  @Effect() addShift$ = this.actions$
    .ofType(ShiftActions.ADD_SHIFT)
    .switchMap((action: ShiftActions.All) => {
      const summArr: Array<IShiftTotal> = this._shiftsReconService.summarizeShifts([action.payload]);
      return Observable.of(new ShiftActions.AddShiftSummary(summArr))
    })
    .catch((e) => {
      return Observable.throw(e);
    });

  constructor(private actions$: Actions, private _shiftsReconService: ShiftsReconService) {
  }
}
