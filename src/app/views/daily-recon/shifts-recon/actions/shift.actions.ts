import { Action } from '@ngrx/store';
import { IShift, IShiftTotal } from '../models/shift';
import { IPartialPayload } from '../../../../initialiaze/initialiaze.component';

export const LOAD_SHIFTS = 'LOAD_SHIFTS';
export const ADD_SHIFT = 'ADD_SHIFT';
export const UPDATE_PARTIAL_SHIFT = 'UPDATE_PARTIAL_SHIFT';
export const REMOVE_SHIFT = 'REMOVE_SHIFT';

export const LOAD_SHIFT_TOTALS = 'LOAD_SHIFT_TOTALS';
export const UPDATE_SHIFT_TOTAL = 'UPDATE_SHIFT_TOTAL';
export const UPDATE_SHIFT_META = 'UPDATE_SHIFT_META';
export const ADD_SHIFT_TOTAL = 'ADD_SHIFT_TOTAL';
export const REMOVE_SHIFT_TOTAL = 'REMOVE_SHIFT_TOTAL';

export class LoadShifts implements Action {
  readonly type = LOAD_SHIFTS;

  constructor(public payload: Array<IShift>) {
  }
}

export class UpdatePartialShift implements Action {
  readonly type = UPDATE_PARTIAL_SHIFT;

  constructor(public payload: IPartialPayload) {
  }
}

//  Adding / removing single cashiers

export class AddNewShift implements Action {
  readonly type = ADD_SHIFT;

  constructor(public payload: IShift) {
  }
}

export class RemoveShift implements Action {
  readonly type = REMOVE_SHIFT;

  constructor(public payload: { id: string }) {
  }
}

// Actions for Cashier Summaries and Totals:

export class LoadAllShiftSummary implements Action {
  readonly type = LOAD_SHIFT_TOTALS;

  constructor(public payload: Array<IShiftTotal>) {
  }
}

export class UpdateShiftSummary implements Action {
  readonly type = UPDATE_SHIFT_TOTAL;

  constructor(public payload: any) {
  }
}

export class UpdateShiftSummaryMeta implements Action {
  readonly type = UPDATE_SHIFT_META;

  constructor(public payload: any) {
  }
}

export class AddShiftSummary implements Action {
  readonly type = ADD_SHIFT_TOTAL;

  constructor(public payload: any) {
  }
}

export class RemoveShiftSummary implements Action {
  readonly type = REMOVE_SHIFT_TOTAL;

  constructor(public payload: any) {
  }
}

export type All
  = LoadShifts
  | UpdatePartialShift
  | AddNewShift
  | RemoveShift
  | LoadAllShiftSummary
  | UpdateShiftSummary
  | UpdateShiftSummaryMeta
  | AddShiftSummary
  | RemoveShiftSummary
