import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { IShift, IShiftTotal } from '../models/shift';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { RECON_TYPES } from '../../../../shared/constants/recon-types';
import { DEPOSIT_TYPES } from '../../../../shared/constants/deposits-types';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../environments/environment';
import { InitializerService } from '../../../../initialiaze/services/initializer.service';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';

export interface INewShiftBody {
  cashier: string;
  shift_number: string;
}

export interface IUpdateShiftBody {
  cashier: string;
  shift_number: string;
}

@Injectable()
export class ShiftsReconService {

  private _apiBaseUrl = environment.apiBaseUrl;

  public shifts$: Observable<Array<IShift>>;
  public shiftSummary$: Observable<Array<IShiftTotal>>;

  public firstShiftID$ = new BehaviorSubject(0);
  public currShiftId$: BehaviorSubject<string | number> = new BehaviorSubject(0);
  public dynRoute$ = new Subject();
  public newShiftModalOpen$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private _currDate: string;

  constructor(private _store: Store<IAppStore>,
              private _settingsService: InitializerService,
              public authHttp: AuthHttp) {
    this.shifts$ = _store.select('shiftsReducer');
    this.shiftSummary$ = _store.select('shiftsSummaryReducer');

    this._settingsService.currDate$.subscribe(currDate => {
      this._currDate = currDate;
    });
  }

  public addNewShift(body: INewShiftBody): any {
    body['date'] = this._currDate;
    return this.authHttp.post(`${this._apiBaseUrl}/recons/shift/create/`, JSON.stringify(body))
  }

  public deleteShift(shiftId: string): any {
    this.authHttp.delete(`${this._apiBaseUrl}/recons/shift/delete/${shiftId}/`)
      .subscribe(
        () => {
          console.log('Deleted', shiftId);
        },
        err => console.log(err));
  }

  public updateShift(body: IUpdateShiftBody, id: string): any {
    return this.authHttp.patch(`${this._apiBaseUrl}/recons/shift/update/${id}/`, JSON.stringify(body))
  }

  public summarizeShifts(shifts: Array<IShift>): Array<IShiftTotal> {

    const tallyKeys = (DEPOSIT_TYPES.concat(RECON_TYPES)).concat('shorts');

    return shifts.map((shiftItm: IShift) => {

      const cashSumObj: IShiftTotal = {
        name: shiftItm.name,
        id: shiftItm.id,
        _id_: shiftItm._id_,
        shift_number: shiftItm.shift_number
      };
      tallyKeys.forEach((tkey: string) => {
        if (tkey === 'float_control') {
          cashSumObj[tkey] = shiftItm[tkey].closing_float - shiftItm[tkey].opening_float;
        } else if (shiftItm[tkey]) {
          cashSumObj[tkey] = shiftItm[tkey].reduce((acc: number, obj: any) => {
            return (Math.round((acc + +obj.amount ) * 100) / 100);
          }, 0);
        } else {
          cashSumObj[tkey] = null
        }

      });
      cashSumObj['sales'] = shiftItm.sales;
      cashSumObj['shorts'] = UtilityFunctions.getTotalShortsArray(shiftItm);
      return cashSumObj
    });

  }

  public getShiftNameById(id: string): string {
    const shiftObs: Observable<Array<IShiftTotal>> = this.shiftSummary$.take(1);
    let name = '';
    shiftObs.subscribe(shifts => {
      const shift = shifts.find(itm => itm.id === id);
      name = shift.name
    });
    return name
  }

}
