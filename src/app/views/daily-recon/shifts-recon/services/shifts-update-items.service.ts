import { Injectable } from '@angular/core';
import { ISubmittedData } from '../components/form-core-shift/form-core-shift.component';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { IShift } from '../models/shift';
import { IFloatSubmit } from '../components/form-float-control/form-float-control.component';

@Injectable()
export class ShiftsUpdateItemsService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public updateReconItemByShift(body: ISubmittedData | IFloatSubmit, id: string): Observable<IShift> {
    return this._authHttp.patch(`${this._apiBaseUrl}/recons/shift/update/${id}/`, JSON.stringify(body))
      .map(res => res.json())
  }

}
