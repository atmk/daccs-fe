import {
  IAccHolderConfig, IGenLedgerConfig, ISupplierConfig,
  IVatConfig
} from '../../../../initialiaze/models/site-config';

export interface IShift {
  id: string;
  _id_: number;
  date: string;
  shift_number: number;
  cashier_id: string;
  site: number;
  name: string;
  sales: Array<ISalesItem>;
  cash_payouts: Array<ICashPayOut>;
  cash_payins: Array<ICashPayIn>;
  received_on_account: Array<IRecOnAccItem>;
  account_sales: Array<IAccSalesItem>;
  cash_deposits: Array<IDepositItem>;
  speedpoint_deposits: Array<IDepositItem>;
  check_deposits: Array<IDepositItem>;
  snapscan_deposits: Array<IDepositItem>;
  zapper_deposits: Array<IDepositItem>;
  voucher_deposits: Array<IDepositItem>;
  eft_deposits: Array<IDepositItem>;
  float_control: IFloatControlItem;
  shorts: Array<IShortsOversItem>;
}

export interface ISalesItem {
  amount: string;
  category_type: number;
  title: string;
  _id_: number;
}

export interface ICashPayOut {
  description: string;
  invoice_number: number;
  amount: number;
  supplier: ISupplierConfig;
  general_ledger: IGenLedgerConfig;
  vat_code: IVatConfig;
}

export interface ICashPayIn {
  description: string;
  ref_number: number;
  amount: number;
  general_ledger: IGenLedgerConfig;
}

export interface IRecOnAccItem {
  description?: string;
  amount: number;
  debtor: IAccHolderConfig;
}

export interface IAccSalesItem {
  amount: number;
  description: string;
  debtor: IAccHolderConfig;
}

export interface IDepositItem {
  ref_number: string;
  amount: number;
  description: string;
  debtor?: IAccHolderConfig;
}

export interface IShortsOversItem {
  description: string;
  ref_number: string;
  amount: number;
}

export interface IFloatControlItem {
  opening_float: number;
  closing_float: number;
}

export interface IShiftTotal {
  id: string;
  _id_: number;
  shift_number?: string | number;
  name: string;
  sales?: Array<ISalesItem>;
  cash_payouts?: number;
  cash_payins?: number;
  received_on_account?: number;
  account_sales?: number;
  cash_deposits?: number;
  check_deposits?: number;
  snapscan_deposits?: number;
  zapper_deposits?: number;
  voucher_deposits?: number;
  eft_deposits?: number;
  fuel_card_deposits?: number;
  speedpoint_deposits?: number;
  shorts?: number;
  float_control?: number;
}
