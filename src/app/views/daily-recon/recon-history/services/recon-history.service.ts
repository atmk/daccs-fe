import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { IHistItem } from '../models/hist-item';

@Injectable()
export class ReconHistoryService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public getHistoryItems(): Observable<Array<IHistItem>> {
    return this._authHttp.get(`${this._apiBaseUrl}/recons/recon/`)
      .map(res => res.json())
  }

  public deleteHistoryItem(id): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/recons/recon/${id}/`)
      .map(res => res.json())
  }

}
