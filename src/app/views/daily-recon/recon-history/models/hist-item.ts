export interface IHistItem {
  id: number;
  site: number;
  created_by: number;
  created_by_fullname: string;
  completed_by: string;
  completed_by_fullname: string;
  date_created: string;
  recon_date: string;
  complete: boolean;
}
