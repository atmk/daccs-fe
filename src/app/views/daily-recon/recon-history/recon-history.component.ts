import { Component, OnInit } from '@angular/core';
import { ReconHistoryService } from './services/recon-history.service';
import { ToasterService } from 'angular2-toaster';
import { IHistItem } from './models/hist-item';

@Component({
  selector: 'daccs-recon-history',
  templateUrl: './recon-history.component.html',
  styleUrls: ['./recon-history.component.scss'],
  providers: [ReconHistoryService]
})
export class ReconHistoryComponent implements OnInit {

  public histItems: Array<IHistItem>;
  public remModalIsOpen = false;
  public loading = false;
  public reconIdSelected: number;

  constructor(private _reconHistoryService: ReconHistoryService, private _toasterService: ToasterService) {
  }

  public onDeleteItem(): void {
    this.loading = true;
    this._reconHistoryService.deleteHistoryItem(this.reconIdSelected)
      .subscribe(() => {
        this.histItems = this.histItems.filter((itm: IHistItem) => {
          return itm.id !== this.reconIdSelected;
        });
        this.loading = false;
        this._toasterService.pop('success', 'Success',
          'Recon Item Deleted');
      }, err => {
        let msg = '';
        err.status === 403 ? msg = 'You do not have permission to perform this action' : msg = err.statusText;
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${msg}`);
        this.loading = false;
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(id: number): void {
    this.reconIdSelected = id;
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnInit(): void {
    this.loading = true;
    this._reconHistoryService.getHistoryItems()
      .subscribe(data => {
        this.histItems = data;
        this.loading = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.loading = false;
      })
  }

}
