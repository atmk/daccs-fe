import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IHistItem } from '../models/hist-item';
import { InitializerService } from '../../../../initialiaze/services/initializer.service';
import { IMainRecon } from '../../../../initialiaze/models/main-recon';
import * as mainReconActions from '../../../../initialiaze/actions/main-recon.actions'
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'daccs-recon-history-item',
  templateUrl: './recon-history-item.component.html',
  styleUrls: ['./recon-history-item.component.scss']
})
export class ReconHistoryItemComponent implements OnInit {

  @Input() histItm: IHistItem;
  @Input() odd: boolean;
  @Input() even: boolean;

  @Output() deleteHistItm$ = new EventEmitter();

  public busy = false;

  constructor(private _initializerService: InitializerService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {
  }

  public handleDeleteHistItm(): void {
    this.deleteHistItm$.emit(this.histItm.id)
  }

  public toggle(ev: boolean): void {
    this.busy = true;
    this._initializerService.closeRecon({complete: ev}, this.histItm.id)
      .subscribe((val: IMainRecon) => {
        this._store.dispatch(new mainReconActions.CompleteMainRecon(val.complete));
        this.busy = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        console.log(err);
      });
  }

  ngOnInit() {
  }

}
