import { ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { IEODData } from '../eod-recon/models/eod';
import { IShift } from '../shifts-recon/models/shift';
import { IHeaderDataItem, IShort } from './models/live-sales-data';
import { IMainRecon } from '../../../initialiaze/models/main-recon';
import { InitializerService } from '../../../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';
import { IAppStore } from '../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as mainReconActions from '../../../initialiaze/actions/main-recon.actions';
import { ToasterService } from 'angular2-toaster';
import { LiveTallyService } from './services/live-tally.service';

@Component({
  selector: 'daccs-live-header',
  templateUrl: './live-header.component.html',
  styleUrls: ['./live-header.component.scss'],
  providers: [LiveTallyService]
})

export class LiveHeaderComponent implements OnChanges, OnDestroy {

  @Input() public EODData: IEODData;
  @Input() public ShiftsData: Array<IShift>;

  public mainRecon: IMainRecon;
  public salesDataArr: Array<IHeaderDataItem>;
  public depositData: Array<IHeaderDataItem>;
  public reconData: Array<IHeaderDataItem>;
  public shortDataShifts: IShort = <IShort>{};
  public shortDataEOD: number;
  public discrepTally: any;
  public loading = false;
  public remModalIsOpen = false;
  public grandVar: { gc: number, vr: number };
  public reconValid = false;

  private _mainreconSub: Subscription;

  constructor(private _cdr: ChangeDetectorRef,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService,
              private _liveTallyService: LiveTallyService,
              private _store: Store<IAppStore>) {
    this._mainreconSub = this._initializerService.mainRecon$.subscribe((val: IMainRecon) => {
      this.mainRecon = val;
    });
  }

  public closeOffRecon(id: number): void {
    this.loading = true;
    this._initializerService.closeRecon({complete: true}, id)
      .subscribe((val: IMainRecon) => {
        this._store.dispatch(new mainReconActions.CompleteMainRecon(val.complete));
        this.loading = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        console.log(err);
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public handleVarianceCheck(ev: { gc: number, vr: number }): void {
    this.grandVar = ev;
    this.reconValid = ev.gc === 0 && ev.vr === 0;
    this._cdr.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {

    if (this.EODData && this.ShiftsData) {
      const tallyObj = this._liveTallyService.getTallyObject(this.EODData, this.ShiftsData);
      this.salesDataArr = tallyObj.salesDataArr;
      this.depositData = tallyObj.depositData;
      this.reconData = tallyObj.reconData;
      this.shortDataShifts = Object.assign({}, tallyObj.shortDataShifts);
      this.shortDataEOD = tallyObj.shortDataEOD;

      this.discrepTally = this._liveTallyService.getTotalDiscrep(tallyObj.reconData, tallyObj.depositData);

    }

  }

  ngOnDestroy(): void {
    this._mainreconSub.unsubscribe();
  }

}
