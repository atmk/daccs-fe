import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { IShort } from '../models/live-sales-data';
import { flashTransition } from '../../../../shared/animations/daccs.animation';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-shorts-header',
  templateUrl: './header-table-shorts.component.html',
  styleUrls: ['./header-table-shorts.component.scss'],
  animations: [flashTransition()],
})
export class HeaderTableShortsComponent implements OnChanges {

  @Input() public shortDataShifts: IShort;
  @Input() public shortDataEOD: number;
  @Input() public discTally: number;

  @Output() public grandVariance$ = new EventEmitter();

  public grandCheck: number;
  public cashierShorts: number;

  ngOnChanges(): void {
    this.grandCheck = UtilityFunctions.rounder(this.shortDataShifts.system - this.shortDataEOD);
    this.cashierShorts = UtilityFunctions.rounder(this.shortDataShifts.system - this.shortDataShifts.explained);

    this.grandVariance$.emit({gc: this.grandCheck, vr: this.cashierShorts});
  }

}
