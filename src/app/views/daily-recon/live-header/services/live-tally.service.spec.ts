import { TestBed, inject } from '@angular/core/testing';

import { LiveTallyService } from './live-tally.service';

describe('LiveTallyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiveTallyService]
    });
  });

  it('should be created', inject([LiveTallyService], (service: LiveTallyService) => {
    expect(service).toBeTruthy();
  }));
});
