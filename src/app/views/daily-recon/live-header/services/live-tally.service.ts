import { Injectable } from '@angular/core';
import {
  IAccSalesItem,
  ICashPayIn,
  ICashPayOut,
  IDepositItem,
  IFloatControlItem,
  IRecOnAccItem,
  ISalesItem,
  IShift,
  IShortsOversItem
} from '../../shifts-recon/models/shift';
import { IEODCategory, IEODData, IEODItem, IEODSubCategory, ISalesEODItem, ISpeedPointDepEOD } from '../../eod-recon/models/eod';
import { IAllReconTotals, IHeaderDataItem, IShort } from '../models/live-sales-data';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';
import { CalcFunctions } from '../../../../shared/utils/calc-functions';
import { DEPOSIT_TYPES } from '../../../../shared/constants/deposits-types';
import { RECON_TYPES } from '../../../../shared/constants/recon-types';

interface IEODReturnObj {
  eodTotal: number;
  discrepTotal: number;
}

export interface ITallyData {
  salesDataArr: Array<IHeaderDataItem>;
  depositData: Array<IHeaderDataItem>;
  reconData: Array<IHeaderDataItem>;
  shortDataShifts: IShort;
  shortDataEOD: number;
}

type Item = IDepositItem & ICashPayOut & ICashPayIn & IRecOnAccItem & IAccSalesItem & IFloatControlItem;

@Injectable()
export class LiveTallyService {

  private _flattenSubCats(salesData: Array<IEODCategory>, flatArr = []): Array<IEODSubCategory> {
    Object.keys(salesData).forEach((key: string) => {
      if (key === 'sub_categories') {
        flatArr.push(...salesData[key]);
        return flatArr;
      }
      if (typeof salesData[key] === 'object') {
        this._flattenSubCats(salesData[key], flatArr);
      }
    });
    return flatArr;
  }

  private _flatten(arr: Array<any>): Array<any> {
    return Array.prototype.concat(...arr);
  }

  private _reduceEODSalesTotals(arrSubCats: Array<IEODSubCategory>): number {
    return arrSubCats.reduce((acc: number, subcat: IEODSubCategory) => {
      const freshSubcat = Object.assign({}, subcat); // avoid mutation
      return acc + CalcFunctions.addVATtoAmnt(freshSubcat, freshSubcat.vat_code.percentage);
    }, 0);
  }

  private _reduceShiftsSalesTotals(shiftArr: Array<IShift>, mainCatTypeId: number): number {
    return shiftArr.reduce((acc: number, shift: IShift) => {
      const freshShift = Object.assign({}, shift);
      const cat = freshShift.sales.find((itm: ISalesItem) => {
        return itm._id_ === mainCatTypeId;
      });
      return acc + +cat.amount;
    }, 0);
  }

  // todo change shift amnt
  // todo change the cashieramount to shiftamount naming

  private _getSalesData(salesData: Array<ISalesEODItem>, shiftData: Array<IShift>): Array<IHeaderDataItem> {
    return salesData.map((mainItem: ISalesEODItem) => {
      const allSubCats: Array<IEODSubCategory> = this._flattenSubCats(mainItem.categories);
      const finalEodAmount: number = this._reduceEODSalesTotals(allSubCats) + +mainItem.discrepancy;
      const finalShiftsAmount: number = this._reduceShiftsSalesTotals(shiftData, mainItem._id_);

      return {
        title: mainItem.title,
        eodAmount: UtilityFunctions.rounder(finalEodAmount),
        cashierAmount: UtilityFunctions.rounder(finalShiftsAmount),
        diffAmount: UtilityFunctions.rounder(finalEodAmount - finalShiftsAmount)
      };
    });
  }

  private _getShiftsHeaderAmount(shiftDepReconData: Array<Item>, itmType?: string): number {
    if (shiftDepReconData.length) {
      return shiftDepReconData.reduce((acc: number, recItm: Item) => {
        if (recItm.amount) {
          return acc + +recItm.amount;
        } else if (itmType === 'float_control') {
          return acc + ( recItm.closing_float - recItm.opening_float);
        } else {
          return 0;
        }
      }, 0);
    } else {
      return 0;
    }
  }

  private _getEODHeaderAmount(eodData: IEODItem & Array<ISpeedPointDepEOD> & number, depOrReconType: string): IEODReturnObj {
    let eodTotal = 0;
    let discrepTotal = 0;
    if (eodData && depOrReconType === 'speedpoint_deposits') {
      eodTotal = eodData.reduce((acc: number, itm: ISpeedPointDepEOD) => {
        const discp = +itm.amount + +itm.discrepancy;
        return acc + discp;
      }, 0);
      discrepTotal = eodData.reduce((acc: number, itm: ISpeedPointDepEOD) => {
        return acc + +itm.discrepancy;
      }, 0);
    } else if (eodData && depOrReconType === 'float_control') {
      eodTotal = eodData;
      discrepTotal = 0;
    } else if (eodData) {
      eodTotal = +eodData.amount;
      eodData.discrepancy ? eodTotal = +eodData.amount + +eodData.discrepancy : +eodData.amount;
      eodData.discrepancy ? discrepTotal = +eodData.discrepancy : discrepTotal = 0;
    }
    return {eodTotal: eodTotal, discrepTotal: discrepTotal};
  }

  private _getHeaderData(eodData: IEODData, shiftData: Array<IShift>, type: string): Array<IHeaderDataItem> {
    let arrType: Array<string>;
    if (type === 'deposits') {
      arrType = DEPOSIT_TYPES;
    } else {
      arrType = RECON_TYPES;
    }
    return arrType.map((depOrReconType: string) => {
      const depReconArr: Array<Array<IDepositItem>> = shiftData.map((shift: IShift) => {
        if (shift[depOrReconType]) {
          return shift[depOrReconType];
        } else {
          return 0;
        }
      });
      const flatArr: Array<Item> = this._flatten(depReconArr);
      const shiftDepsTotal = UtilityFunctions.rounder(this._getShiftsHeaderAmount(flatArr, depOrReconType));
      const EODTotalsDiscreps: IEODReturnObj = this._getEODHeaderAmount(eodData[depOrReconType], depOrReconType);
      const eodTotal = UtilityFunctions.rounder(EODTotalsDiscreps.eodTotal);
      return {
        title: depOrReconType,
        cashierAmount: shiftDepsTotal,
        eodAmount: eodTotal,
        diffAmount: UtilityFunctions.rounder(eodTotal - shiftDepsTotal),
        discAmount: UtilityFunctions.rounder(EODTotalsDiscreps.discrepTotal),
      };
    });
  }

  private _getTotalShorts(data: IAllReconTotals) {

    const totalDeps = DEPOSIT_TYPES.map((depType: string) => {
      return data[depType];
    }).reduce((acc, obj) => {
      return acc + obj;
    });

    let total = data.sales
      - data.cash_payouts
      + data.cash_payins
      + data.received_on_account
      - data.account_sales
      - totalDeps;

    total = total - (data.float_control);

    return UtilityFunctions.rounder(total);
  }

  private _getShortsData(data: any) {
    const shortDataShifts = {explained: 0, system: 0};
    const shortsExplained = data.shortArr.reduce((acc: number, short: IShortsOversItem) => {
      return acc + +short.amount;
    }, 0);

    shortDataShifts.explained = UtilityFunctions.rounder(shortsExplained);

    const allReconTotal: IAllReconTotals = <IAllReconTotals>{};
    [...data.depositData, ...data.reconData].forEach(itm => {
      allReconTotal[itm.title] = itm.cashierAmount;
    });
    allReconTotal['sales'] = data.salesData.reduce((acc: any, obj: IHeaderDataItem) => {
      return acc + +obj.cashierAmount;
    }, 0);
    allReconTotal['float_control'] = data.shifts.map((itm: IShift) => {
      return (itm.float_control.closing_float - itm.float_control.opening_float);
    }).reduce((acc: number, fcArr: number) => {
      return acc + fcArr;
    }, 0);

    shortDataShifts.system = this._getTotalShorts(allReconTotal);
    const shortDataEOD = this._getTotalShortsEOD(data.eodData, data.salesData);
    return {shifts: shortDataShifts, eod: shortDataEOD};
  }

  private _getTotalShortsEOD(data: IEODData, sales: any) {

    const totalDeps = DEPOSIT_TYPES
      .filter((dep: string) => {
        return !Array.isArray(data[dep]);
      })
      .map((itm: string) => {
        if (data[itm]) {
          return UtilityFunctions.rounder(+data[itm].amount + +data[itm].discrepancy);
        } else {
          return 0;
        }
      }).reduce((acc, obj) => {
        return UtilityFunctions.rounder(acc + obj);
      });

    const speedPointTotal = data.speedpoint_deposits.reduce((acc: number, itm: ISpeedPointDepEOD) => {
      const discp = +itm.amount + +itm.discrepancy;
      return acc + discp;
    }, 0);

    const salesEODTotal = sales.reduce((acc: number, itm: IHeaderDataItem) => {
      return acc + itm.eodAmount;
    }, 0);

    let total = salesEODTotal
      - this._getMovementsTotals(data.cash_payouts)
      + this._getMovementsTotals(data.cash_payins)
      + this._getMovementsTotals(data.received_on_account)
      - this._getMovementsTotals(data.account_sales)
      - speedPointTotal
      - totalDeps;

    total -= +data.float_control; // account movement

    return UtilityFunctions.rounder(total);
  }

  private _getMovementsTotals(obj: IEODItem): number {
    if (obj) {
      const discp = obj.discrepancy ? obj.discrepancy : 0;
      return UtilityFunctions.rounder(+obj.amount + +discp);
    } else {
      return 0;
    }
  }

  public getTallyObject(eodData: IEODData, shifts: Array<IShift>): ITallyData {

    const salesData: Array<IHeaderDataItem> = this._getSalesData(eodData.sales, shifts);
    const depositData: Array<IHeaderDataItem> = this._getHeaderData(eodData, shifts, 'deposits');
    const reconData: Array<IHeaderDataItem> = this._getHeaderData(eodData, shifts, 'recons');

    const shortArr: Array<Array<IShortsOversItem>> = shifts.map((shift: IShift) => {
      return shift.shorts;
    });

    const data = {
      shortArr: this._flatten(shortArr),
      depositData: depositData,
      reconData: reconData,
      salesData: salesData,
      shifts: shifts,
      eodData: eodData
    };
    const shortsData: any = this._getShortsData(data);

    return {
      salesDataArr: salesData,
      depositData: depositData,
      reconData: reconData,
      shortDataShifts: shortsData.shifts,
      shortDataEOD: shortsData.eod
    };
  }

  public getTotalDiscrep(recons, deps) {
    const allItms = recons.concat(deps);

    const total = allItms.reduce((acc, itm) => {
      if (isNaN(itm.discAmount)) {
        itm.discAmount = 0;
      }
      return acc + itm.discAmount;
    }, 0);

    return UtilityFunctions.rounder(total);
  }

}
