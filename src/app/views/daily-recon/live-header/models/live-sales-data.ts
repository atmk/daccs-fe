export interface IHeaderDataItem {
  title: string;
  _id_?: number;
  eodAmount: number;
  cashierAmount: number;
  diffAmount?: number;
  discAmount?: number;
}

export interface IShort {
  system: number;
  explained: number;
  variance?: number;
}

export interface IAllReconTotals {
  sales: number;
  cash_payouts: number;
  cash_payins: number;
  received_on_account: number;
  account_sales: number;
  cash_deposits: number;
  speedpoint_deposits: number;
  check_deposits: number;
  eft_deposits: number;
  snapscan_deposits: number;
  zapper_deposits: number;
  voucher_deposits: number;
  float_control: number;
}
