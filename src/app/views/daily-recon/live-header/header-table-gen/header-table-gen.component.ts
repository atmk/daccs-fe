import { Component, Input, OnChanges } from '@angular/core';
import { IHeaderDataItem } from '../models/live-sales-data';
import { flashTransition } from '../../../../shared/animations/daccs.animation';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-gen-header',
  templateUrl: './header-table-gen.component.html',
  styleUrls: ['./header-table-gen.component.scss'],
  animations: [flashTransition()],
})
export class HeaderTableGenComponent implements OnChanges {

  @Input() public dataArr: Array<IHeaderDataItem>;
  @Input() public hideTotals: boolean;
  public totalObj: IHeaderDataItem = <IHeaderDataItem>{};
  public metaArr = [];

  constructor() {
  }

  private _tally(arr: Array<IHeaderDataItem>): any {
    const eodAmount = arr.reduce((acc: number, obj: IHeaderDataItem) => {
      return UtilityFunctions.rounder(acc + +obj.eodAmount);
    }, 0);
    const cashierAmount = arr.reduce((acc: number, obj: IHeaderDataItem) => {
      return UtilityFunctions.rounder(acc + +obj.cashierAmount);
    }, 0);
    const diffAmount = arr.reduce((acc: number, obj: IHeaderDataItem) => {
      return UtilityFunctions.rounder(acc + +obj.diffAmount);
    }, 0);
    if (isNaN(eodAmount) || isNaN(cashierAmount) || isNaN(diffAmount)) {
      throw new Error('Did not receive a valid integer');
    }
    return {
      title: 'Total',
      eodAmount: eodAmount,
      cashierAmount: cashierAmount,
      diffAmount: diffAmount
    }
  }

  trackByFn(index) {
    return index;
  }

  ngOnChanges(): void {

    if (this.dataArr) {

      this.metaArr = [];
      this.dataArr.forEach(arr => {
        const subArr = [];
        subArr.push(arr.title);
        subArr.push(arr.cashierAmount);
        subArr.push(arr.eodAmount);
        subArr.push(arr.diffAmount);
        this.metaArr.push(subArr)
      });

      try {
        this.totalObj = this._tally(this.dataArr)
      } catch (err) {
        console.error(err);
      }
    }
  }

}
