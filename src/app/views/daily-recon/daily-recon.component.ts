import { Component, OnDestroy } from '@angular/core';
import { EODReconService } from './eod-recon/services/eod-recon.service';
import { ShiftsReconService } from './shifts-recon/services/shifts-recon.service';
import { IShift } from './shifts-recon/models/shift';
import { Observable } from 'rxjs/Observable';
import { IEODData } from './eod-recon/models/eod';
import { ISetupReq, InitializerService } from '../../initialiaze/services/initializer.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'daccs-daily-recon',
  templateUrl: './daily-recon.component.html',
  styleUrls: ['./daily-recon.component.scss']
})
export class DailyReconComponent implements OnDestroy {

  public EODData$: Observable<IEODData>;
  public cashierShifts$: Observable<Array<IShift>>;

  private _setupRq: Subscription;
  private needSetup: boolean;

  constructor(private _cashierShiftsReconService: ShiftsReconService,
              private _initializerService: InitializerService,
              private _router: Router,
              private _EODReconService: EODReconService) {
    this.EODData$ = this._EODReconService.EODData$;
    this.cashierShifts$ = this._cashierShiftsReconService.shifts$;

    this._setupRq = this._initializerService.setupRequired$.subscribe((val: ISetupReq) => {
      this.needSetup = val.setup_required;
    });

    if (this.needSetup) {
      this._router.navigate(['/check-setup']);
      this._initializerService.reloadSocket$.next(true);
    }
  }

  ngOnDestroy(): void {
    if (this._setupRq) {
      this._setupRq.unsubscribe();
    }
  }

}
