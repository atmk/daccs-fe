import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DailyReconComponent } from './daily-recon.component';
import { ClarityModule } from 'clarity-angular';
import { ShiftsReconComponent } from './shifts-recon/shifts-recon.component';
import { RouterModule } from '@angular/router';
import { EODReconComponent } from './eod-recon/eod-recon.component';
import { FormShortsOversReconComponent } from './shifts-recon/components/form-shorts-overs-recon/form-shorts-overs-recon.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShiftsUpdateItemsService } from './shifts-recon/services/shifts-update-items.service';
import {
  FormShortsOversFieldComponent
} from './shifts-recon/components/form-shorts-overs-recon/field/form-shorts-overs-field.component';
import { FormCoreShiftComponent } from './shifts-recon/components/form-core-shift/form-core-shift.component';
import { FormFieldComponent } from './shifts-recon/components/form-core-shift/form-field/form-field.component';
import { FormCoreEODComponent } from './eod-recon/components/form-core-eod/form-core-eod.component';
import { EODUpdateItemsService } from './eod-recon/services/eod-update-items.service';
import { SalesEodContainerComponent } from './eod-recon/components/sales-eod-container/sales-eod-container.component';
import { FormSalesEodComponent } from './eod-recon/components/sales-eod-container/form-sales-eod/form-sales-eod.component';
import {
  FormSalesCatergoyComponent
} from './eod-recon/components/sales-eod-container/form-sales-eod/form-sales-category/form-sales-category.component';
import {
  FormSalesRowComponent
} from './eod-recon/components/sales-eod-container/form-sales-eod/form-sales-category/form-sales-row/form-sales-row.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { DepositsEodContainerComponent } from './eod-recon/components/deposits-eod-container/deposits-eod-container.component';
import {
  FormDepositsSpeedpointComponent
} from './eod-recon/components/deposits-eod-container/form-deposits-speedpoint/form-deposits-speedpoint.component';
import {
  FormDepositsSpeedpointRowComponent
} from './eod-recon/components/deposits-eod-container/form-deposits-speedpoint/form-deposits-speedpoint-row/form-deposits-speedpoint-row.component';
import {
  FormSalesReconRowComponent
} from './shifts-recon/components/form-sales-recon/form-sales-recon-row/form-sales-recon-row.component';
import { ShiftControllerComponent } from './shifts-recon/controller/shift-controller.component';
import { ShiftRouteRedirectComponent } from './shifts-recon/controller/shift-route-redirect.component';
import {
  DepositsShiftsContainerComponent
} from './shifts-recon/components/deposits-shifts-container/deposits-shifts-container.component';
import { EodSalesRouteRedirectComponent } from './eod-recon/controller/eod-sales-route-redirect.component';
import { EodControllerComponent } from './eod-recon/controller/eod-controller.component';
import { ShiftItemComponent } from './shifts-recon/components/shift-item/shift-item.component';
import { FormSalesReconComponent } from './shifts-recon/components/form-sales-recon/form-sales-recon.component';
import { LiveHeaderComponent } from './live-header/live-header.component';
import { HeaderTableGenComponent } from './live-header/header-table-gen/header-table-gen.component';
import { HeaderTableShortsComponent } from './live-header/header-table-shorts/header-table-shorts.component';
import { RouteInfoComponent } from './shifts-recon/components/route-info/route-info.component';
import { EODReconService } from './eod-recon/services/eod-recon.service';
import { ShiftsReconService } from './shifts-recon/services/shifts-recon.service';
import { ReconHistoryComponent } from './recon-history/recon-history.component';
import { FormFloatControlComponent } from './shifts-recon/components/form-float-control/form-float-control.component';
import { FormSimpleEodComponent } from './eod-recon/components/form-simple-eod/form-simple-eod.component';
import { GlOrderPipe } from './shifts-recon/pipes/gl-order.pipe';
import { ReconHistoryItemComponent } from './recon-history/recon-history-item/recon-history-item.component';
import { FilesComponent } from './files/files.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ClarityModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    DailyReconComponent,
    ReconHistoryComponent,
    ShiftsReconComponent,
    EODReconComponent,
    FormSalesReconComponent,
    DepositsShiftsContainerComponent,
    FormShortsOversReconComponent,

    // fields
    FormShortsOversFieldComponent,
    FormCoreShiftComponent,
    FormFieldComponent,

    // EOD
    FormCoreEODComponent,
    SalesEodContainerComponent,
    FormSalesEodComponent,
    FormSalesCatergoyComponent,
    FormSalesRowComponent,
    DepositsEodContainerComponent,
    FormDepositsSpeedpointComponent,
    FormDepositsSpeedpointRowComponent,
    FormSalesReconRowComponent,
    ShiftControllerComponent,
    ShiftRouteRedirectComponent,
    EodSalesRouteRedirectComponent,
    EodControllerComponent,
    ShiftItemComponent,
    RouteInfoComponent,
    FormFloatControlComponent,
    FormSimpleEodComponent,
    ReconHistoryItemComponent,

    // Header
    LiveHeaderComponent,
    HeaderTableGenComponent,
    HeaderTableShortsComponent,

    // Pipes
    GlOrderPipe,

    FilesComponent,

    // directives

  ],
  providers: [
    ShiftsReconService,
    EODReconService,
    ShiftsUpdateItemsService,
    EODUpdateItemsService,
    ]
})
export class DailyReconReconModule {
}
