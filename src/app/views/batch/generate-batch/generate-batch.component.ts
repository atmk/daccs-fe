import { Component, OnInit } from '@angular/core';
import { BatchService } from '../services/batch.service';
import { InitializerService } from '../../../initialiaze/services/initializer.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { Subscription } from 'rxjs/Subscription';
import { INITIAL_YEAR } from '../../../shared/constants/reporting.constant';
import { MONTHS } from '../../../shared/constants/months';
import { BatchState, IBatchStatus } from '../models/batch-status';
import { IAppStore } from '../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as batchStatusActions from '../actions/batch-status.actions';
import { Observable } from 'rxjs/Observable';
import { UtilityFunctions } from '../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-generate-batch',
  templateUrl: './generate-batch.component.html',
  styleUrls: ['./generate-batch.component.scss']
})
export class GenerateBatchComponent implements OnInit {

  public months: Array<{ val: number, name: string, longName: string }>;
  public mm: number;
  public yy: number;
  public years: Array<number> = [];
  public reportMonth: { month: number, year: number };
  public dateString: any;

  public batch: IBatchStatus;
  public batchState$: Observable<BatchState>;

  private _selectedDate: Date;
  private _dateSub: Subscription;
  private _timer: Date;

  constructor(private _batchService: BatchService,
              private _store: Store<IAppStore>,
              private _toasterService: ToasterService,
              private _initializerService: InitializerService) {
    this.batchState$ = this._batchService.batchState$;
    this.months = MONTHS;
    this._dateSub = this._initializerService.currDate$.subscribe(val => {
      this._selectedDate = new Date(val);
      this._getMonth();
      this._getYear();
      this.dateString = this._getDateString();
    });
  }

  public onSubmitRetrieve(body: { month: number, year: number }, valid) {
    this.reportMonth['year'] = this.yy = body.year;
    this.reportMonth['month'] = this.mm = +body.month;
    this.dateString = this._getDateString();
    this._store.dispatch(new batchStatusActions.ResetBatchState());
    this._store.dispatch(new batchStatusActions.RetrieveDateRequest());
    this._checkBatchStatus();
  }

  private _generate(): void {
    this._timer = new Date();
    this._batchService.generateBatch(this.reportMonth)
      .subscribe(res => {
        console.log(res);
        this._store.dispatch(new batchStatusActions.BatchInProgress());
      }, err => {
        this._store.dispatch(new batchStatusActions.ResetBatchState());
        const toast: Toast = {
          type: 'error',
          timeout: 10000,
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
      });
  }

  public handleGenerate(): void {
    this._store.dispatch(new batchStatusActions.GenerateRequest());
    this._generate();
  }

  public handleReGenerate(): void {
    this._store.dispatch(new batchStatusActions.RegenRequest());
    this._generate();
  }

  public handleRefresh(): void {
    this._store.dispatch(new batchStatusActions.RefreshRequest());

    const clickTime: Date = new Date;
    const diffTime = this._timer.getTime() - clickTime.getTime();
    const secs = diffTime / 1000;
    const secsBetween = Math.abs(secs);

    // fake the refresh for 5secs
    if (secsBetween > 5) {
      this._checkBatchStatus();
    } else {
      setTimeout(() => {
        this._store.dispatch(new batchStatusActions.BatchInProgress());
      }, 1000);
    }

  }

  private _getDateString(): { val: number, name: string, longName?: string } {
    return MONTHS.find(month => {
      return month.val === this.mm;
    });
  }

  private _getMonth(): void {
    this.mm = +(this._selectedDate.getMonth() + 1).toString().slice(-2);
  }

  private _getYear(): void {
    this.yy = this._selectedDate.getFullYear();
    for (let i = INITIAL_YEAR; i <= this.yy; i++) {
      this.years.push(i);
    }
  }

  private _checkBatchStatus(): void {
    this._batchService.getBatchStatus(this.reportMonth.year, this.reportMonth.month)
      .subscribe((batch: IBatchStatus) => {
        console.log('BATCH INFO: ', batch);
        this.batch = batch;
        if (batch.processed && batch.complete) {
          this._store.dispatch(new batchStatusActions.BatchCompleteSuccess());
        } else if (batch.processed && !batch.complete) {
          this._store.dispatch(new batchStatusActions.BatchCompleteFail());
        } else if (!batch.processed) {
          this._store.dispatch(new batchStatusActions.BatchInProgress());
        }

      }, err => {
        if (err.status === 404) {
          this._store.dispatch(new batchStatusActions.BatchNotFound());
        } else {
          const toast: Toast = {
            type: 'error',
            title: `Error Code: ${err.status}, Message: ${err.statusText}`,
            body: UtilityFunctions.parseErrs(err._body, err.status),
            bodyOutputType: BodyOutputType.TrustedHtml
          };
          this._toasterService.pop(toast);
        }
      });
  }

  ngOnInit(): void {

    this._store.dispatch(new batchStatusActions.ResetBatchState());
    this._store.dispatch(new batchStatusActions.PageLoad());

    this._getMonth();
    this._getYear();
    this.reportMonth = {
      year: this.yy,
      month: this.mm
    };

    this._checkBatchStatus();
  }

}
