export interface IBatchStatus {
  site: number;
  month: number;
  year: number;
  csv: string;
  csv_name: string;
  status_text: string;
  complete: boolean;
  processed: boolean;
}

export class BatchState {
  mainBusy: boolean = false;
  noBatchFound: boolean = false;
  batchInProgress: boolean = false;
  batchProcessed: boolean = false;
  batchSuccess: boolean = false;
  batchFail: boolean = false;
  genRequest: boolean = false;
  refreshRequest: boolean = false;
  regenRequest: boolean = false;
  retrieveDateRequest: boolean = false;
  serverErr: boolean = false;
}
