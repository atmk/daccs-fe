import * as batchStatusActions from '../actions/batch-status.actions';
import { BatchState } from '../models/batch-status';

export type Action = batchStatusActions.All;

export function batchStateReducer(state: BatchState = new BatchState(), action: Action): BatchState {
  switch (action.type) {

    case batchStatusActions.RESET_STATE:
      return Object.assign({}, new BatchState());

    case batchStatusActions.PAGE_LOAD:
      return Object.assign({}, state, {mainBusy: true});

    case batchStatusActions.BATCH_NOT_FOUND:
      return Object.assign({}, new BatchState(), {noBatchFound: true});

    case batchStatusActions.GENERATE_REQUEST:
      return Object.assign({}, state, {genRequest: true});

    case batchStatusActions.BATCH_IN_PROGRESS:
      return Object.assign({}, new BatchState(), {batchInProgress: true});

    case batchStatusActions.REFRESH_REQUEST:
      return Object.assign({}, state, {refreshRequest: true});

    case batchStatusActions.BATCH_COMPLETE_FAIL:
      return Object.assign({}, new BatchState(), {batchProcessed: true}, {batchFail: true});

    case batchStatusActions.BATCH_COMPLETE_SUCCESS:
      return Object.assign({}, new BatchState(), {batchProcessed: true}, {batchSuccess: true});

    case batchStatusActions.REGEN_REQUEST:
      return Object.assign({}, state, {regenRequest: true});

    case batchStatusActions.RETRIEVE_DATE_REQUEST:
      return Object.assign({}, state, {retrieveDateRequest: true});

    default:
      return state;
  }

}
