import { Action } from '@ngrx/store';

export const RESET_STATE = 'RESET_STATE';
export const PAGE_LOAD = 'PAGE_LOAD';
export const BATCH_NOT_FOUND = 'BATCH_NOT_FOUND';
export const BATCH_IN_PROGRESS = 'BATCH_IN_PROGRESS';
export const BATCH_COMPLETE_SUCCESS = 'BATCH_COMPLETE_SUCCESS';
export const BATCH_COMPLETE_FAIL = 'BATCH_COMPLETE_FAIL';
export const GENERATE_REQUEST = 'GENERATE_REQUEST';
export const REFRESH_REQUEST = 'REFRESH_REQUEST';
export const REGEN_REQUEST = 'REGEN_REQUEST';
export const RETRIEVE_DATE_REQUEST = 'RETRIEVE_DATE_REQUEST';

export class ResetBatchState implements Action {
  readonly type = RESET_STATE;
}

export class PageLoad implements Action {
  readonly type = PAGE_LOAD;
}

export class BatchNotFound implements Action {
  readonly type = BATCH_NOT_FOUND;
}

export class BatchInProgress implements Action {
  readonly type = BATCH_IN_PROGRESS;
}

export class BatchCompleteSuccess implements Action {
  readonly type = BATCH_COMPLETE_SUCCESS;
}

export class BatchCompleteFail implements Action {
  readonly type = BATCH_COMPLETE_FAIL;
}

export class GenerateRequest implements Action {
  readonly type = GENERATE_REQUEST;
}

export class RefreshRequest implements Action {
  readonly type = REFRESH_REQUEST;
}

export class RegenRequest implements Action {
  readonly type = REGEN_REQUEST;
}

export class RetrieveDateRequest implements Action {
  readonly type = RETRIEVE_DATE_REQUEST;
}

export type All = ResetBatchState
  | PageLoad
  | BatchNotFound
  | BatchInProgress
  | BatchCompleteSuccess
  | BatchCompleteFail
  | GenerateRequest
  | RefreshRequest
  | RegenRequest
  | RetrieveDateRequest
