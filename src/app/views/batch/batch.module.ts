import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/modules/shared.module';
import { GenerateBatchComponent } from './generate-batch/generate-batch.component';
import { BatchService } from './services/batch.service';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from 'clarity-angular';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ClarityModule
  ],
  declarations: [GenerateBatchComponent],
  providers: [BatchService]
})
export class BatchModule {
}
