import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { AuthHttp } from 'angular2-jwt';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../shared/ngrx/appstore.ngrx.interface';
import { BatchState } from '../models/batch-status';

@Injectable()
export class BatchService {

  public batchState$: Observable<BatchState>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.batchState$ = this._store.select('batchStateReducer')
  }

  public getBatchStatus(year: number, month: number): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/ledgers/pastel-batch/retrieve/${year}/${month}/`)
      .map(res => res.json())
  }

  public generateBatch(dateObj: { month: number, year: number }) {
    return this._authHttp.post(`${this._apiBaseUrl}/ledgers/pastel-batch/generate/`, dateObj)
      .map(res => res.json())
  }

}
