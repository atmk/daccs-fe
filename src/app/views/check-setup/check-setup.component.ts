import { Component, OnDestroy, OnInit } from '@angular/core';
import { ISetupReq, InitializerService } from '../../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { getSetupRoute } from '../../shared/utils/get-setup-route';

@Component({
  selector: 'daccs-check-setup',
  templateUrl: './check-setup.component.html',
  styleUrls: ['./check-setup.component.scss']
})
export class CheckSetupComponent implements OnInit, OnDestroy {

  public setupRequired: ISetupReq;
  public firstSetupRoute = 'categories';

  private _setupReqSub: Subscription;

  constructor(private _router: Router,
              private _initializerService: InitializerService) {
    this._setupReqSub = this._initializerService.setupRequired$.subscribe((val: ISetupReq) => {
      if (val.required_items.length) {
        this.firstSetupRoute = getSetupRoute(val.required_items);
      }
      this.setupRequired = val
    });
  }

  ngOnInit(): void {

    if (!this.setupRequired.setup_required) {
      this._router.navigate(['/daily-recon/shifts']);
      this._initializerService.reloadSocket$.next(true);
    }
  }

  ngOnDestroy(): void {
    if (this._setupReqSub) {
      this._setupReqSub.unsubscribe();
    }
  }

}
