import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyLiterSalesComponent } from './daily-liter-sales.component';

describe('DailyLiterSalesComponent', () => {
  let component: DailyLiterSalesComponent;
  let fixture: ComponentFixture<DailyLiterSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyLiterSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyLiterSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
