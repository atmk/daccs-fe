import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { DashGraphService } from '../../services/dash-graph.service';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';
import { IChartData } from '../models/chart-data';
import { ACTIVE_BLUE_CHART } from '../../../../shared/constants/chart-colors';

@Component({
  selector: 'daccs-daily-liter-sales',
  templateUrl: './daily-liter-sales.component.html',
  styleUrls: ['./daily-liter-sales.component.scss']
})
export class DailyLiterSalesComponent implements OnInit {

  @Input() year: string;
  @Input() month: string;
  @Output() failed: EventEmitter<boolean> = new EventEmitter();

  @HostBinding('class') show = 'col-md-6';
  @HostBinding('class.hide-elem') hide = false;

  public options: any;
  public type = 'bar';
  public data: any;
  public chartTitle = 'Daily Liters';

  constructor(private _dashGraphService: DashGraphService) {
    this.options = {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            userCallback: (value) => {
              value = UtilityFunctions.rounder(value).toString();
              value = value.split(/(?=(?:...)*$)/);
              value = value.join(',');
              return value;
            }
          }
        }],
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        callbacks: {
          label: (tooltipItems, data) => {
            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel.toLocaleString();
          }
        }
      }
    };
  }

  ngOnInit(): void {
    this.hide = false;
    this._dashGraphService.getDailyLiterSalesData(this.year, this.month)
      .subscribe(data => {
        if (!data.chart) {
          this.failed.emit(true);
          return;
        }
        const chartData: IChartData = JSON.parse(data.chart);
        if (chartData.title) {
          this.chartTitle = chartData.title;
        }
        const colorArr = this._dashGraphService.colorCycle(chartData.datasets.length);
        chartData.datasets.map((set: any, i: number) => {
          console.log(colorArr);
          set['backgroundColor'] = ACTIVE_BLUE_CHART[1];
          set['borderColor'] = ACTIVE_BLUE_CHART[0];
          set['borderWidth'] = 1;
          return set;
        });
        this.data = chartData;
        this.failed.emit(false);
      }, () => {
        this.failed.emit(true);
        this.hide = true;
      });
  }

}
