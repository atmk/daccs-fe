import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyOpsComponent } from './monthly-ops.component';

describe('MonthlyOpsComponent', () => {
  let component: MonthlyOpsComponent;
  let fixture: ComponentFixture<MonthlyOpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyOpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyOpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
