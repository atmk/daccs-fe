export interface IChartData {
  datasets: Array<any>;
  labels: Array<any>;
  title: string;
}
