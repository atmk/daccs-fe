import { Component, EventEmitter, HostBinding, OnInit, Output } from '@angular/core';
import { DashGraphService } from '../../services/dash-graph.service';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';
import { IChartData } from '../models/chart-data';

@Component({
  selector: 'daccs-annual-liter-sales',
  templateUrl: './annual-liter-sales.component.html',
  styleUrls: ['./annual-liter-sales.component.scss']
})
export class AnnualLiterSalesComponent implements OnInit {

  @Output() failed: EventEmitter<boolean> = new EventEmitter();

  @HostBinding('class') show = 'col-md-6';
  @HostBinding('class.hide-elem') hide = false;

  public options: any;
  public type = 'line';
  public data: any;
  public chartTitle = 'Annual Litre Sales';

  constructor(private _dashGraphService: DashGraphService) {

    this.options = {
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            userCallback: (value) => {
              value = UtilityFunctions.rounder(value).toString();
              value = value.split(/(?=(?:...)*$)/);
              value = value.join(',');
              return value;
            }
          }
        }],
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      },
      tooltips: {
        callbacks: {
          label: (tooltipItems, data) => {
            return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel.toLocaleString();
          }
        }
      }
    };

  }

  ngOnInit(): void {
    this.hide = false;
    this._dashGraphService.getAnnualLiterSalesData()
      .subscribe(data => {
        if (!data.chart) {
          this.failed.emit(true);
          return;
        }
        const chartData: IChartData = JSON.parse(data.chart);
        const colorArr = this._dashGraphService.colorCycle(chartData.datasets.length);
        if (chartData.title) {
          this.chartTitle = chartData.title;
        }
        chartData.datasets.map((set: any, i: number) => {
          set['borderColor'] = colorArr[i][0];
          set['borderWidth'] = 1;
          set['fill'] = false;
          return set;
        });
        this.data = chartData;
        this.failed.emit(false);
      }, () => {
        this.hide = true;
        this.failed.emit(true);
      });
  }

}
