import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualLiterSalesComponent } from './annual-liter-sales.component';

describe('AnnualLiterSalesComponent', () => {
  let component: AnnualLiterSalesComponent;
  let fixture: ComponentFixture<AnnualLiterSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualLiterSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualLiterSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
