export const DATA1 = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  datasets: [
    {
      label: 'Sales',
      data: [371486, 399272, 502594, 840754, 378068, 752501, 348225, 272910, 196456, 585905, 979343, 344862],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1
    },
    {
      label: 'Deposits',
      data: [21581, 17562, 14849, 20190, 31253, 40225, 34900, 20688, 26824, 35230, 14332, 49382],
      backgroundColor: '#ebafa6',
      borderColor: '#c92100',
      borderWidth: 1
    },
    {
      label: 'Other Movements',
      data: [128499, 50594, 95219, 64335, 100906, 103656, 187100, 187629, 68782, 133659, 138241, 193150],
      backgroundColor: '#c6e4ab',
      borderColor: '#60b515',
      borderWidth: 1
    }
  ]
};

export const DATA2 = {
  labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
    '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
  datasets: [
    {
      label: 'Wet Stock',
      data: [
        8830,
        31648,
        14921,
        31990,
        4155,
        4391,
        3457,
        8053,
        24845,
        33961,
        4529,
        7137,
        34743,
        29870,
        9904,
        682,
        32391,
        11614,
        10941,
        23036,
        8983,
        9259,
        10764,
        20447,
        11871,
        23929,
        21217,
        33143,
        2967,
        28962,
        25304
      ],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1
    },
    {
      label: 'Dry Stock',
      data: [
        9952,
        7462,
        887,
        4408,
        22292,
        15841,
        14201,
        34416,
        28443,
        29907,
        32299,
        33860,
        12754,
        9749,
        27370,
        12224,
        28428,
        20752,
        9215,
        27172,
        16580,
        29289,
        17433,
        8834,
        9121,
        22555,
        34234,
        26044,
        3456,
        21776,
        15528
      ],
      backgroundColor: 'rgba(255, 159, 64, 0.2)',
      borderColor: 'rgba(255, 159, 64, 1)',
      borderWidth: 1
    },
    {
      label: 'Express Store',
      data: [
        28588,
        13580,
        31998,
        6184,
        1499,
        3376,
        31979,
        23455,
        14646,
        8399,
        29089,
        10737,
        31164,
        22447,
        8289,
        5280,
        10087,
        23405,
        11210,
        33338,
        32834,
        13584,
        29591,
        7117,
        32725,
        5857,
        27446,
        3897,
        10716,
        26998,
        31173
      ],
      backgroundColor: '#b68cd1',
      borderColor: '#853fb3',
      borderWidth: 1
    }
  ]
};

export const DATA3 = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  datasets: [{
    label: 'Sales',
    data: [432, 457, 153, 355, 252, 263, 449, 363, 304, 133, 205, 305],
    backgroundColor: 'rgba(255, 99, 132, 0.2)',
    borderColor: 'rgba(255,99,132,1)',
    borderWidth: 1
  },
    {
      label: 'Deposits',
      data: [152, 460, 475, 398, 466, 412, 338, 241, 243, 167, 107, 202],
      backgroundColor: 'rgba(255, 159, 64, 0.2)',
      borderColor: 'rgba(255, 159, 64, 1)',
      borderWidth: 1
    },
    {
      label: 'Other Movements',
      data: [268, 194, 249, 453, 457, 491, 340, 111, 371, 156, 492, 301],
      backgroundColor: '#b68cd1',
      borderColor: '#853fb3',
      borderWidth: 1
    }
  ]
};

export const DATA4 = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  datasets: [{
    label: '2013',
    data: [371486, 399272, 502594, 840754, 378068, 752501, 348225, 272910, 196456, 585905, 979343, 344862],
    borderColor: 'rgba(255,99,132,1)',
    fill: false,
    borderWidth: 1
  },
    {
      label: '2014',
      data: [17229, 46233, 58395, 35282, 21468, 49044, 46997, 17318, 34589, 21769, 32546, 40782],
      borderColor: 'rgba(255, 159, 64, 1)',
      fill: false,
      borderWidth: 1
    },
    {
      label: '2015',
      data: [239283, 123279, 269996, 219585, 203354, 241368, 262994, 106818, 114214, 148163, 144095, 209140],
      borderColor: '#853fb3',
      fill: false,
      borderWidth: 1
    },
    {
      label: '2016',
      data: [21581, 17562, 14849, 20190, 31253, 40225, 34900, 20688, 26824, 35230, 14332, 49382],
      borderColor: '#c92100',
      fill: false,
      borderWidth: 1
    },
    {
      label: '2017',
      data: [128499, 50594, 95219, 64335, 100906, 103656, 187100, 187629, 68782, 133659, 138241, 193150],
      borderColor: '#60b515',
      fill: false,
      borderWidth: 1
    }
  ]
};
