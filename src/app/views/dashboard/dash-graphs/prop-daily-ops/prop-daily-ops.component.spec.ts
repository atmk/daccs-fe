import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropDailyOpsComponent } from './prop-daily-ops.component';

describe('PropDailyOpsComponent', () => {
  let component: PropDailyOpsComponent;
  let fixture: ComponentFixture<PropDailyOpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropDailyOpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropDailyOpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
