import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlySalesByCatComponent } from './monthly-sales-by-cat.component';

describe('MonthlySalesByCatComponent', () => {
  let component: MonthlySalesByCatComponent;
  let fixture: ComponentFixture<MonthlySalesByCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlySalesByCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlySalesByCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
