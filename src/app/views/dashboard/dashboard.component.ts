import { Component, OnDestroy, OnInit } from '@angular/core';
import { DashGraphService } from './services/dash-graph.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { UtilityFunctions } from '../../shared/utils/utility-functions';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { InitializerService } from '../../initialiaze/services/initializer.service';
import { Subscription } from 'rxjs/Subscription';

interface INotice {
  id: number;
  title: string;
  notice: string;
  date: string;
}

@Component({
  selector: 'daccs-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('flyInOut', [
      state('in', style({opacity: 1, transform: 'translateX(0)'})),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit, OnDestroy {

  public year: number;
  public month: number;
  public numFails = 0;
  public numLoaded = 0;
  public loadingNotices = false;
  public notices: Array<INotice>;
  public activeNotices: Array<INotice>;
  public hiddenNoticeIds: Array<number> = [];

  private _dateSub: Subscription;

  constructor(private _dashGraphService: DashGraphService,
              private _initializerService: InitializerService,
              private _toasterService: ToasterService) {
    this._dateSub = this._initializerService.currDate$.subscribe((val: string) => {
      const currDate: Date = new Date(val);
      this.year = currDate.getFullYear();
      this.month = (currDate.getMonth() + 1);
    });
  }

  public handleFails(failed: number): void {
    if (failed) {
      ++this.numFails;
    } else {
      ++this.numLoaded;
    }
  }

  public dismissNotice(noticeId): void {
    this.hiddenNoticeIds.push(noticeId);
    localStorage.setItem('dismissedNotices', JSON.stringify(this.hiddenNoticeIds));
    this.activeNotices = this._getActiveNotices(this.notices);
  }

  public returnDismissedNotices(): void {
    this.hiddenNoticeIds = [];
    this.activeNotices = this.notices;
    localStorage.removeItem('dismissedNotices');
  }

  private _getHiddenNotices(): Array<number> {
    return JSON.parse(localStorage.getItem('dismissedNotices'));
  }

  private _getActiveNotices(notices: Array<INotice>): Array<INotice> {
    if (this.hiddenNoticeIds.length) {
      return notices.filter((notice: INotice) => {
        return this.hiddenNoticeIds.indexOf(notice.id) === -1;
      });
    } else {
      return notices;
    }
  }

  ngOnInit(): void {
    this.hiddenNoticeIds = this._getHiddenNotices() || [];
    this.loadingNotices = true;
    this._dashGraphService.getNotices()
      .subscribe((notices: Array<INotice>) => {
        this.notices = notices;
        this.activeNotices = this._getActiveNotices(notices);
      }, (err) => {
        const toast: Toast = {
          type: 'error',
          title: `Error Loading Notices: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
      });
  }

  ngOnDestroy(): void {
    this._dateSub.unsubscribe();
  }

}
