import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ClarityModule } from 'clarity-angular';
import { ChartModule } from 'angular2-chartjs';
import { PropDailyOpsComponent } from './dash-graphs/prop-daily-ops/prop-daily-ops.component';
import { MonthlyOpsComponent } from './dash-graphs/monthly-ops/monthly-ops.component';
import { MonthlySalesByCatComponent } from './dash-graphs/monthly-sales-by-cat/monthly-sales-by-cat.component';
import { AnnualLiterSalesComponent } from './dash-graphs/annual-liter-sales/annual-liter-sales.component';
import { DashGraphService } from './services/dash-graph.service';
import { DailyLiterSalesComponent } from './dash-graphs/daily-liter-sales/daily-liter-sales.component';

@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    ClarityModule
  ],
  declarations: [DashboardComponent,
    PropDailyOpsComponent,
    MonthlyOpsComponent,
    MonthlySalesByCatComponent,
    AnnualLiterSalesComponent,
    DailyLiterSalesComponent],
  providers: [DashGraphService]
})
export class DashboardModule {
}
