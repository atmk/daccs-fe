import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { AuthHttp } from 'angular2-jwt';
import { CHART_COLORS } from '../../../shared/constants/chart-colors';

@Injectable()
export class DashGraphService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public getNotices(): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/notices/`)
      .map(res => res.json());
  }

  public getPropDailyOpsData(year: string, month: string): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/charts/daily-operations/${year}/${month}/`)
      .map(res => res.json());
  }

  public getMonthlyOpsData(year: string): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/charts/monthly-operations/${year}/`)
      .map(res => res.json());
  }

  public getMonthlySalesByCatData(year: string): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/charts/monthly-sales/${year}/`)
      .map(res => res.json());
  }

  public getAnnualLiterSalesData(): Observable<any> {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/charts/annual-liters/`)
      .map(res => res.json());
  }

  public getDailyLiterSalesData(year: string, month: string) {
    return this._authHttp.get(`${this._apiBaseUrl}/reports/charts/daily-liters/${year}/${month}/`)
      .map(res => res.json());
  }

  public colorCycle(numOfColors: number) {
    const colorCycle: Array<Array<string>> = CHART_COLORS;

    return Array.from(Array(numOfColors), (_, x) => {
      const pointer = x % colorCycle.length;
      return colorCycle[pointer];
    });

  }

}
