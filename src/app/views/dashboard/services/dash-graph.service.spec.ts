import { TestBed, inject } from '@angular/core/testing';

import { DashGraphService } from './dash-graph.service';

describe('DashGraphService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashGraphService]
    });
  });

  it('should be created', inject([DashGraphService], (service: DashGraphService) => {
    expect(service).toBeTruthy();
  }));
});
