import { Component, OnInit, ViewChild } from '@angular/core';
import { Wizard } from 'clarity-angular';
import { ISetupReq, InitializerService } from '../../../../initialiaze/services/initializer.service';
import 'rxjs/add/operator/take'
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'daccs-cat-setup',
  templateUrl: './cat-setup.component.html',
  styleUrls: ['./cat-setup.component.scss']
})
export class CatSetupComponent implements OnInit {

  @ViewChild('wizard') wizard: Wizard;

  public openWizard = false;
  public setupRequired$: Observable<ISetupReq>;

  constructor(private _initializerService: InitializerService, private _router: Router) {
    this.setupRequired$ = this._initializerService.setupRequired$;
  }

  public doFinish(): void {
    this.doReset();
    console.log('finished');
    this._initializerService.setupRequired$.take(1).subscribe((val: ISetupReq) => {
      if (val.setup_required) {
        this._router.navigate(['/daily-recon/shifts'])
      }
    });
  }

  open() {
    this.openWizard = !this.openWizard;
  }

  public doReset(): void {
    this.wizard.reset();

  }

  public ngOnInit() {
    console.log();
  }
}
