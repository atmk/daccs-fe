import { Action } from '@ngrx/store';
import { IEODCat } from '../../../../daily-recon/eod-recon/models/eod';

export const LOAD_EOD_CATS = 'LOAD_EOD_CATS';
export const UPDATE_EOD_CAT = 'UPDATE_EOD_CAT';

export class LoadEODCats implements Action {
  readonly type = LOAD_EOD_CATS;

  constructor(public payload: Array<IEODCat>) {
  }
}

export class UpdateEODCats implements Action {
  readonly type = UPDATE_EOD_CAT;

  constructor(public payload: IEODCat) {
  }
}

export type All = LoadEODCats | UpdateEODCats
