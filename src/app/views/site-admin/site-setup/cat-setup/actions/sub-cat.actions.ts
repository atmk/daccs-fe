import { Action } from '@ngrx/store';
import { ISubCat } from '../../../../daily-recon/eod-recon/models/eod';

export const LOAD_SUB_CATS = 'LOAD_SUB_CATS';
export const UPDATE_SUB_CAT = 'UPDATE_SUB_CAT';

export class LoadSubCats implements Action {
  readonly type = LOAD_SUB_CATS;

  constructor(public payload: Array<ISubCat>) {
  }
}

export class UpdateSubCats implements Action {
  readonly type = UPDATE_SUB_CAT;

  constructor(public payload: ISubCat) {
  }
}

export type All = LoadSubCats | UpdateSubCats
