import { Action } from '@ngrx/store';
import { IMainCat } from '../../../../daily-recon/eod-recon/models/eod';

export const LOAD_MAIN_CATS = 'LOAD_MAIN_CATS';
export const UPDATE_MAIN_CAT = 'UPDATE_MAIN_CAT';

export class LoadMainCats implements Action {
  readonly type = LOAD_MAIN_CATS;

  constructor(public payload: Array<IMainCat>) {
  }
}

export class UpdateMainCats implements Action {
  readonly type = UPDATE_MAIN_CAT;

  constructor(public payload: IMainCat) {
  }
}

export type All = LoadMainCats | UpdateMainCats
