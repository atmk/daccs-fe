import { Injectable } from '@angular/core';
import { ICat, IEODCat, IMainCat, ISubCat } from '../../../../daily-recon/eod-recon/models/eod';
import { Observable } from 'rxjs/Observable';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../../environments/environment';
import 'rxjs/add/observable/forkJoin';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';

export interface IBuiltCat {
  parent: string;
  data: Array<IEODCat | ISubCat>;
}

@Injectable()
export class CatsService {

  public mainCats$: Observable<Array<IMainCat>>;
  public eodCats$: Observable<Array<IEODCat>>;
  public subCats$: Observable<Array<ISubCat>>;
  public busyToggle$: Subject<boolean> = new Subject();

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.mainCats$ = this._store.select('mainCatReducer');
    this.eodCats$ = this._store.select('eodCatReducer');
    this.subCats$ = this._store.select('subCatReducer');
  }

  public rebuildAvailable<T extends ICat>(avail: Array<T>, active: Array<T>): Array<T> {
    const availArr = avail.map((availItm: T) => {
      return Object.assign({}, availItm, {active: false});
    });

    const activeArr = active.map((actItm: T) => {
      return Object.assign({}, actItm, {active: true});
    });

    return availArr.concat(activeArr);
  }

  public sortCatsByParent<T>(arr: Array<T>, sortByString: string): Array<IBuiltCat> {
    const catObject = arr.reduce((obj: any, itm: T) => {
      if (!obj[itm[sortByString]]) {
        obj[itm[sortByString]] = [];
      }
      obj[itm[sortByString]].push(itm);
      return obj;
    }, {});
    return Object.keys(catObject).map((key) => {
      return {parent: key, data: catObject[key]};
    });
  }

  // SERVICE FOR ALL CATS *******
  public getAllCats<T>(catUrl: string): Observable<[Array<T>, Array<T>]> {
    const availCats = this._authHttp.get(`${this._apiBaseUrl}/recons/categories/available/${catUrl}/`)
      .map(res => res.json());

    const activeCats = this._authHttp.get(`${this._apiBaseUrl}/recons/categories/site/${catUrl}/`)
      .map(res => res.json());

    return Observable.forkJoin(availCats, activeCats);
  }

  public addCatToActive<T>(_id_: number, catUrl: string): Observable<T> {
    return this._authHttp.post(`${this._apiBaseUrl}/recons/categories/site/${catUrl}/${_id_}/`, {})
      .map(res => res.json());
  }

  public removeCatFromActive<T>(_id_, catUrl: string): Observable<T> {
    return this._authHttp.delete(`${this._apiBaseUrl}/recons/categories/site/${catUrl}/${_id_}/`)
      .map(res => res.json());
  }
}
