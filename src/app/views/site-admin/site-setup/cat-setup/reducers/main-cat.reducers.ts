import * as mainCatActions from '../actions/main-cat.actions';
import { IMainCat } from '../../../../daily-recon/eod-recon/models/eod';

export type Action = mainCatActions.All;

export function mainCatReducer(state: Array<IMainCat>, action: Action): Array<IMainCat> {
  switch (action.type) {
    case mainCatActions.LOAD_MAIN_CATS:
      return action.payload;
    case mainCatActions.UPDATE_MAIN_CAT:
      return state.map((mainCat: IMainCat) => {
        return mainCat._id_ === action.payload._id_
          ? Object.assign({}, mainCat, action.payload)
          : mainCat;
      });
    default:
      return state;
  }
}
