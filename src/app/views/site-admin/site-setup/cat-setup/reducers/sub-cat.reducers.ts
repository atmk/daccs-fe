import * as subCatActions from '../actions/sub-cat.actions';
import { ISubCat } from '../../../../daily-recon/eod-recon/models/eod';

export type Action = subCatActions.All;

export function subCatReducer(state: Array<ISubCat>, action: Action): Array<ISubCat> {
  switch (action.type) {
    case subCatActions.LOAD_SUB_CATS:
      return action.payload;
    case subCatActions.UPDATE_SUB_CAT:
      return state.map((subCat: ISubCat) => {
        return subCat._id_ === action.payload._id_
          ? Object.assign({}, subCat, action.payload)
          : subCat;
      });
    default:
      return state;
  }
}
