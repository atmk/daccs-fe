import * as eodCatActions from '../actions/eod-cat.actions';
import { IEODCat } from '../../../../daily-recon/eod-recon/models/eod';

export type Action = eodCatActions.All;

export function eodCatReducer(state: Array<IEODCat>, action: Action): Array<IEODCat> {
  switch (action.type) {
    case eodCatActions.LOAD_EOD_CATS:
      return action.payload;
    case eodCatActions.UPDATE_EOD_CAT:
      return state.map((eodCat: IEODCat) => {
        return eodCat._id_ === action.payload._id_
          ? Object.assign({}, eodCat, action.payload)
          : eodCat;
      });
    default:
      return state;
  }
}
