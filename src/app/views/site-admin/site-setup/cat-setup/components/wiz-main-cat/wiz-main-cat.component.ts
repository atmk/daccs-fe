import { Component, OnInit } from '@angular/core';
import { CatsService } from '../../services/cats.service';
import { IMainCat } from '../../../../../daily-recon/eod-recon/models/eod';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as mainCatActions from '../../actions/main-cat.actions';

@Component({
  selector: 'daccs-wiz-main-cat',
  templateUrl: './wiz-main-cat.component.html',
  styleUrls: ['./wiz-main-cat.component.scss']
})
export class WizMainCatComponent implements OnInit {

  public activatedMainCats: Array<IMainCat>;
  public availMainCats: Array<IMainCat>;
  public sortedMainCats: Array<IMainCat>;

  public mainCats$: Observable<Array<IMainCat>>;

  constructor(private _catsService: CatsService,
              private _store: Store<IAppStore>) {
    this.mainCats$ = this._catsService.mainCats$;
  }

  ngOnInit() {
    this._catsService.getAllCats<IMainCat>('main-category')
      .subscribe((data: [Array<IMainCat>, Array<IMainCat>]) => {
        console.log('MAIN ARRAYS', data);
        this.activatedMainCats = data[1];
        this.availMainCats = data[0];
        this.sortedMainCats = this._catsService.rebuildAvailable<IMainCat>(this.availMainCats, this.activatedMainCats);

        this._store.dispatch(new mainCatActions.LoadMainCats(this.sortedMainCats));

      }, err => {
        console.log(err);
      });
  }

}
