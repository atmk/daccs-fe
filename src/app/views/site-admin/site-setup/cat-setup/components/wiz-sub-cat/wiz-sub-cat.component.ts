import { Component, OnDestroy, OnInit } from '@angular/core';
import { ISubCat } from '../../../../../daily-recon/eod-recon/models/eod';
import { CatsService, IBuiltCat } from '../../services/cats.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import * as subCatActions from '../../actions/sub-cat.actions';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'daccs-wiz-sub-cat',
  templateUrl: './wiz-sub-cat.component.html',
  styleUrls: ['./wiz-sub-cat.component.scss']
})
export class WizSubCatComponent implements OnInit, OnDestroy {

  public activatedSubCats: Array<ISubCat>;
  public availSubCats: Array<ISubCat>;
  public sortedSubCats: Array<ISubCat>;
  public subCatsSorted: Array<IBuiltCat>;

  private _subCatSub: Subscription;

  constructor(private _catsService: CatsService,
              private _store: Store<IAppStore>) {
    this._subCatSub = this._catsService.subCats$.subscribe((val: Array<ISubCat>) => {
      if (val) {
        this.subCatsSorted = this._catsService.sortCatsByParent(val, 'category_title');
      }
    })
  }

  ngOnInit(): void {
    this._catsService.getAllCats<ISubCat>('sub-category')
      .subscribe((data: [Array<ISubCat>, Array<ISubCat>]) => {
        console.log('SUB ARRAYS', data);
        this.activatedSubCats = data[1];
        this.availSubCats = data[0];
        this.sortedSubCats = this._catsService.rebuildAvailable<ISubCat>(this.availSubCats, this.activatedSubCats);

        this._store.dispatch(new subCatActions.LoadSubCats(this.sortedSubCats));

      }, err => {
        console.log(err);
      });
  }

  ngOnDestroy(): void {
    if (this._subCatSub) {
      this._subCatSub.unsubscribe();
    }
  }

}
