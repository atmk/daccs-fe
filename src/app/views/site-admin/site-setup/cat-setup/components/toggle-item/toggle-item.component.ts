import { Component, Input, OnInit } from '@angular/core';
import { IEODCat, IMainCat, ISubCat } from '../../../../../daily-recon/eod-recon/models/eod';
import { CatsService } from '../../services/cats.service';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as mainCatActions from '../../actions/main-cat.actions';
import * as eodCatActions from '../../actions/eod-cat.actions';
import * as subCatActions from '../../actions/sub-cat.actions';
import { Observable } from 'rxjs/Observable';
import { ToasterService } from 'angular2-toaster';

export type Cat = IMainCat & IEODCat & ISubCat

@Component({
  selector: 'daccs-toggle-item',
  templateUrl: './toggle-item.component.html',
  styleUrls: ['./toggle-item.component.scss']
})
export class WizMainItemComponent implements OnInit {

  @Input() public catData: Cat;
  @Input() public catUrl: string;
  @Input() public idx: number;

  public active: boolean;
  public busy$: Observable<boolean>;

  constructor(private _catsService: CatsService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {
    this.busy$ = this._catsService.busyToggle$;
  }

  public toggle(ev: boolean): void {
    console.log('event', ev, this.catData);

    this._catsService.busyToggle$.next(true);
    if (ev) {
      this._catsService.addCatToActive<Cat>(this.catData._id_, this.catUrl)
        .subscribe(() => {
          this.catData.active = ev;
          if (this.catUrl === 'main-category') {
            this._store.dispatch(new mainCatActions.UpdateMainCats(this.catData));
          } else if (this.catUrl === 'category') {
            this._store.dispatch(new eodCatActions.UpdateEODCats(this.catData));
          } else if (this.catUrl === 'sub-category') {
            this._store.dispatch(new subCatActions.UpdateSubCats(this.catData));
          }
          this._catsService.busyToggle$.next(false);
        }, err => {
          this.active = !ev;
          this._catsService.busyToggle$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
        })
    } else {
      this._catsService.removeCatFromActive<null>(this.catData._id_, this.catUrl)
        .subscribe(() => {
          this.catData.active = ev;
          if (this.catUrl === 'main-category') {
            this._store.dispatch(new mainCatActions.UpdateMainCats(this.catData));
          } else if (this.catUrl === 'category') {
            this._store.dispatch(new eodCatActions.UpdateEODCats(this.catData));
          } else if (this.catUrl === 'sub-category') {
            this._store.dispatch(new subCatActions.UpdateSubCats(this.catData));
          }
          this._catsService.busyToggle$.next(false);
        }, err => {
          this.active = !ev;
          this._catsService.busyToggle$.next(false);
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
        })
    }
  }

  ngOnInit() {
    this.active = this.catData.active;
  }

}
