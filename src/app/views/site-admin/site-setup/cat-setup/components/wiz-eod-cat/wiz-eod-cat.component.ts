import { Component, OnDestroy, OnInit } from '@angular/core';
import { IEODCat } from '../../../../../daily-recon/eod-recon/models/eod';
import { CatsService, IBuiltCat } from '../../services/cats.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import * as eodCatActions from '../../actions/eod-cat.actions';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'daccs-wiz-eod-cat',
  templateUrl: './wiz-eod-cat.component.html',
  styleUrls: ['./wiz-eod-cat.component.scss']
})
export class WizEodCatComponent implements OnInit, OnDestroy {

  public activatedEODCats: Array<IEODCat>;
  public availEODCats: Array<IEODCat>;
  public sortedEODCats: Array<IEODCat>;
  public eodCatsSorted: Array<IBuiltCat>;

  private _eodCatSub: Subscription;

  constructor(private _catsService: CatsService,
              private _store: Store<IAppStore>) {
    this._eodCatSub = this._catsService.eodCats$.subscribe((val: Array<IEODCat>) => {
      if (val) {
        this.eodCatsSorted = this._catsService.sortCatsByParent(val, 'main_category_title');
      }
    })
  }

  ngOnInit(): void {
    this._catsService.getAllCats<IEODCat>('category')
      .subscribe((data: [Array<IEODCat>, Array<IEODCat>]) => {
        console.log('EOD ARRAYS', data);
        this.activatedEODCats = data[1];
        this.availEODCats = data[0];
        this.sortedEODCats = this._catsService.rebuildAvailable<IEODCat>(this.availEODCats, this.activatedEODCats);

        this._store.dispatch(new eodCatActions.LoadEODCats(this.sortedEODCats));

      }, err => {
        console.log(err);
      });
  }

  ngOnDestroy(): void {
    if (this._eodCatSub) {
      this._eodCatSub.unsubscribe();
    }
  }

}
