import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ISpeedpointTermsConfig } from '../../../../initialiaze/models/site-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SpeedpointService } from './services/speedpoint.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import * as speedpointsSetupActions from './actions/speedpoints-setup.actions';

@Component({
  selector: 'daccs-speedpoint-setup',
  templateUrl: './speedpoint-setup.component.html',
  styleUrls: ['../shared/site-setup.scss']
})
export class SpeedpointSetupComponent implements OnInit {

  public speedpoints$: Observable<Array<ISpeedpointTermsConfig>>;
  public speedpointAddForm: FormGroup;
  public busy = false;
  public loading = false;

  constructor(private _suppliersService: SpeedpointService,
              private _speedpointService: SpeedpointService,
              private _fb: FormBuilder,
              private _store: Store<IAppStore>) {
    this.speedpoints$ = this._suppliersService.speedpoints$
  }

  public onAddSupplier(body: ISpeedpointTermsConfig): void {
    this.busy = true;
    this._speedpointService.addSpeedpoint(body)
      .subscribe((res: ISpeedpointTermsConfig) => {
        this.busy = false;

        this._store.dispatch(new speedpointsSetupActions.AddSpeedpointConfig(res));

        this.speedpointAddForm.setValue({
          terminal_number: '',
          description: '',
        });
        this.speedpointAddForm.markAsPristine();

      }, err => {
        this.busy = false;
        console.log(err);
      });
  }

  ngOnInit(): void {

    this.loading = true;
    this._suppliersService.loadSpeedpoints()
      .subscribe((data: Array<ISpeedpointTermsConfig>) => {
        this.loading = false;
        this._store.dispatch(new speedpointsSetupActions.LoadSpeedpointsConfig(data));
      }, err => {
        this.loading = false;
        console.log(err);
      });

    this.speedpointAddForm = this._fb.group({
      terminal_number: '',
      description: '',
    })
  }
}
