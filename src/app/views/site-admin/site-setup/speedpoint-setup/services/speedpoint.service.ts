import { Injectable } from '@angular/core';
import { environment } from '../../../../../../environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { ISpeedpointTermsConfig } from '../../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';

@Injectable()
export class SpeedpointService {

  public speedpoints$: Observable<Array<ISpeedpointTermsConfig>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.speedpoints$ = _store.select('speedpointsSetupReducer');
  }

  public loadSpeedpoints(): Observable<Array<ISpeedpointTermsConfig>> {
    return this._authHttp.get(`${this._apiBaseUrl}/recons/speedpoint-terminal/`)
      .map(res => res.json())
  }

  public addSpeedpoint(body: ISpeedpointTermsConfig): Observable<ISpeedpointTermsConfig> {
    return this._authHttp.post(`${this._apiBaseUrl}/recons/speedpoint-terminal/create/`, JSON.stringify(body))
      .map(res => res.json())
  }

  public removeSpeedpoint(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/recons/speedpoint-terminal/${_id_}/`)
      .map(res => res.json())
  }

  public updateSpeedpoint(body: ISpeedpointTermsConfig, _id_: number): Observable<ISpeedpointTermsConfig> {
    return this._authHttp.patch(`${this._apiBaseUrl}/recons/speedpoint-terminal/${_id_}/`, JSON.stringify(body))
      .map(res => res.json())
  }
}
