import { ISpeedpointTermsConfig } from '../../../../../initialiaze/models/site-config';
import { Action } from '@ngrx/store';

export const LOAD_SPEEDPOINTS = 'LOAD_SPEEDPOINTS';
export const ADD_SPEEDPOINT = 'ADD_SPEEDPOINT';
export const REMOVE_SPEEDPOINT = 'REMOVE_SPEEDPOINT';
export const UPDATE_SPEEDPOINT = 'UPDATE_SPEEDPOINT';

export class LoadSpeedpointsConfig implements Action {
  readonly type = LOAD_SPEEDPOINTS;

  constructor(public payload: Array<ISpeedpointTermsConfig>) {
  }
}

export class AddSpeedpointConfig implements Action {
  readonly type = ADD_SPEEDPOINT;

  constructor(public payload: ISpeedpointTermsConfig) {
  }
}

export class RemoveSpeedpointConfig implements Action {
  readonly type = REMOVE_SPEEDPOINT;

  constructor(public payload: number) {
  }
}

export class UpdateSpeedpointConfig implements Action {
  readonly type = UPDATE_SPEEDPOINT;

  constructor(public payload: ISpeedpointTermsConfig) {
  }
}

export type All = LoadSpeedpointsConfig
  | AddSpeedpointConfig
  | RemoveSpeedpointConfig
  | UpdateSpeedpointConfig
