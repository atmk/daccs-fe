import { Component, Input, OnChanges } from '@angular/core';
import { ISpeedpointTermsConfig } from '../../../../../initialiaze/models/site-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SpeedpointService } from '../services/speedpoint.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import * as speedpointsSetupActions from '../actions/speedpoints-setup.actions';

@Component({
  selector: 'daccs-speedpoint-item',
  templateUrl: './speedpoint-item.component.html',
  styleUrls: ['../../shared/site-setup.scss']
})
export class SpeedpointItemComponent implements OnChanges {

  @Input() speedpoint: ISpeedpointTermsConfig;

  public speedpointUpdateForm: FormGroup;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _fb: FormBuilder,
              private _speedpointService: SpeedpointService,
              private _store: Store<IAppStore>) {
    this.speedpointUpdateForm = this._fb.group({
      _id_: '',
      terminal_number: '',
      description: '',
    })
  }

  public onUpdateSupplier(body: ISpeedpointTermsConfig) {
    this.busy = true;
    this._speedpointService.updateSpeedpoint(body, body._id_)
      .subscribe((res: ISpeedpointTermsConfig) => {
        this._store.dispatch(new speedpointsSetupActions.UpdateSpeedpointConfig(res));
        this.busy = false;
      }, err => {
        console.log(err);
        this.busy = false;
      });
  }

  public onRemoveSupplier() {
    this._speedpointService.removeSpeedpoint(this.speedpoint._id_)
      .subscribe(() => {
        this._store.dispatch(new speedpointsSetupActions.RemoveSpeedpointConfig(this.speedpoint._id_));
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnChanges(): void {
    if (this.speedpoint && this.speedpointUpdateForm) {
      this.speedpointUpdateForm.setValue({
        _id_: this.speedpoint._id_,
        terminal_number: this.speedpoint.terminal_number,
        description: this.speedpoint.description,
      });
    }
  }
}
