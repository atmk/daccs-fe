import * as speedpointsSetupActions from '../actions/speedpoints-setup.actions';
import { ISpeedpointTermsConfig } from '../../../../../initialiaze/models/site-config';

export type Action = speedpointsSetupActions.All;

export function speedpointsSetupReducer(state: Array<any> = [], action: Action): Array<any> {
  switch (action.type) {

    case speedpointsSetupActions.LOAD_SPEEDPOINTS:
      return action.payload;

    case speedpointsSetupActions.ADD_SPEEDPOINT:
      return [
        ...state,
        action.payload
      ];

    case speedpointsSetupActions.REMOVE_SPEEDPOINT:
      return state.filter((spoint: ISpeedpointTermsConfig) => {
        return spoint._id_ !== action.payload;
      });

    case speedpointsSetupActions.UPDATE_SPEEDPOINT:
      return state.map((spoint: ISpeedpointTermsConfig) => {
        return spoint._id_ === action.payload._id_
          ? Object.assign({}, spoint, action.payload)
          : spoint;
      });

    default:
      return state;

  }
}
