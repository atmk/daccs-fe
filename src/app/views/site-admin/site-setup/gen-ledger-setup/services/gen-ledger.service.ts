import { Injectable } from '@angular/core';
import { IGenLedgerConfig } from '../../../../../initialiaze/models/site-config';
import { environment } from '../../../../../../environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';

@Injectable()
export class GenLedgerService {

  public genLedgers$: Observable<Array<IGenLedgerConfig>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.genLedgers$ = _store.select('genLedgersSetupReducer');
  }

  public loadGenLedgers(): Observable<Array<IGenLedgerConfig>> {
    return this._authHttp.get(`${this._apiBaseUrl}/ledgers/general-ledger/`)
      .map(res => res.json())
  }

  public addGenLedger(body: IGenLedgerConfig): Observable<IGenLedgerConfig> {
    return this._authHttp.post(`${this._apiBaseUrl}/ledgers/general-ledger/create/`, JSON.stringify(body))
      .map(res => res.json())
  }

  public removeGenLedger(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/ledgers/general-ledger/${_id_}/`)
      .map(res => res.json())
  }

  public updateGenLedger(body: IGenLedgerConfig, _id_: number): Observable<IGenLedgerConfig> {
    return this._authHttp.patch(`${this._apiBaseUrl}/ledgers/general-ledger/${_id_}/`, JSON.stringify(body))
      .map(res => res.json())
  }

}
