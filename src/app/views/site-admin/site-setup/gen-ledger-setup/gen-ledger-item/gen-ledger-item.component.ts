import { Component, Input, OnChanges } from '@angular/core';
import { IGenLedgerConfig } from '../../../../../initialiaze/models/site-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GenLedgerService } from '../services/gen-ledger.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { GLTypes } from '../../../../../shared/constants/gl-types';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';
import * as genLedgersSetupActions from '../actions/gen-ledger-setup.actions';

@Component({
  selector: 'daccs-gen-ledger-item',
  templateUrl: './gen-ledger-item.component.html',
  styleUrls: ['../../shared/site-setup.scss']
})
export class GenLedgerItemComponent implements OnChanges {

  @Input() genLedger: IGenLedgerConfig;

  public genLedgerUpdateForm: FormGroup;
  public glTypes: Array<{ value: number, title: string }>;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _fb: FormBuilder,
              private _genLedgerService: GenLedgerService,
              private _store: Store<IAppStore>) {

    this.glTypes = UtilityFunctions.enumSelector(GLTypes);

    this.genLedgerUpdateForm = this._fb.group({
      _id_: '',
      title: '',
      code: '',
      ledger_type: '',
    })
  }

  public onUpdateGenLedger(body: IGenLedgerConfig) {
    this.busy = true;
    this._genLedgerService.updateGenLedger(body, body._id_)
      .subscribe((res: IGenLedgerConfig) => {
        this._store.dispatch(new genLedgersSetupActions.UpdateGenLedgerConfig(res));
        this.busy = false;
      }, err => {
        console.log(err);
        this.busy = false;
      });
  }

  public onRemoveGenLedger() {
    this._genLedgerService.removeGenLedger(this.genLedger._id_)
      .subscribe(() => {
        this._store.dispatch(new genLedgersSetupActions.RemoveGenLedgerConfig(this.genLedger._id_));
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnChanges(): void {
    if (this.genLedger && this.genLedgerUpdateForm) {
      this.genLedgerUpdateForm.setValue({
        _id_: this.genLedger._id_,
        title: this.genLedger.title,
        code: this.genLedger.code,
        ledger_type: this.genLedger.ledger_type,
      });
    }
  }

}
