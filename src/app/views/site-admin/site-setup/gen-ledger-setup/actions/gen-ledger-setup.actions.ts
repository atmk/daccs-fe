import { IGenLedgerConfig } from '../../../../../initialiaze/models/site-config';
import { Action } from '@ngrx/store';

export const LOAD_GEN_LEDGERS = 'LOAD_GEN_LEDGERS';
export const ADD_GEN_LEDGER = 'ADD_GEN_LEDGER';
export const REMOVE_GEN_LEDGER = 'REMOVE_GEN_LEDGER';
export const UPDATE_GEN_LEDGER = 'UPDATE_GEN_LEDGER';

export class LoadGenLedgersConfig implements Action {
  readonly type = LOAD_GEN_LEDGERS;

  constructor(public payload: Array<IGenLedgerConfig>) {
  }
}

export class AddGenLedgerConfig implements Action {
  readonly type = ADD_GEN_LEDGER;

  constructor(public payload: IGenLedgerConfig) {
  }
}

export class RemoveGenLedgerConfig implements Action {
  readonly type = REMOVE_GEN_LEDGER;

  constructor(public payload: number) {
  }
}

export class UpdateGenLedgerConfig implements Action {
  readonly type = UPDATE_GEN_LEDGER;

  constructor(public payload: IGenLedgerConfig) {
  }
}

export type All = LoadGenLedgersConfig
  | AddGenLedgerConfig
  | RemoveGenLedgerConfig
  | UpdateGenLedgerConfig
