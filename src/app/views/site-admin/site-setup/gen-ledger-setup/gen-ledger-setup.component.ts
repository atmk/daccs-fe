import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IGenLedgerConfig } from '../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { GenLedgerService } from './services/gen-ledger.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { UtilityFunctions } from '../../../../shared/utils/utility-functions';
import { GLTypes } from '../../../../shared/constants/gl-types';
import * as genLedgersSetupActions from './actions/gen-ledger-setup.actions';

@Component({
  selector: 'daccs-gen-ledger-setup',
  templateUrl: './gen-ledger-setup.component.html',
  styleUrls: ['../shared/site-setup.scss']
})
export class GenLedgerSetupComponent implements OnInit {

  public genLedgers$: Observable<Array<IGenLedgerConfig>>;
  public genLedgerAddForm: FormGroup;
  public busy = false;
  public loading = false;
  public glTypes: Array<{ value: number, title: string }>;

  constructor(private _genLedgerService: GenLedgerService,
              private _fb: FormBuilder,
              private _store: Store<IAppStore>) {
    this.genLedgers$ = this._genLedgerService.genLedgers$;

    this.glTypes = UtilityFunctions.enumSelector(GLTypes);
  }

  public onAddGenLedger(body: IGenLedgerConfig): void {
    this.busy = true;
    this._genLedgerService.addGenLedger(body)
      .subscribe((res: IGenLedgerConfig) => {
        this.busy = false;

        this._store.dispatch(new genLedgersSetupActions.AddGenLedgerConfig(res));

        this.genLedgerAddForm.setValue({
          title: '',
          code: '',
          ledger_type: '',
        });
        this.genLedgerAddForm.markAsPristine();

      }, err => {
        this.busy = false;
        console.log(err);
      });
  }

  ngOnInit(): void {

    this.loading = true;
    this._genLedgerService.loadGenLedgers()
      .subscribe((data: Array<IGenLedgerConfig>) => {
        this.loading = false;
        this._store.dispatch(new genLedgersSetupActions.LoadGenLedgersConfig(data));
      }, err => {
        this.loading = false;
        console.log(err);
      });

    this.genLedgerAddForm = this._fb.group({
      title: '',
      code: '',
      ledger_type: ['', Validators.required],
    })
  }

}
