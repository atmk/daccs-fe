import * as genLedgersSetupActions from '../actions/gen-ledger-setup.actions';
import { IGenLedgerConfig } from '../../../../../initialiaze/models/site-config';

export type Action = genLedgersSetupActions.All;

export function genLedgersSetupReducer(state: Array<IGenLedgerConfig> = [], action: Action): Array<IGenLedgerConfig> {
  switch (action.type) {

    case genLedgersSetupActions.LOAD_GEN_LEDGERS:
      return action.payload;

    case genLedgersSetupActions.ADD_GEN_LEDGER:
      return [
        ...state,
        action.payload
      ];

    case genLedgersSetupActions.REMOVE_GEN_LEDGER:
      return state.filter((genL: IGenLedgerConfig) => {
        return genL._id_ !== action.payload;
      });

    case genLedgersSetupActions.UPDATE_GEN_LEDGER:
      return state.map((genL: IGenLedgerConfig) => {
        return genL._id_ === action.payload._id_
          ? Object.assign({}, genL, action.payload)
          : genL;
      });

    default:
      return state;

  }
}
