import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'systemGl'
})
export class SystemGlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    value.sort((a, b) => {
      return b.site - a.site;
    });
    return value;
  }

}
