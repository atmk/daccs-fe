import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../../environments/environment';
import { IAccHolderConfig } from '../../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';

@Injectable()
export class AccHolderService {

  public accHolders$: Observable<Array<IAccHolderConfig>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.accHolders$ = _store.select('accHoldersSetupReducer');
  }

  public loadAccHolders(): Observable<Array<IAccHolderConfig>> {
    return this._authHttp.get(`${this._apiBaseUrl}/ledgers/account-holder/`)
      .map(res => res.json())
  }

  public addAccHolder(body: IAccHolderConfig): Observable<IAccHolderConfig> {
    return this._authHttp.post(`${this._apiBaseUrl}/ledgers/account-holder/create/`, JSON.stringify(body))
      .map(res => res.json())
  }

  public removeAccHolder(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/ledgers/account-holder/${_id_}/`)
      .map(res => res.json())
  }

  public updateAccHolder(body: IAccHolderConfig, _id_: number): Observable<IAccHolderConfig> {
    return this._authHttp.patch(`${this._apiBaseUrl}/ledgers/account-holder/${_id_}/`, JSON.stringify(body))
      .map(res => res.json())
  }

}
