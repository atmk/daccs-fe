import { Action } from '@ngrx/store';
import { IAccHolderConfig } from '../../../../../initialiaze/models/site-config';

export const LOAD_ACC_HOLDERS = 'LOAD_ACC_HOLDERS';
export const ADD_ACC_HOLDER = 'ADD_ACC_HOLDER';
export const REMOVE_ACC_HOLDER = 'REMOVE_ACC_HOLDER';
export const UPDATE_ACC_HOLDER = 'UPDATE_ACC_HOLDER';

export class LoadAccHoldersConfig implements Action {
  readonly type = LOAD_ACC_HOLDERS;

  constructor(public payload: Array<IAccHolderConfig>) {
  }
}

export class AddAccHolderConfig implements Action {
  readonly type = ADD_ACC_HOLDER;

  constructor(public payload: IAccHolderConfig) {
  }
}

export class RemoveAccHolderConfig implements Action {
  readonly type = REMOVE_ACC_HOLDER;

  constructor(public payload: number) {
  }
}

export class UpdateAccHolderConfig implements Action {
  readonly type = UPDATE_ACC_HOLDER;

  constructor(public payload: IAccHolderConfig) {
  }
}

export type All = LoadAccHoldersConfig
  | AddAccHolderConfig
  | RemoveAccHolderConfig
  | UpdateAccHolderConfig
