import { Component, OnInit } from '@angular/core';
import { IAccHolderConfig } from '../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccHolderService } from './services/account-holders.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import * as accHoldersActions from './actions/acc-holders-setup.actions';

@Component({
  selector: 'daccs-debtors-setup',
  templateUrl: './acc-holders-setup.component.html',
  styleUrls: ['../shared/site-setup.scss']
})
export class AccHoldersSetupComponent implements OnInit {

  public accHolders$: Observable<Array<IAccHolderConfig>>;
  public accHolderAddForm: FormGroup;
  public busy = false;
  public loading = false;

  constructor(private _accHoldersService: AccHolderService,
              private _fb: FormBuilder,
              private _store: Store<IAppStore>) {
    this.accHolders$ = this._accHoldersService.accHolders$
  }

  public onAddAccHolder(body: IAccHolderConfig): void {
    this.busy = true;
    this._accHoldersService.addAccHolder(body)
      .subscribe((res: IAccHolderConfig) => {
        this.busy = false;

        this._store.dispatch(new accHoldersActions.AddAccHolderConfig(res));

        this.accHolderAddForm.setValue({
          title: '',
          code: '',
        });
        this.accHolderAddForm.markAsPristine();

      }, err => {
        this.busy = false;
        console.log(err);
      });
  }

  ngOnInit(): void {

    this.loading = true;
    this._accHoldersService.loadAccHolders()
      .subscribe((data: Array<IAccHolderConfig>) => {
        this.loading = false;
        this._store.dispatch(new accHoldersActions.LoadAccHoldersConfig(data));
      }, err => {
        this.loading = false;
        console.log(err);
      });

    this.accHolderAddForm = this._fb.group({
      title: '',
      code: '',
    })
  }

}
