import * as accHoldersSetupActions from '../actions/acc-holders-setup.actions';
import { IAccHolderConfig } from '../../../../../initialiaze/models/site-config';

export type Action = accHoldersSetupActions.All;

export function accHoldersSetupReducer(state: Array<IAccHolderConfig> = [], action: Action): Array<IAccHolderConfig> {
  switch (action.type) {

    case accHoldersSetupActions.LOAD_ACC_HOLDERS:
      return action.payload;

    case accHoldersSetupActions.ADD_ACC_HOLDER:
      return [
        ...state,
        action.payload
      ];

    case accHoldersSetupActions.REMOVE_ACC_HOLDER:
      return state.filter((acc: IAccHolderConfig) => {
        return acc._id_ !== action.payload;
      });

    case accHoldersSetupActions.UPDATE_ACC_HOLDER:
      return state.map((acc: IAccHolderConfig) => {
        return acc._id_ === action.payload._id_
          ? Object.assign({}, acc, action.payload)
          : acc;
      });

    default:
      return state;

  }
}
