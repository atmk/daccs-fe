import { Component, Input, OnChanges } from '@angular/core';
import { IAccHolderConfig } from '../../../../../initialiaze/models/site-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccHolderService } from '../services/account-holders.service';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as accHoldersActions from '../actions/acc-holders-setup.actions';

@Component({
  selector: 'daccs-acc-holder-item',
  templateUrl: './acc-holder-item.component.html',
  styleUrls: ['../../shared/site-setup.scss']
})
export class AccHolderItemComponent implements OnChanges {

  @Input() accHolder: IAccHolderConfig;

  public accHolderUpdateForm: FormGroup;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _fb: FormBuilder,
              private _accHolderService: AccHolderService,
              private _store: Store<IAppStore>) {
    this.accHolderUpdateForm = this._fb.group({
      _id_: '',
      title: '',
      code: '',
    })
  }

  public onUpdateAccHolder(body: IAccHolderConfig) {
    this.busy = true;
    this._accHolderService.updateAccHolder(body, body._id_)
      .subscribe((res: IAccHolderConfig) => {
        this._store.dispatch(new accHoldersActions.UpdateAccHolderConfig(res));
        this.busy = false;
      }, err => {
        console.log(err);
        this.busy = false;
      });
  }

  public onRemoveAccHolder() {
    this._accHolderService.removeAccHolder(this.accHolder._id_)
      .subscribe(() => {
        this._store.dispatch(new accHoldersActions.RemoveAccHolderConfig(this.accHolder._id_));
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnChanges(): void {
    if (this.accHolder && this.accHolderUpdateForm) {
      this.accHolderUpdateForm.setValue({
        _id_: this.accHolder._id_,
        title: this.accHolder.title,
        code: this.accHolder.code,
      });
    }
  }

}
