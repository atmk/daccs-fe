import { Component, OnInit } from '@angular/core';
import { ICashierConfig } from '../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CashierService } from './services/cashier.service';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as cashiersSetupActions from './actions/cashiers-setup.actions';

@Component({
  selector: 'daccs-cashiers-setup',
  templateUrl: './cashiers-setup.component.html',
  styleUrls: ['../shared/site-setup.scss']
})
export class CashiersSetupComponent implements OnInit {

  public cashiers$: Observable<Array<ICashierConfig>>;
  public cashierAddForm: FormGroup;
  public busy = false;
  public loading = false;

  constructor(private _cashierService: CashierService,
              private _store: Store<IAppStore>,
              private _fb: FormBuilder) {
    this.cashiers$ = this._cashierService.cashiers$
  }

  public onAddCashier(body: ICashierConfig): void {
    console.log(body);
    this.busy = true;
    this._cashierService.addCashier(body)
      .subscribe((res: ICashierConfig) => {
        this.busy = false;

        this._store.dispatch(new cashiersSetupActions.AddCashierConfig(res));

        this.cashierAddForm.setValue({
          name: '',
          surname: '',
          employee_number: '',
          id_number: '',
        });
        this.cashierAddForm.markAsPristine();

      }, err => {
        this.busy = false;
        console.log(err);
      });
  }

  ngOnInit(): void {

    this.loading = true;
    this._cashierService.loadCashiers()
      .subscribe((data: Array<ICashierConfig>) => {
        this.loading = false;
        this._store.dispatch(new cashiersSetupActions.LoadCashiersConfig(data));
      }, err => {
        this.loading = false;
        console.log(err);
      });

    this.cashierAddForm = this._fb.group({
      name: '',
      surname: '',
      employee_number: '',
      id_number: '',
    })
  }
}
