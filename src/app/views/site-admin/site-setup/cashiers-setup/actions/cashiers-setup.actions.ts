import { ICashierConfig } from '../../../../../initialiaze/models/site-config';
import { Action } from '@ngrx/store';

export const LOAD_CASHIERS = 'LOAD_CASHIERS';
export const ADD_CASHIER = 'ADD_CASHIER';
export const REMOVE_CASHIER = 'REMOVE_CASHIER';
export const UPDATE_CASHIER = 'UPDATE_CASHIER';

export class LoadCashiersConfig implements Action {
  readonly type = LOAD_CASHIERS;

  constructor(public payload: Array<ICashierConfig>) {
  }
}

export class AddCashierConfig implements Action {
  readonly type = ADD_CASHIER;

  constructor(public payload: ICashierConfig) {
  }
}

export class RemoveCashierConfig implements Action {
  readonly type = REMOVE_CASHIER;

  constructor(public payload: number) {
  }
}

export class UpdateCashierConfig implements Action {
  readonly type = UPDATE_CASHIER;

  constructor(public payload: ICashierConfig) {
  }
}

export type All = LoadCashiersConfig
  | AddCashierConfig
  | RemoveCashierConfig
  | UpdateCashierConfig
