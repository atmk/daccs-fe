import * as cashiersSetupActions from '../actions/cashiers-setup.actions';
import { ICashierConfig } from '../../../../../initialiaze/models/site-config';

export type Action = cashiersSetupActions.All;

export function cashiersSetupReducer(state: Array<ICashierConfig> = [], action: Action): Array<ICashierConfig> {
  switch (action.type) {

    case cashiersSetupActions.LOAD_CASHIERS:
      return action.payload;

    case cashiersSetupActions.ADD_CASHIER:
      return [
        ...state,
        action.payload
      ];

    case cashiersSetupActions.REMOVE_CASHIER:
      return state.filter((cshr: ICashierConfig) => {
        return cshr._id_ !== action.payload;
      });

    case cashiersSetupActions.UPDATE_CASHIER:
      return state.map((cshr: ICashierConfig) => {
        return cshr._id_ === action.payload._id_
          ? Object.assign({}, cshr, action.payload)
          : cshr;
      });

    default:
      return state;

  }
}
