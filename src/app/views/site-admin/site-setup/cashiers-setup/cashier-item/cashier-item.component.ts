import { Component, Input, OnChanges } from '@angular/core';
import { ICashierConfig } from '../../../../../initialiaze/models/site-config';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CashierService } from '../services/cashier.service';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as cashiersSetupActions from '../actions/cashiers-setup.actions';

@Component({
  selector: 'daccs-cashier-item',
  templateUrl: './cashier-item.component.html',
  styleUrls: ['../../shared/site-setup.scss']
})
export class CashierItemComponent implements OnChanges {

  @Input() cashier: ICashierConfig;

  public editable = false;
  public cashierUpdateForm: FormGroup;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _fb: FormBuilder,
              private _cashierService: CashierService,
              private _store: Store<IAppStore>) {
    this.cashierUpdateForm = this._fb.group({
      _id_: '',
      name: '',
      surname: '',
      employee_number: '',
      id_number: '',
    })
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public onUpdateCashier(body: ICashierConfig) {
    this.busy = true;
    this._cashierService.updateCashier(body, body._id_)
      .subscribe((res: ICashierConfig) => {
        this._store.dispatch(new cashiersSetupActions.UpdateCashierConfig(res));
        this.busy = false;
      }, err => {
        console.log(err);
        this.busy = false;
      });
  }

  public onRemoveCashier() {
    this._cashierService.removeCashier(this.cashier._id_)
      .subscribe(() => {
        this._store.dispatch(new cashiersSetupActions.RemoveCashierConfig(this.cashier._id_));
      });
  }

  ngOnChanges(): void {
    if (this.cashier && this.cashierUpdateForm) {
      this.cashierUpdateForm.setValue({
        _id_: this.cashier._id_,
        name: this.cashier.name,
        surname: this.cashier.surname,
        employee_number: this.cashier.employee_number,
        id_number: this.cashier.id_number,
      });
    }
  }

  public toggleEdit(): void {
    this.editable = !this.editable;
  }

}
