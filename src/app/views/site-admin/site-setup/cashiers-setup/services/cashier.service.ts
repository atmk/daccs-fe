import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { ICashierConfig } from '../../../../../initialiaze/models/site-config';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';

@Injectable()
export class CashierService {

  public cashiers$: Observable<Array<ICashierConfig>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.cashiers$ = _store.select('cashiersSetupReducer');
  }

  public loadCashiers(): Observable<Array<ICashierConfig>> {
    return this._authHttp.get(`${this._apiBaseUrl}/recons/cashier/`)
      .map(res => res.json());
  }

  public addCashier(body: ICashierConfig): Observable<ICashierConfig> {
    return this._authHttp.post(`${this._apiBaseUrl}/recons/cashier/create/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public removeCashier(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/recons/cashier/${_id_}/`)
      .map(res => res.json());
  }

  public updateCashier(body: ICashierConfig, _id_: number): Observable<ICashierConfig> {
    return this._authHttp.patch(`${this._apiBaseUrl}/recons/cashier/${_id_}/`, JSON.stringify(body))
      .map(res => res.json());
  }

}
