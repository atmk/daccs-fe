import * as supplierSetupActions from '../actions/suppliers-setup.actions';
import { ISupplierConfig } from '../../../../../initialiaze/models/site-config';

export type Action = supplierSetupActions.All;

export function suppliersSetupReducer(state: Array<any>, action: Action): Array<any> {
  switch (action.type) {

    case supplierSetupActions.LOAD_SUPPLIERS:
      return action.payload;

    case supplierSetupActions.ADD_SUPPLIER:
      return [
        ...state,
        action.payload
      ];

    case supplierSetupActions.REMOVE_SUPPLIER:
      return state.filter((supplier: ISupplierConfig) => {
        return supplier._id_ !== action.payload;
      });

    case supplierSetupActions.UPDATE_SUPPLIER:
      return state.map((supplier: ISupplierConfig) => {
        return supplier._id_ === action.payload._id_
          ? Object.assign({}, supplier, action.payload)
          : supplier;
      });

    default:
      return state;

  }
}
