import { Injectable } from '@angular/core';
import { ISupplierConfig } from '../../../../../initialiaze/models/site-config';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';

@Injectable()
export class SuppliersService {

  public suppliers$: Observable<Array<ISupplierConfig>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.suppliers$ = _store.select('suppliersSetupReducer');
  }

  public loadSuppliers(): Observable<Array<ISupplierConfig>> {
    return this._authHttp.get(`${this._apiBaseUrl}/ledgers/supplier/`)
      .map(res => res.json())
  }

  public addSupplier(body: ISupplierConfig): Observable<ISupplierConfig> {
    return this._authHttp.post(`${this._apiBaseUrl}/ledgers/supplier/create/`, JSON.stringify(body))
      .map(res => res.json())
  }

  public removeSupplier(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/ledgers/supplier/${_id_}/`)
      .map(res => res.json())
  }

  public updateSupplier(body: ISupplierConfig, _id_: number): Observable<ISupplierConfig> {
    return this._authHttp.patch(`${this._apiBaseUrl}/ledgers/supplier/${_id_}/`, JSON.stringify(body))
      .map(res => res.json())
  }

}
