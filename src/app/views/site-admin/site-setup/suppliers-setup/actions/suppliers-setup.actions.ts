import { ISupplierConfig } from '../../../../../initialiaze/models/site-config';
import { Action } from '@ngrx/store';

export const LOAD_SUPPLIERS = 'LOAD_SUPPLIERS';
export const ADD_SUPPLIER = 'ADD_SUPPLIER';
export const REMOVE_SUPPLIER = 'REMOVE_SUPPLIER';
export const UPDATE_SUPPLIER = 'UPDATE_SUPPLIER';

export class LoadSuppliersConfig implements Action {
  readonly type = LOAD_SUPPLIERS;

  constructor(public payload: Array<ISupplierConfig>) {
  }
}

export class AddSupplierConfig implements Action {
  readonly type = ADD_SUPPLIER;

  constructor(public payload: ISupplierConfig) {
  }
}

export class RemoveSupplierConfig implements Action {
  readonly type = REMOVE_SUPPLIER;

  constructor(public payload: number) {
  }
}

export class UpdateSupplierConfig implements Action {
  readonly type = UPDATE_SUPPLIER;

  constructor(public payload: ISupplierConfig) {
  }
}

export type All = LoadSuppliersConfig
  | AddSupplierConfig
  | RemoveSupplierConfig
  | UpdateSupplierConfig
