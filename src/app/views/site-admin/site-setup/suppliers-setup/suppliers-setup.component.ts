import { Component, OnInit } from '@angular/core';
import { ISupplierConfig } from '../../../../initialiaze/models/site-config';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SuppliersService } from './services/suppliers.service';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import * as supplierSetupActions from './actions/suppliers-setup.actions';

@Component({
  selector: 'daccs-suppliers-setup',
  templateUrl: './suppliers-setup.component.html',
  styleUrls: ['../shared/site-setup.scss']
})
export class SuppliersSetupComponent implements OnInit {

  public suppliers$: Observable<Array<ISupplierConfig>>;
  public supplierAddForm: FormGroup;
  public busy = false;
  public loading = false;

  constructor(private _suppliersService: SuppliersService,
              private _fb: FormBuilder,
              private _store: Store<IAppStore>) {
    this.suppliers$ = this._suppliersService.suppliers$
  }

  public onAddSupplier(body: ISupplierConfig): void {
    this.busy = true;
    this._suppliersService.addSupplier(body)
      .subscribe((res: ISupplierConfig) => {
        this.busy = false;

        this._store.dispatch(new supplierSetupActions.AddSupplierConfig(res));

        this.supplierAddForm.setValue({
          title: '',
          code: '',
        });
        this.supplierAddForm.markAsPristine();

      }, err => {
        this.busy = false;
        console.log(err);
      });
  }

  ngOnInit(): void {

    this.loading = true;
    this._suppliersService.loadSuppliers()
      .subscribe((data: Array<ISupplierConfig>) => {
        this.loading = false;
        this._store.dispatch(new supplierSetupActions.LoadSuppliersConfig(data));
      }, err => {
        this.loading = false;
        console.log(err);
      });

    this.supplierAddForm = this._fb.group({
      title: '',
      code: '',
    })
  }

}
