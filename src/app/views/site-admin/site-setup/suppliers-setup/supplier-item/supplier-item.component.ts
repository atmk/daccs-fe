import { Component, Input, OnChanges } from '@angular/core';
import { ISupplierConfig } from '../../../../../initialiaze/models/site-config';
import { SuppliersService } from '../services/suppliers.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import * as supplierSetupActions from '../actions/suppliers-setup.actions';

@Component({
  selector: 'daccs-supplier-item',
  templateUrl: './supplier-item.component.html',
  styleUrls: ['../../shared/site-setup.scss']
})
export class SupplierItemComponent implements OnChanges {

  @Input() supplier: ISupplierConfig;

  public supplierUpdateForm: FormGroup;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _fb: FormBuilder,
              private _suppliersService: SuppliersService,
              private _store: Store<IAppStore>) {
    this.supplierUpdateForm = this._fb.group({
      _id_: '',
      title: '',
      code: '',
    })
  }

  public onUpdateSupplier(body: ISupplierConfig) {
    this.busy = true;
    this._suppliersService.updateSupplier(body, body._id_)
      .subscribe((res: ISupplierConfig) => {
        this._store.dispatch(new supplierSetupActions.UpdateSupplierConfig(res));
        this.busy = false;
      }, err => {
        console.log(err);
        this.busy = false;
      });
  }

  public onRemoveSupplier() {
    this._suppliersService.removeSupplier(this.supplier._id_)
      .subscribe(() => {
        this._store.dispatch(new supplierSetupActions.RemoveSupplierConfig(this.supplier._id_));
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnChanges(): void {
    if (this.supplier && this.supplierUpdateForm) {
      this.supplierUpdateForm.setValue({
        _id_: this.supplier._id_,
        title: this.supplier.title,
        code: this.supplier.code,
      });
    }
  }

}
