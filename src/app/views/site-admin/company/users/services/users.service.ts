import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { IUser } from '../../../../../shared/models/user';
import { environment } from '../../../../../../environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { ISite } from '../../sites/models/site';

@Injectable()

export class UsersService {
  public users$: Observable<Array<IUser>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.users$ = this._store.select('usersReducer');
  }

  public getUsers(): Observable<Array<IUser>> {
    return this._authHttp.get(`${this._apiBaseUrl}/users/user/`)
      .map(res => res.json());
  }

  public getUserById(id): Observable<[IUser, Array<ISite>]> {

    const user = this._authHttp.get(`${this._apiBaseUrl}/users/user/${id}/`).map(res => res.json());
    const sites = this._authHttp.get(`${this._apiBaseUrl}/companies/site/`).map(res => res.json());

    return Observable.forkJoin(user, sites);
  }

  public createUser(body: IUser): Observable<IUser> {
    return this._authHttp.post(`${this._apiBaseUrl}/users/user/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public updateUser(body: IUser): Observable<IUser> {
    return this._authHttp.patch(`${this._apiBaseUrl}/users/user/${body.id}/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public changePassword(body: IUser): Observable<IUser> {
    return this._authHttp.patch(`${this._apiBaseUrl}/users/user/password/update/${body.id}/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public removeUser(id: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/users/user/${id}/`)
      .map(res => res.json());
  }
}

