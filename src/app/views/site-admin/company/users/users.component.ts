import { Component, OnInit } from '@angular/core';
import { UsersService } from './services/users.service';
import { IUser } from '../../../../shared/models/user';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as usersAct from './actions/users.actions';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'daccs-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public users$: Observable<Array<IUser>>;
  public remModalIsOpen = false;
  public userIdSelected: number;
  public busy = false;
  public loading = false;

  constructor(private _usersService: UsersService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {
    this.users$ = this._usersService.users$;
  }

  public onRemoveUser(): void {
    this.busy = true;
    this._usersService.removeUser(this.userIdSelected)
      .subscribe(() => {
        this._store.dispatch(new usersAct.RemoveUser(this.userIdSelected));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'User deleted');
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev: Event, id: number): void {
    ev.preventDefault();
    this.userIdSelected = id;
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnInit() {
    this.loading = true;
    this._usersService.getUsers()
      .subscribe((res: Array<IUser>) => {
        this._store.dispatch(new usersAct.LoadUsers(res));
        this.loading = false;
      }, err => {
        console.log(err);
        this.loading = false;
      })
  }

}

