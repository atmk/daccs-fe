import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { Store } from '@ngrx/store';
import { UsersService } from '../services/users.service';
import { IUser } from '../../../../../shared/models/user';
import * as userAct from '../actions/users.actions'
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import { Observable } from 'rxjs/Observable';
import { ISite } from '../../sites/models/site';
import { SitesService } from '../../sites/services/sites.service';
import { ROLES } from '../../../../../shared/constants/role';

@Component({
  selector: 'daccs-user-new',
  templateUrl: 'user-new.component.html',
  styleUrls: ['user-new.component.scss']
})
export class UserNewComponent implements OnInit {
  public createNewUserForm: FormGroup;
  public busy = false;
  public sites$: Observable<Array<ISite>>;
  public roles: Array<{ title: string, id: number }>;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>,
              private _usersService: UsersService,
              private _sitesService: SitesService,
              private _actRoute: ActivatedRoute) {
    this.roles = ROLES;
  }

  public onCreateUser(body: IUser) {
    this.busy = true;
    this._usersService.createUser(body)
      .subscribe((res: IUser) => {
        this._store.dispatch(new userAct.CreateUser(res));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'New user added');
        this._router.navigate(['../'], {relativeTo: this._actRoute});
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
  }

  ngOnInit(): void {
    this.sites$ = this._sitesService.getSites();

    this.createNewUserForm = this._fb.group({
      first_name: '',
      last_name: '',
      designation: ['', Validators.required],
      site: '',
      mobile_number: '',
      email: ['', [CustomSyncValidators.emailValidator, Validators.required]],
    })
  }

}
