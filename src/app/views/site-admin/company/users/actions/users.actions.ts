import { Action } from '@ngrx/store';
import { IUser } from '../../../../../shared/models/user';

export const LOAD_USERS = 'LOAD_USERS';
export const CREATE_USER = 'CREATE_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const REMOVE_USER = 'REMOVE_USER';

export class LoadUsers implements Action {
  readonly type = LOAD_USERS;

  constructor(public payload: Array<IUser>) {
  }
}

export class CreateUser implements Action {
  readonly type = CREATE_USER;

  constructor(public payload: IUser) {
  }
}

export class UpdateUser implements Action {
  readonly type = UPDATE_USER;

  constructor(public payload: IUser) {
  }
}

export class RemoveUser implements Action {
  readonly type = REMOVE_USER;

  constructor(public payload: number) {
  }
}

export type All = LoadUsers | CreateUser | UpdateUser | RemoveUser
