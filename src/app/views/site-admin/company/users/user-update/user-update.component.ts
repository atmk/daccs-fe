import { Component, OnInit } from '@angular/core';
import { IUser } from '../../../../../shared/models/user';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { UsersService } from '../services/users.service';
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import * as userAct from '../actions/users.actions'
import { ISite } from '../../sites/models/site';
import { ROLES } from '../../../../../shared/constants/role';

@Component({
  selector: 'daccs-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  public user: IUser;
  public sites: Array<ISite>;
  public loading = false;
  public updateUserForm: FormGroup;
  public busy = false;
  public roles: Array<{ title: string, id: number }>;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>,
              private _usersService: UsersService) {
    this.roles = ROLES;
    this.updateUserForm = this._fb.group({
      first_name: '',
      last_name: '',
      designation:  ['', Validators.required],
      site: '',
      suspended: '',
      is_staff: '',
      is_active: '',
      mobile_number: '',
      email: ['', [CustomSyncValidators.emailValidator, Validators.required]],
      id: '',
    })
  }

  public onUpdateUser(body: IUser) {
    this.busy = true;
    this._usersService.updateUser(body)
      .subscribe((res: IUser) => {
        this._store.dispatch(new userAct.UpdateUser(res));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'User updated');
        this._router.navigate(['../../'], {relativeTo: this._actRoute});
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
  }

  ngOnInit() {
    this.loading = true;
    this._actRoute.paramMap
      .switchMap((params: ParamMap) => {
        return this._usersService.getUserById(+params.get('user_id'));
      }).subscribe(
      (data: [IUser, Array<ISite>]) => {
        this.loading = false;
        this.user = data[0];
        this.sites = data[1];
        this.updateUserForm.setValue({
          first_name: this.user.first_name,
          last_name: this.user.last_name,
          designation: this.user.designation,
          site: this.user.site,
          suspended: this.user.suspended,
          is_staff: this.user.is_staff,
          is_active: this.user.is_active,
          email: this.user.email,
          mobile_number: this.user.mobile_number,
          id: this.user.id,
        })
      },
      err => {
        this.loading = false;
        console.log(err);
      })
  }

}
