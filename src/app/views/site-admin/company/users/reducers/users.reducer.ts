import * as UserActions from '../actions/users.actions';
import { IUser } from '../../../../../shared/models/user';

export type Action = UserActions.All;

export function usersReducer(state: Array<IUser> = [], action: Action): Array<IUser> {
  switch (action.type) {

    case UserActions.LOAD_USERS:
      return action.payload;

    case UserActions.CREATE_USER:
      return [...state, action.payload];

    case UserActions.UPDATE_USER:
      if (!state.length) {
        return [action.payload]
      } else {
        return state.map(user => {
          return user.id === action.payload.id ? Object.assign({}, user, action.payload) : user;
        });
      }

    case UserActions.REMOVE_USER:
      return state.filter(user => {
        return user.id !== action.payload;
      });
    default:
      return state;
  }
}
