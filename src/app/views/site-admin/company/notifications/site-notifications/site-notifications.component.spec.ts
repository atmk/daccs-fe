import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteNotificationsComponent } from './site-notifications.component';

describe('SiteNotificationsComponent', () => {
  let component: SiteNotificationsComponent;
  let fixture: ComponentFixture<SiteNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
