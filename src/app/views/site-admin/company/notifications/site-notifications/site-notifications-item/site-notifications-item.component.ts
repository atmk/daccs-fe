import { Component, Input, OnChanges } from '@angular/core';
import { ISiteNotifySetting } from '../../models/site-notifications';
import { NotificationsService } from '../../services/notifications.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { IUser } from '../../../../../../shared/models/user';
import { SiteProfileService } from '../../../../../../initialiaze/services/site-profile.service';
import { ISite } from '../../../sites/models/site';
import { Observable } from 'rxjs/Observable';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as siteNotifyActions from '../../actions/site-notifications.actions'
import { UtilityFunctions } from '../../../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-site-notifications-item',
  templateUrl: './site-notifications-item.component.html',
  styleUrls: ['./site-notifications-item.component.scss']
})
export class SiteNotificationsItemComponent implements OnChanges {

  @Input() siteNotifyItem: ISiteNotifySetting;
  @Input() userList: Array<IUser>;
  @Input() even: boolean;
  @Input() odd: boolean;

  public siteProfile$: Observable<ISite>;

  public user: IUser;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _store: Store<IAppStore>,
              private _notificationsService: NotificationsService,
              private _siteProfileService: SiteProfileService,
              private _toasterService: ToasterService) {
    this.siteProfile$ = this._siteProfileService.siteProfile$;
  }

  public toggleNotification(ev: boolean, key: string) {
    this.busy = true;
    this._notificationsService.updateSiteNotifications({[key]: ev}, this.siteNotifyItem.id)
      .subscribe((notif: ISiteNotifySetting) => {
        this._store.dispatch(new siteNotifyActions.UpdateSiteNotification(notif));
        this.busy = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        console.log(err);
      });
  }

  public openRemModal(ev: Event): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public onRemoveNotif(id: number): void {
    console.log(this.siteNotifyItem);
    this.remModalIsOpen = false;
    this.busy = true;
    this._notificationsService.removeSiteNotifications(id)
      .subscribe(() => {
        this._store.dispatch(new siteNotifyActions.RemoveSiteNotification(id));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'Notification deleted');
      }, err => {
        const toast: Toast = {
          type: 'error',
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
        this.busy = false;
      });
    this.remModalIsOpen = false;
  }

  private _getUserName(userArr: Array<IUser>, id: number): IUser {
    return userArr.find((user: IUser) => {
      return user.id === id
    })
  }

  ngOnChanges(): void {
    if (this.userList) {
      this.user = this._getUserName(this.userList, this.siteNotifyItem.user)
    }
  }

}
