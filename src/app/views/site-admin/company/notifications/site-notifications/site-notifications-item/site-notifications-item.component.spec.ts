import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteNotificationsItemComponent } from './site-notifications-item.component';

describe('SiteNotificationsItemComponent', () => {
  let component: SiteNotificationsItemComponent;
  let fixture: ComponentFixture<SiteNotificationsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteNotificationsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteNotificationsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
