import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { ISiteNotifySetting } from '../models/site-notifications';
import { IUser } from '../../../../../shared/models/user';
import { ISubmitSiteNot, NotificationsService } from '../services/notifications.service';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';
import * as siteNotifyActions from '../actions/site-notifications.actions'
import { ISite } from '../../sites/models/site';
import { SiteProfileService } from '../../../../../initialiaze/services/site-profile.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'daccs-site-notifications',
  templateUrl: './site-notifications.component.html',
  styleUrls: ['./site-notifications.component.scss']
})
export class SiteNotificationsComponent implements OnChanges, OnDestroy {

  @Input() siteNotifyData: Array<ISiteNotifySetting>;
  @Input() userList: Array<IUser>;

  public busy = false;
  public userId = '';
  public selectedUser: IUser;
  public siteProfile: ISite;
  public siteNotifObj: ISiteNotifySetting = {
    daily_summary_sms: false,
    daily_summary_email: false,
    monthly_cumulative_email: false,
  };
  public profileSub: Subscription;

  constructor(private _notificationsService: NotificationsService,
              private _siteProfileService: SiteProfileService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {
    this.profileSub = this._siteProfileService.siteProfile$.subscribe((val: ISite) => {
      this.siteProfile = val;
    })
  }

  public onModelChanged(ev: number): void {
    this.selectedUser = this._getSelectedUser(ev)
  }

  public onSubmitNew(body: ISubmitSiteNot) {
    this.busy = true;
    this._notificationsService.addSiteNotifications(body)
      .subscribe((res: ISiteNotifySetting) => {
        this.busy = false;
        this.userId = '';
        this._store.dispatch(new siteNotifyActions.AddSiteNotification(res));
        this._toasterService.pop('success', 'Success',
          'Notification added');
      }, err => {
        const toast: Toast = {
          type: 'error',
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
        this.busy = false;
        this.userId = '';
      });
  }

  private _getSelectedUser(id: number): IUser {
    if (id) {
      return this.userList.find((user: IUser) => {
        return user.id === id;
      })
    } else {
      return null;
    }
  }

  ngOnChanges(): void {
    this.userList = this.userList.filter((user: IUser) => {
      return user.site === this.siteProfile.id
    })
  }

  ngOnDestroy(): void {
    this.profileSub.unsubscribe();
  }

}
