export interface ISiteNotifySetting {
  id?: number;
  user?: number;
  site?: number;
  daily_summary_sms: boolean;
  daily_summary_email: boolean;
  monthly_cumulative_email: boolean;
}
