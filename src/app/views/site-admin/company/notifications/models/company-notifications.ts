export interface ICompanyNotifySetting {
  id?: number;
  holding_company?: number;
  user?: number;
  newsletter_email: boolean;
}
