import { Injectable } from '@angular/core';
import 'rxjs/add/observable/forkJoin';
import { environment } from '../../../../../../environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { ICompanyNotifySetting } from '../models/company-notifications';
import { ISiteNotifySetting } from '../models/site-notifications';
import { Response } from '@angular/http';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';

export interface ISubmitCompanyNot {
  user: number;
  newsletter_email: boolean;
}

export interface ISubmitSiteNot {
  user: number;
  daily_summary_sms: boolean;
  daily_summary_email: boolean;
  monthly_cumulative_email: boolean;
}

@Injectable()
export class NotificationsService {

  public siteNotifications$: Observable<Array<ISiteNotifySetting>>;
  public companyNotifications$: Observable<Array<ICompanyNotifySetting>>;
  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.siteNotifications$ = this._store.select('siteNotificationsReducer');
    this.companyNotifications$ = this._store.select('companyNotificationsReducer');
  }

  public getAllNotificationSettings(): Observable<[Array<ISiteNotifySetting>, Array<ICompanyNotifySetting>]> {

    const siteNotifySettings = this._authHttp.get(`${this._apiBaseUrl}/notifications/notification/site/`)
      .map((res: Response) => res.json());
    const companyNotifySettings = this._authHttp.get(`${this._apiBaseUrl}/notifications/notification/company/`)
      .map((res: Response) => res.json());

    return Observable.forkJoin(siteNotifySettings, companyNotifySettings);
  }

  public addSiteNotifications(body: ISubmitSiteNot): Observable<ISiteNotifySetting> {
    return this._authHttp.post(`${this._apiBaseUrl}/notifications/notification/site/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public updateSiteNotifications(body: { [key: string]: boolean }, id: number): Observable<ISiteNotifySetting> {
    return this._authHttp.patch(`${this._apiBaseUrl}/notifications/notification/site/${id}/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public removeSiteNotifications(id: number): Observable<ISiteNotifySetting> {
    return this._authHttp.delete(`${this._apiBaseUrl}/notifications/notification/site/${id}/`)
      .map(res => res.json());
  }

  public addCompanyNotifications(body: ISubmitCompanyNot): Observable<ICompanyNotifySetting> {
    return this._authHttp.post(`${this._apiBaseUrl}/notifications/notification/company/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public updateCompanyNotifications(body: { [key: string]: boolean }, id: number): Observable<ICompanyNotifySetting> {
    return this._authHttp.patch(`${this._apiBaseUrl}/notifications/notification/company/${id}/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public removeCompanyNotifications(id: number): Observable<ISiteNotifySetting> {
    return this._authHttp.delete(`${this._apiBaseUrl}/notifications/notification/company/${id}/`)
      .map(res => res.json());
  }

}
