import * as companyNotifyActions from '../actions/company-notifications.actions';
import { ICompanyNotifySetting } from '../models/company-notifications';

export type Action = companyNotifyActions.All;

export function companyNotificationsReducer(state: Array<ICompanyNotifySetting> = [], action: Action): Array<ICompanyNotifySetting> {
  switch (action.type) {

    case companyNotifyActions.LOAD_COMPANY_NOTIFICATIONS:
      return action.payload;

    case companyNotifyActions.ADD_COMPANY_NOTIFICATION:
      return [
        ...state,
        action.payload
      ];

    case companyNotifyActions.REMOVE_COMPANY_NOTIFICATION:
      return state.filter((notif: ICompanyNotifySetting) => {
        return notif.id !== action.payload;
      });

    case companyNotifyActions.UPDATE_COMPANY_NOTIFICATION:
      return state.map((notify: ICompanyNotifySetting) => {
        return notify.id === action.payload.id
          ? Object.assign({}, notify, action.payload)
          : notify;
      });

    default:
      return state;

  }
}
