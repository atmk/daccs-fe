import * as siteNotifyActions from '../actions/site-notifications.actions';
import { ISiteNotifySetting } from '../models/site-notifications';

export type Action = siteNotifyActions.All;

export function siteNotificationsReducer(state: Array<ISiteNotifySetting> = [], action: Action): Array<ISiteNotifySetting> {
  switch (action.type) {

    case siteNotifyActions.LOAD_SITE_NOTIFICATIONS:
      return action.payload;

    case siteNotifyActions.ADD_SITE_NOTIFICATION:
      return [
        ...state,
        action.payload
      ];

    case siteNotifyActions.REMOVE_SITE_NOTIFICATION:
      return state.filter((notif: ISiteNotifySetting) => {
        return notif.id !== action.payload;
      });

    case siteNotifyActions.UPDATE_SITE_NOTIFICATION:
      return state.map((notify: ISiteNotifySetting) => {
        return notify.id === action.payload.id
          ? Object.assign({}, notify, action.payload)
          : notify;
      });

    default:
      return state;

  }
}
