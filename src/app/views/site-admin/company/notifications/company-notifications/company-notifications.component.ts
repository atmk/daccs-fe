import { Component, Input } from '@angular/core';
import { ICompanyNotifySetting } from '../models/company-notifications';
import { IUser } from '../../../../../shared/models/user';
import { ISubmitCompanyNot, NotificationsService } from '../services/notifications.service';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { UtilityFunctions } from '../../../../../shared/utils/utility-functions';
import * as companyNotifyActions from '../actions/company-notifications.actions'

@Component({
  selector: 'daccs-company-notifications',
  templateUrl: './company-notifications.component.html',
  styleUrls: ['./company-notifications.component.scss']
})
export class CompanyNotificationsComponent {

  @Input() companyNotifyData: Array<ICompanyNotifySetting>;
  @Input() userList: Array<IUser>;

  public busy = false;
  public userId = '';
  public selectedUser: IUser;
  public companyNotifObj: ICompanyNotifySetting = {
    newsletter_email: false
  };

  constructor(private _notificationsService: NotificationsService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {

  }

  public onModelChanged(ev: number): void {
    this.selectedUser = this._getSelectedUser(ev)
  }

  public onSubmitNew(body: ISubmitCompanyNot) {
    this.busy = true;
    this._notificationsService.addCompanyNotifications(body)
      .subscribe((res: ICompanyNotifySetting) => {
        this.busy = false;
        this.userId = '';
        this._store.dispatch(new companyNotifyActions.AddCompanyNotification(res));
        this._toasterService.pop('success', 'Success',
          'Notification added');
      }, err => {
        const toast: Toast = {
          type: 'error',
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
        this.busy = false;
        this.userId = '';
      });
  }

  private _getSelectedUser(id: number): IUser {
    if (id) {
      return this.userList.find((user: IUser) => {
        return user.id === id;
      })
    } else {
      return null;
    }
  }

}
