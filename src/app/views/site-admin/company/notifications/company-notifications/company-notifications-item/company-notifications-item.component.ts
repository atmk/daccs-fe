import { Component, Input, OnChanges } from '@angular/core';
import { ICompanyNotifySetting } from '../../models/company-notifications';
import { NotificationsService } from '../../services/notifications.service';
import { BodyOutputType, Toast, ToasterService } from 'angular2-toaster';
import { IUser } from '../../../../../../shared/models/user';
import { IAppStore } from '../../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as companyNotifyActions from '../../actions/company-notifications.actions'
import { UtilityFunctions } from '../../../../../../shared/utils/utility-functions';

@Component({
  selector: 'daccs-company-notifications-item',
  templateUrl: './company-notifications-item.component.html',
  styleUrls: ['./company-notifications-item.component.scss']
})
export class CompanyNotificationsItemComponent implements OnChanges {

  @Input() companyNotifyItem: ICompanyNotifySetting;
  @Input() userList: Array<IUser>;
  @Input() even: boolean;
  @Input() odd: boolean;

  public user: IUser;
  public busy = false;
  public remModalIsOpen = false;

  constructor(private _store: Store<IAppStore>,
              private _notificationsService: NotificationsService,
              private _toasterService: ToasterService) {
  }

  public toggleNotification(ev: boolean, key: string) {
    this.busy = true;
    this._notificationsService.updateCompanyNotifications({[key]: ev}, this.companyNotifyItem.id)
      .subscribe((notif: ICompanyNotifySetting) => {
        this._store.dispatch(new companyNotifyActions.UpdateCompanyNotification(notif));
        this.busy = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        console.log(err);
      });
  }

  public openRemModal(ev: Event): void {
    ev.preventDefault();
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  public onRemoveNotif(id: number): void {
    console.log(id);
    this.remModalIsOpen = false;
    this.busy = true;
    this._notificationsService.removeCompanyNotifications(id)
      .subscribe(() => {
        this._store.dispatch(new companyNotifyActions.RemoveCompanyNotification(id));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'Notification deleted');
      }, err => {
        const toast: Toast = {
          type: 'error',
          title: `Error Code: ${err.status}, Message: ${err.statusText}`,
          body: UtilityFunctions.parseErrs(err._body, err.status),
          bodyOutputType: BodyOutputType.TrustedHtml
        };
        this._toasterService.pop(toast);
        this.busy = false;
      });
    this.remModalIsOpen = false;
  }

  private _getUser(userArr: Array<IUser>, id: number): IUser {
    return userArr.find((user: IUser) => {
      return user.id === id
    })
  }

  ngOnChanges(): void {
    if (this.userList) {
      this.user = this._getUser(this.userList, this.companyNotifyItem.user)
    }
  }

}
