import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyNotificationsItemComponent } from './company-notifications-item.component';

describe('CompanyNotificationsItemComponent', () => {
  let component: CompanyNotificationsItemComponent;
  let fixture: ComponentFixture<CompanyNotificationsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyNotificationsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyNotificationsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
