import { Component, OnInit } from '@angular/core';
import { NotificationsService } from './services/notifications.service';
import { ISiteNotifySetting } from './models/site-notifications';
import { ICompanyNotifySetting } from './models/company-notifications';
import { UsersService } from '../users/services/users.service';
import { IUser } from '../../../../shared/models/user';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as siteNotifyActions from './actions/site-notifications.actions'
import * as companyNotifyActions from './actions/company-notifications.actions'
import * as userActions from '../../company/users/actions/users.actions'

@Component({
  selector: 'daccs-notifications',
  templateUrl: './notifications.component.html'
})
export class NotificationsComponent implements OnInit {

  public loading = false;
  public userList$: Observable<Array<IUser>>;
  public siteNotifications$: Observable<Array<ISiteNotifySetting>>;
  public companyNotifications$: Observable<Array<ICompanyNotifySetting>>;

  constructor(private _store: Store<IAppStore>,
              private _notificationsService: NotificationsService,
              private _usersService: UsersService) {
    this.siteNotifications$ = this._notificationsService.siteNotifications$;
    this.companyNotifications$ = this._notificationsService.companyNotifications$;
    this.userList$ = this._usersService.users$;
  }

  ngOnInit() {
    this.loading = true;
    this._notificationsService.getAllNotificationSettings()
      .subscribe((res: [Array<ISiteNotifySetting>, Array<ICompanyNotifySetting>]) => {
        this._store.dispatch(new siteNotifyActions.LoadSiteNotifications(res[0]));
        this._store.dispatch(new companyNotifyActions.LoadCompanyNotifications(res[1]));
        this.loading = false;
      }, (err) => {
        console.log(err);
        this.loading = false;
      });
    this._usersService.getUsers().subscribe((users: Array<IUser>) => {
      this._store.dispatch(new userActions.LoadUsers(users))
    })
  }

}
