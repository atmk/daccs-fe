import { Action } from '@ngrx/store';
import { ISiteNotifySetting } from '../models/site-notifications';

export const LOAD_SITE_NOTIFICATIONS = 'LOAD_SITE_NOTIFICATIONS';
export const ADD_SITE_NOTIFICATION = 'ADD_SITE_NOTIFICATION';
export const REMOVE_SITE_NOTIFICATION = 'REMOVE_SITE_NOTIFICATION';
export const UPDATE_SITE_NOTIFICATION = 'UPDATE_SITE_NOTIFICATION';

export class LoadSiteNotifications implements Action {
  readonly type = LOAD_SITE_NOTIFICATIONS;

  constructor(public payload: Array<ISiteNotifySetting>) {
  }
}

export class AddSiteNotification implements Action {
  readonly type = ADD_SITE_NOTIFICATION;

  constructor(public payload: ISiteNotifySetting) {
  }
}

export class RemoveSiteNotification implements Action {
  readonly type = REMOVE_SITE_NOTIFICATION;

  constructor(public payload: number) {
  }
}

export class UpdateSiteNotification implements Action {
  readonly type = UPDATE_SITE_NOTIFICATION;

  constructor(public payload: ISiteNotifySetting) {
  }
}

export type All = LoadSiteNotifications
  | UpdateSiteNotification
  | AddSiteNotification
  | RemoveSiteNotification;
