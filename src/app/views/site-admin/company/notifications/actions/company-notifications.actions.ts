import { Action } from '@ngrx/store';
import { ICompanyNotifySetting } from '../models/company-notifications';

export const LOAD_COMPANY_NOTIFICATIONS = 'LOAD_COMPANY_NOTIFICATIONS';
export const ADD_COMPANY_NOTIFICATION = 'ADD_COMPANY_NOTIFICATION';
export const REMOVE_COMPANY_NOTIFICATION = 'REMOVE_COMPANY_NOTIFICATION';
export const UPDATE_COMPANY_NOTIFICATION = 'UPDATE_COMPANY_NOTIFICATION';

export class LoadCompanyNotifications implements Action {
  readonly type = LOAD_COMPANY_NOTIFICATIONS;

  constructor(public payload: Array<ICompanyNotifySetting>) {
  }
}

export class AddCompanyNotification implements Action {
  readonly type = ADD_COMPANY_NOTIFICATION;

  constructor(public payload: ICompanyNotifySetting) {
  }
}

export class RemoveCompanyNotification implements Action {
  readonly type = REMOVE_COMPANY_NOTIFICATION;

  constructor(public payload: number) {
  }
}

export class UpdateCompanyNotification implements Action {
  readonly type = UPDATE_COMPANY_NOTIFICATION;

  constructor(public payload: ICompanyNotifySetting) {
  }
}

export type All = LoadCompanyNotifications
  | UpdateCompanyNotification
  | AddCompanyNotification
  | RemoveCompanyNotification
