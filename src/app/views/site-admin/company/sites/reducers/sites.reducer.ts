import * as SiteActions from '../actions/sites.actions';
import { ISite } from '../models/site';

export type Action = SiteActions.All;

export function sitesReducer(state: Array<ISite> = [], action: Action): Array<ISite> {
  switch (action.type) {

    case SiteActions.LOAD_SITES:
      return action.payload;

    case SiteActions.CREATE_SITE:
      return [...state, action.payload];

    case SiteActions.UPDATE_SITE:
      if (!state.length) {
        return [action.payload]
      } else {
        return state.map(site => {
          return site.id === action.payload.id ? Object.assign({}, site, action.payload) : site;
        });
      }

    case SiteActions.REMOVE_SITE:
      return state.filter(site => {
        return site.id !== action.payload;
      });
    default:
      return state;
  }
}
