import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ISite } from '../models/site';
import { environment } from '../../../../../../environments/environment';
import { AuthHttp } from 'angular2-jwt';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { IFranchise } from '../models/franchise';
import 'rxjs/add/observable/forkJoin';

@Injectable()
export class SitesService {

  public sites$: Observable<Array<ISite>>;

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp, private _store: Store<IAppStore>) {
    this.sites$ = this._store.select('sitesReducer');
  }

  public getSites(): Observable<Array<ISite>> {
    return this._authHttp.get(`${this._apiBaseUrl}/companies/site/`)
      .map(res => res.json());
  }

  public getFranchises(): Observable<Array<IFranchise>> {
    return this._authHttp.get(`${this._apiBaseUrl}/companies/franchise/`).map(res => res.json());
  }

  public getSiteById(_id_): Observable<[ISite, Array<IFranchise>]> {

    const site = this._authHttp.get(`${this._apiBaseUrl}/companies/site/${_id_}/`).map(res => res.json());
    const franchises = this._authHttp.get(`${this._apiBaseUrl}/companies/franchise/`).map(res => res.json());

    return Observable.forkJoin(site, franchises);
  }

  public createSite(body: ISite): Observable<ISite> {
    return this._authHttp.post(`${this._apiBaseUrl}/companies/site/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public updateSite(body: ISite): Observable<ISite> {
    return this._authHttp.patch(`${this._apiBaseUrl}/companies/site/${body._id_}/`, JSON.stringify(body))
      .map(res => res.json());
  }

  public removeSite(_id_: number): Observable<null> {
    return this._authHttp.delete(`${this._apiBaseUrl}/companies/site/${_id_}/`)
      .map(res => res.json());
  }
}
