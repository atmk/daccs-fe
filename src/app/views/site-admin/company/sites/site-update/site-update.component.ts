import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import 'rxjs/operator/switchMap';
import { SitesService } from '../services/sites.service';
import { ISite } from '../models/site';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IFranchise } from '../models/franchise';
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as siteAct from '../actions/sites.actions'
import * as siteProfileAct from '../../../../../initialiaze/actions/site-profile.actions'
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'daccs-site-update',
  templateUrl: './site-update.component.html',
  styleUrls: ['./site-update.component.scss']
})
export class SiteUpdateComponent implements OnInit {

  public site: ISite;
  public franchises: Array<IFranchise>;
  public loading = false;
  public updateSiteForm: FormGroup;
  public busy = false;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _actRoute: ActivatedRoute,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>,
              private _sitesService: SitesService) {
    this.updateSiteForm = this._fb.group({
      title: ['', Validators.required],
      active: '',
      company_registration_name: '',
      company_registration_number: '',
      company_vat_number: '',
      contact_person_name: '',
      contact_email: ['', CustomSyncValidators.emailValidator],
      contact_mobile_number: '',
      site_address: '',
      site_telephone: ['', Validators.required],
      franchise: ['', Validators.required],
      _id_: '',
    })
  }

  public onUpdateSite(body: ISite) {
    this.busy = true;
    this._sitesService.updateSite(body)
      .subscribe((res: ISite) => {
        this._store.dispatch(new siteAct.UpdateSite(res));
        this._store.dispatch(new siteProfileAct.UpdateAccessSites(res));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'Site updated');
        this._router.navigate(['../../'], {relativeTo: this._actRoute});
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
  }

  ngOnInit() {
    this.loading = true;
    this._actRoute.paramMap
      .switchMap((params: ParamMap) => {
        return this._sitesService.getSiteById(+params.get('site_id'));
      }).subscribe(
      (data: [ISite, Array<IFranchise>]) => {
        this.loading = false;
        this.site = data[0];
        this.franchises = data[1];
        this.updateSiteForm.setValue({
          title: this.site.title,
          active: this.site.active,
          company_registration_name: this.site.company_registration_name,
          company_registration_number: this.site.company_registration_number,
          company_vat_number: this.site.company_vat_number,
          contact_person_name: this.site.contact_person_name,
          contact_email: this.site.contact_email,
          contact_mobile_number: this.site.contact_mobile_number,
          site_address: this.site.site_address,
          site_telephone: this.site.site_telephone,
          franchise: this.site.franchise,
          _id_: this.site._id_,
        })
      },
      err => {
        this.loading = false;
        console.log(err);
      })
  }

}
