import { Action } from '@ngrx/store';
import { ISite } from '../models/site';

export const LOAD_SITES = 'LOAD_SITES';
export const CREATE_SITE = 'CREATE_SITE';
export const UPDATE_SITE = 'UPDATE_SITE';
export const REMOVE_SITE = 'REMOVE_SITE';

export class LoadSites implements Action {
  readonly type = LOAD_SITES;

  constructor(public payload: Array<ISite>) {
  }
}

export class CreateSite implements Action {
  readonly type = CREATE_SITE;

  constructor(public payload: ISite) {
  }
}

export class UpdateSite implements Action {
  readonly type = UPDATE_SITE;

  constructor(public payload: ISite) {
  }
}

export class RemoveSite implements Action {
  readonly type = REMOVE_SITE;

  constructor(public payload: number) {
  }
}

export type All = LoadSites | CreateSite | UpdateSite | RemoveSite
