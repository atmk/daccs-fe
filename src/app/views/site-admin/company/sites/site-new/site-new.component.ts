import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomSyncValidators } from '../../../../../shared/validators/sync-validators';
import { ActivatedRoute, Router } from '@angular/router';
import { ISite } from '../models/site';
import { ToasterService } from 'angular2-toaster';
import { Store } from '@ngrx/store';
import { IAppStore } from '../../../../../shared/ngrx/appstore.ngrx.interface';
import { SitesService } from '../services/sites.service';
import * as siteAct from '../actions/sites.actions'
import * as siteProfileAct from '../../../../../initialiaze/actions/site-profile.actions'
import { IFranchise } from '../models/franchise';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'daccs-site-new',
  templateUrl: './site-new.component.html',
  styleUrls: ['./site-new.component.scss']
})
export class SiteNewComponent implements OnInit {

  public createNewSiteForm: FormGroup;
  public busy = false;
  public franchises$: Observable<Array<IFranchise>>;

  constructor(private _fb: FormBuilder,
              private _router: Router,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>,
              private _sitesService: SitesService,
              private _actRoute: ActivatedRoute) {
  }

  public onCreateSite(body: ISite) {
    this.busy = true;
    this._sitesService.createSite(body)
      .subscribe((res: ISite) => {
        this._store.dispatch(new siteAct.CreateSite(res));
        this._store.dispatch(new siteProfileAct.AddAccessSites(res));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'New site added');
        this._router.navigate(['../'], {relativeTo: this._actRoute});
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
  }

  ngOnInit(): void {

    this.franchises$ = this._sitesService.getFranchises();

    this.createNewSiteForm = this._fb.group({
      title: ['', Validators.required],
      active: '',
      company_registration_name: '',
      company_registration_number: '',
      company_vat_number: '',
      contact_person_name: '',
      contact_email: ['', CustomSyncValidators.emailValidator],
      contact_mobile_number: '',
      site_address: '',
      site_telephone: ['', Validators.required],
      franchise: ['', Validators.required],
      _id_: '',
    })
  }

}
