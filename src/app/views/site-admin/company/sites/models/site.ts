export interface ISite {
  title: string;
  active: boolean;
  setup_complete: boolean;
  company_registration_name: string;
  company_registration_number: string;
  company_vat_number: string;
  contact_person_name: string;
  contact_email: string;
  contact_mobile_number: string;
  site_address: string;
  site_telephone: string;
  franchise: number;
  date_created: string;
  franchise_logo_url: string;
  franchise_title: string;
  _id_: number;
  holding_company?: number;
  holding_company_title?: string;
  id?: number;
  account_number?: string;
  access_sites?: Array<ISite>
}
