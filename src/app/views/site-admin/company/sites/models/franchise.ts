export interface IFranchise {
  color: string;
  country: string;
  id: number;
  logo_url: string;
  title: string;
}
