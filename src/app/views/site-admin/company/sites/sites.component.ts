import { Component, OnInit } from '@angular/core';
import { SitesService } from './services/sites.service';
import { ISite } from './models/site';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import * as sitesAct from './actions/sites.actions';
import * as siteProfileAct from '../../../../initialiaze/actions/site-profile.actions';
import { Observable } from 'rxjs/Observable';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'daccs-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit {

  public sites$: Observable<Array<ISite>>;
  public remModalIsOpen = false;
  public siteIdSelected: number;
  public busy = false;
  public loading = false;

  constructor(private _sitesService: SitesService,
              private _toasterService: ToasterService,
              private _store: Store<IAppStore>) {
    this.sites$ = this._sitesService.sites$;
  }

  public onRemoveSite(): void {
    this.busy = true;
    this._sitesService.removeSite(this.siteIdSelected)
      .subscribe(() => {
        this._store.dispatch(new sitesAct.RemoveSite(this.siteIdSelected));
        this._store.dispatch(new siteProfileAct.RemoveAccessSites(this.siteIdSelected));
        this.busy = false;
        this._toasterService.pop('success', 'Success',
          'Site deleted');
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
    this.remModalIsOpen = false;
  }

  public openRemModal(ev: Event, _id_: number): void {
    ev.preventDefault();
    this.siteIdSelected = _id_;
    this.remModalIsOpen = true;
  }

  public closeRemModal(): void {
    this.remModalIsOpen = false;
  }

  ngOnInit() {
    this.loading = true;
    this._sitesService.getSites()
      .subscribe((res: Array<ISite>) => {
        this._store.dispatch(new sitesAct.LoadSites(res));
        this.loading = false
      }, err => {
        console.log(err);
        this.loading = false;
      })
  }

}
