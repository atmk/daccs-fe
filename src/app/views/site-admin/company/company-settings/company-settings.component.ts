import { Component, OnInit } from '@angular/core';
import { CompanyService, ICompany } from './services/company.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Countries } from '../../../../shared/utils/countriesISO3166';
import { ToasterService } from 'angular2-toaster';
import { CustomSyncValidators } from '../../../../shared/validators/sync-validators';

@Component({
  selector: 'daccs-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.scss'],
  providers: [CompanyService]
})
export class CompanySettingsComponent implements OnInit {

  public companyDataForm: FormGroup;
  public companyData: ICompany;
  public busy = false;
  public loading = false;
  public countries: Array<any>;

  constructor(private _companyService: CompanyService,
              private _toasterService: ToasterService,
              private _fb: FormBuilder) {

    this.countries = Countries;

    this.companyDataForm = this._fb.group({
      _id_: '',
      title: ['', Validators.required],
      active: '',
      company_registration_name: '',
      company_vat_number: '',
      contact_person_name: '',
      contact_email: ['', CustomSyncValidators.emailValidator],
      contact_mobile_number: '',
      company_address: '',
      company_telephone: ['', Validators.required],
      country: '',
    })
  }

  public onUpdateCompany(body: ICompany): void {
    this.busy = true;
    this._companyService.updateCompanySettings(body)
      .subscribe(() => {
        this._toasterService.pop('success', 'Success',
          'Company Details Updated');
        this.busy = false;
        this.companyDataForm.markAsPristine();
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      })
  }

  ngOnInit(): void {
    this.loading = true;
    this._companyService.getCompanySettings()
      .subscribe((data: ICompany) => {
        this.companyData = data;
        this.companyDataForm.setValue({
          _id_: data._id_,
          title: data.title,
          active: data.active,
          company_registration_name: data.company_registration_name,
          company_vat_number: data.company_vat_number,
          contact_person_name: data.contact_person_name,
          contact_email: data.contact_email,
          contact_mobile_number: data.contact_mobile_number,
          company_address: data.company_address,
          company_telephone: data.company_telephone,
          country: data.country,
        });
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
      });
  }

}
