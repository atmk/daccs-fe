import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthHttp } from 'angular2-jwt';
import { environment } from '../../../../../../environments/environment';

export interface ICompany {
  id: string;
  _id_: number
  title: string;
  active: boolean;
  account_number?: number;
  company_registration_name: string;
  company_vat_number: string;
  contact_person_name: string;
  contact_email: string;
  contact_mobile_number: string;
  company_address: string;
  company_telephone: string;
  country: string;
  date_created: string
}

@Injectable()
export class CompanyService {

  private _apiBaseUrl = environment.apiBaseUrl;

  constructor(private _authHttp: AuthHttp) {
  }

  public getCompanySettings(): Observable<ICompany> {
    return this._authHttp.get(`${this._apiBaseUrl}/companies/company/me/`)
      .map(res => res.json());
  }

  public updateCompanySettings(body: ICompany): Observable<ICompany> {
    return this._authHttp.patch(`${this._apiBaseUrl}/companies/company/me/update/`, JSON.stringify(body))
      .map(res => res.json());
  }

}
