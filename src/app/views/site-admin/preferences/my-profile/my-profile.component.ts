import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { CustomSyncValidators } from '../../../../shared/validators/sync-validators';
import { UsersService } from '../../company/users/services/users.service';
import { IAppStore } from '../../../../shared/ngrx/appstore.ngrx.interface';
import { Store } from '@ngrx/store';
import { IUser } from '../../../../shared/models/user';
import * as meAct from '../../../../initialiaze/actions/me.actions';

@Component({
  selector: 'daccs-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  public profileDataForm: FormGroup;
  public profileData: IUser;
  public busy = false;
  public busy1 = false;
  public loading = false;
  public me$: Observable<IUser>;
  public passwordResetForm: FormGroup;

  constructor(private _store: Store<IAppStore>,
              private _toasterService: ToasterService,
              private _fb: FormBuilder,
              private _usersService: UsersService) {
    this.profileDataForm = this._fb.group({
      id: '',
      first_name: '',
      last_name: '',
      mobile_number: '',
      email: ['', [CustomSyncValidators.emailValidator, Validators.required]],
    });
    this.passwordResetForm = this._fb.group({
      id: '',
      old_password: ['', Validators.required],
      password: ['', [CustomSyncValidators.passwordValidator, Validators.required]],
      confirm_password: ['', [CustomSyncValidators.passwordValidator, Validators.required]],
    });
    this.me$ = this._store.select('meReducer')
  }

  onSubmitPasswordChange(body) {
    this.busy1 = true;
    if (body.password !== body.confirm_password) {
      this._toasterService.pop('error', 'Errors',
        `Your New Password and Confirmation do not match`);
      this.busy1 = false;
      return false;
    } else {
      this._usersService.changePassword(body)
        .subscribe(() => {
          this._toasterService.pop('success', 'Success',
            'Password changed');
          this.busy1 = false;
          this._resetPassForm();
        }, err => {
          this._toasterService.pop('error', `Error: ${err.status}`,
            `Message: ${err.statusText}`);
          this.busy1 = false;
          this._resetPassForm();
        });
    }
  }

  public onUpdateMe(body): void {
    this.busy = true;
    this._usersService.updateUser(body)
      .subscribe((profile: IUser) => {
        this._store.dispatch(new meAct.LoadMe(profile));
        this._toasterService.pop('success', 'Success',
          'Profile Details Updated');
        this.profileDataForm.markAsPristine();
        this.busy = false;
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
        this.busy = false;
      });
  }

  private _resetPassForm(): void {
    this.passwordResetForm.patchValue({
        old_password: '',
        password: '',
        confirm_password: ''
      }
    );
    this.passwordResetForm.markAsPristine();
  }

  ngOnInit(): void {
    this.me$
      .subscribe((data: IUser) => {
        this.profileData = data;
        this.profileDataForm.setValue({
          id: data.id,
          first_name: data.first_name,
          last_name: data.last_name,
          mobile_number: data.mobile_number,
          email: data.email,
        });
        this.passwordResetForm.patchValue({
          id: data.id,
        });
      }, err => {
        this._toasterService.pop('error', `Error: ${err.status}`,
          `Message: ${err.statusText}`);
      });
  }

}
