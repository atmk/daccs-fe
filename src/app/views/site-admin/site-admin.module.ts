import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/modules/shared.module';
import { ClarityModule } from 'clarity-angular';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuppliersSetupComponent } from './site-setup/suppliers-setup/suppliers-setup.component';
import { AccHoldersSetupComponent } from './site-setup/acc-holders-setup/acc-holders-setup.component';
import { CashiersSetupComponent } from './site-setup/cashiers-setup/cashiers-setup.component';
import { GenLedgerSetupComponent } from './site-setup/gen-ledger-setup/gen-ledger-setup.component';
import { CatSetupComponent } from './site-setup/cat-setup/cat-setup.component';
import { SitesComponent } from './company/sites/sites.component';
import { CompanySettingsComponent } from './company/company-settings/company-settings.component';
import { MyProfileComponent } from './preferences/my-profile/my-profile.component';
import { CashierItemComponent } from './site-setup/cashiers-setup/cashier-item/cashier-item.component';
import { CashierService } from './site-setup/cashiers-setup/services/cashier.service';
import { GenLedgerService } from './site-setup/gen-ledger-setup/services/gen-ledger.service';
import { GenLedgerItemComponent } from './site-setup/gen-ledger-setup/gen-ledger-item/gen-ledger-item.component';
import { AccHolderItemComponent } from './site-setup/acc-holders-setup/acc-holders-item/acc-holder-item.component';
import { AccHolderService } from './site-setup/acc-holders-setup/services/account-holders.service';
import { SuppliersService } from './site-setup/suppliers-setup/services/suppliers.service';
import { SupplierItemComponent } from './site-setup/suppliers-setup/supplier-item/supplier-item.component';
import { SiteNewComponent } from './company/sites/site-new/site-new.component';
import { SiteUpdateComponent } from './company/sites/site-update/site-update.component';
import { SitesService } from './company/sites/services/sites.service';
import { SystemGlPipe } from './site-setup/gen-ledger-setup/pipes/system-gl.pipe';
import { WizMainCatComponent } from './site-setup/cat-setup/components/wiz-main-cat/wiz-main-cat.component';
import { CatsService } from './site-setup/cat-setup/services/cats.service';
import { WizMainItemComponent } from './site-setup/cat-setup/components/toggle-item/toggle-item.component';
import { WizEodCatComponent } from './site-setup/cat-setup/components/wiz-eod-cat/wiz-eod-cat.component';
import { WizSubCatComponent } from './site-setup/cat-setup/components/wiz-sub-cat/wiz-sub-cat.component';
import { SpeedpointSetupComponent } from './site-setup/speedpoint-setup/speedpoint-setup.component';
import { SpeedpointService } from './site-setup/speedpoint-setup/services/speedpoint.service';
import { SpeedpointItemComponent } from './site-setup/speedpoint-setup/speedpoint-item/speedpoint-item.component';
import { UsersService } from './company/users/services/users.service';
import { UsersComponent } from './company/users/users.component';
import { UserUpdateComponent } from './company/users/user-update/user-update.component';
import { UserNewComponent } from './company/users/user-new/user-new.component';
import { NotificationsComponent } from './company/notifications/notifications.component';
import { NotificationsService } from './company/notifications/services/notifications.service';
import { CompanyNotificationsComponent } from './company/notifications/company-notifications/company-notifications.component';
import { SiteNotificationsComponent } from './company/notifications/site-notifications/site-notifications.component';
import { SiteNotificationsItemComponent } from './company/notifications/site-notifications/site-notifications-item/site-notifications-item.component';
import { CompanyNotificationsItemComponent } from './company/notifications/company-notifications/company-notifications-item/company-notifications-item.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ClarityModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    MyProfileComponent,
    CompanySettingsComponent,
    CatSetupComponent,
    GenLedgerSetupComponent,
    CashiersSetupComponent,
    AccHoldersSetupComponent,
    SuppliersSetupComponent,
    CashierItemComponent,
    GenLedgerItemComponent,
    AccHolderItemComponent,
    SupplierItemComponent,
    SitesComponent,
    SiteNewComponent,
    SiteUpdateComponent,
    UsersComponent,
    UserUpdateComponent,
    UserNewComponent,
    SystemGlPipe,
    WizMainCatComponent,
    WizMainItemComponent,
    WizEodCatComponent,
    WizSubCatComponent,
    SpeedpointSetupComponent,
    SpeedpointItemComponent,
    NotificationsComponent,
    CompanyNotificationsComponent,
    SiteNotificationsComponent,
    SiteNotificationsItemComponent,
    CompanyNotificationsItemComponent
  ],
  providers: [
    CashierService,
    GenLedgerService,
    AccHolderService,
    SuppliersService,
    SitesService,
    UsersService,
    CatsService,
    SpeedpointService,
    NotificationsService
  ]
})
export class SiteAdminModule {
}
