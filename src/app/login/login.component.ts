import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
  selector: 'daccs-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public logo = 'assets/images/logo_bl.png';
  public model: any = {};
  public loading = false;
  public resetting = false;
  public badLogin = false;
  public openWizard = false;
  public isResetError = false;
  public loginErrs: Array<string>;
  public signUpURL: string = environment.apiBaseUrl + '/signup';
  public resetMessage = '';

  constructor(private _authService: AuthService, private _router: Router) {
  }

  public login(): void {
    this.loading = true;
    this._authService.login(this.model.email, this.model.password)
      .subscribe(result => {
        if (result === true) {
          this._router.navigate(['/']);
        } else {
          this.loading = false;
        }
      }, err => {
        const errRes = JSON.parse(err._body);
        if (errRes.non_field_errors) {
          this.loginErrs = errRes.non_field_errors || [];
          this.badLogin = true;
        }
        this.loading = false;
      });
  }

  public resetPassword(): void {
    this.resetting = true;
    this.resetMessage = '';
    this._authService.passwordReset(this.model.reset_email)
      .subscribe(result => {
        if (result) {
          this.resetMessage = 'Password reset success, please check your mail to complete the reset';
          this.resetting = false;
        } else {
          this.isResetError = true;
          this.resetMessage = 'Sorry, an unexpected error occurred, please try again.';
        }
      }, err => {
          this.isResetError = true;
        const errRes = JSON.parse(err._body);
        this.resetMessage = 'Sorry, an unexpected error occurred, please try again. \n' + JSON.stringify(errRes);
        this.resetting = false;
      });
  }

  ngOnInit() {
  }

  open() {
    this.openWizard = !this.openWizard;
    this.resetMessage = '';
    event.preventDefault();
  }

}
